--[[  ******************** How to install ********************
- Add to gamemode:
		- Add to OnGameModeInit:

			MySQL_Config.host = "127.0.0.1";
			MySQL_Config.user = "user";
			MySQL_Config.password = "password";
			MySQL_Config.dbname = "database";
			SD_OnGameModeInit()
			
		- Add SD_OnGameModeInit() to OnGameModeInit
		- Add SD_OnPlayerConneted() to OnPlayerConnect
		- Add SD_OnPlayerSpawn() to OnPlayerSpawn
		- Add SD_OnPlayerSelectClass() to OnPlayerSelectClass
		- Add SD_OnPlayerCommandText() to OnPlayerCommandText
		- Add SD_OnPlayerChangeMaxHealth() to OnPlayerChangeMaxHealth
		- Add SD_OnPlayerChangeMaxMana() to OnPlayerChangeMaxMana
		- Add SD_OnPlayerChangeGold() to OnPlayerChangeGold
		- Add SD_OnPlayerChangeAdditionalVisual() to OnPlayerChangeAdditionalVisual
		- Add SD_OnPlayerChangeDexterity() to OnPlayerChangeDexterity
		- Add SD_OnPlayerChangeAcrobatic() to OnPlayerChangeAcrobatic
		- Add SD_OnPlayerChangeStrength() to OnPlayerChangeStrength 
		- On top gamemode add 'require "(HERE PATH TO FILE)/SaveData.lua"'
- Change default settings
- The table in the MySQL will be created automatically.
--]]

--[[ ************* Settings ********************* ]]--
local DEFAULT_MaxHP = 100;
local DEFAULT_MaxMana = 100;
local DEFAULT_Strength = 50;
local DEFAULT_Dex = 50;		
local DEFAULT_Body = 0;	-- Body model id
local DEFAULT_Head = 0;	-- Head texture id
local DEFAULT_Headmodel = 0; -- Head model id
local DEFAULT_Gold = 200;
local DEFAULT_Acrobatic = 0;
local DEFAULT_Fatness = 1.0;
local DEFAULT_Exp = 0;
local CheckMySQLTime = 30000; -- Optional.

--[[ ******************** Info ******************** ]]--
-- IsLogin[playerid] == 0 -- Player is not register.
-- IsLogin[playerid] == 1 -- Player is register, not logged.
-- IsLogin[playerid] == 2 -- Player is logged.
-- MySQL Handle: hmysql

IsLogin = {};
MySQL_Config = {};

function SD_OnGameModeInit()
	
	hmysql = mysql_connect(MySQL_Config.host, MySQL_Config.user, MySQL_Config.password, MySQL_Config.dbname);
	
	if mysql_ping(hmysql) == false then
		LogString("mysql","Trying to connect to the server MySQL failed.");
	else
		mysql_query(hmysql, "CREATE TABLE IF NOT EXISTS `players` (`id` int(11) NOT NULL AUTO_INCREMENT,`nick` varchar(60) NOT NULL,`pass` varchar(30) NOT NULL, `maxhp` int(11) NOT NULL DEFAULT '".. DEFAULT_MaxHP .."', `maxmana` int(11) NOT NULL DEFAULT '".. DEFAULT_MaxMana .."',  `strength` int(11) NOT NULL DEFAULT '".. DEFAULT_Strength .."',  `dex` int(11) NOT NULL DEFAULT '".. DEFAULT_Dex .."',  `body` int(11) NOT NULL DEFAULT '".. DEFAULT_Body .."',  `headmodel` int(11) NOT NULL DEFAULT '.. DEFAULT_Headmodel ..',  `head` int(11) NOT NULL DEFAULT '".. DEFAULT_Head.."',  `acro` tinyint(2) NOT NULL DEFAULT '".. DEFAULT_Acrobatic .."',  `fat` float NOT NULL DEFAULT '".. DEFAULT_Fatness .."',  `gold` int(11) NOT NULL DEFAULT '".. DEFAULT_Gold .."',  PRIMARY KEY (`id`), UNIQUE KEY `nick` (`nick`))");
		SetTimer("mysqlCheckConnection", CheckMySQLTime, 1);
		print("---------------------------------------");
		print("- SaveData has been loaded correctly. -");
		print("---------------------------------------");
	end				
end



function SD_OnPlayerConnect(playerid)

	local nick = GetPlayerName(playerid);
	local result = mysql_query(hmysql,"SELECT `id` FROM `playerid` WHERE `nick` = '".. nick .."'");
    
	if(mysql_num_rows(result) ~= 0) then
		SendPlayerMessage(playerid, 255,255,255, "Вы зарегестрированы. Залогиньтесь!");
		GameTextForPlayer(playerid,2500,6000,"/лог (пароль)","Font_Old_20_White_Hi.TGA",255,0,0,2000);
		IsLogin[playerid] = 1;
	else
		SendPlayerMessage(playerid, 255,255,255, "Зарегестрируйся: /рег (пароль) чтобы сохранять статистику");
		SendPlayerMessage(playerid, 255,255,255, "Регистрация не обязательна.");
		IsLogin[playerid] = 0;
	end
end

function SD_OnPlayerSelectClass(playerid, classid)

	if IsLogin[playerid] == 0 then
		SetPlayerAcrobatic(playerid, DEFAULT_Acrobatic);
		SetPlayerDexterity(playerid, DEFAULT_Dex);
		SetPlayerGold(playerid, DEFAULT_Gold);
		SetPlayerStrength(playerid, DEFAULT_Strength);
		SetPlayerMaxHealth(playerid, DEFAULT_MaxHP);
		SetPlayerMaxMana(playerid, DEFAULT_MaxMana);
		SetPlayerFatness(playerid, DEFAULT_Fatness);
	end
end

function SD_OnPlayerSpawn(playerid, classid)
	if IsLogin[playerid] == 1 then
		SendPlayerMessage(playerid, 255,255,255, " ");
		SendPlayerMessage(playerid, 255,255,255, " ");
		SendPlayerMessage(playerid, 255,255,255, " ");
		SendPlayerMessage(playerid, 255,255,255, " ");
		SendPlayerMessage(playerid, 255,255,255, " ");
		SendPlayerMessage(playerid, 255,255,255, " ");
		SendPlayerMessage(playerid, 255,255,255, " ");
		SendPlayerMessage(playerid, 255,255,255, " ");
		SendPlayerMessage(playerid, 255,255,255, "Вы были выкинуты с сервера.");
		SendPlayerMessage(playerid, 255,255,255, "Причина: Не залогинился");
		Kick(playerid);	
	end
end


function SD_OnPlayerCommandText(playerid, cmdtext)
	local cmd, params = GetCommand(cmdtext);
	
	if cmd == "/лог" then
		if IsLogin[playerid] == 0 then
			SendPlayerMessage(playerid, 255,255,255, "Вы не зарегистрированы.");
		elseif IsLogin[playerid] == 2 then
			SendPlayerMessage(playerid, 255,255,255, "Вы уже вошли!");
		elseif IsLogin[playerid] == 1 then
			
			local nick = GetPlayerName(playerid);
			local password = MD5(MD5(params)); -- Double MD5, more safety
			local result = mysql_query(hmysql,"SELECT `maxhp`,`maxmana`,`acro`,`dex`,`strength`,`fat`,`gold`,`bodyModel`,`bodyTextureID`,`headModel`,`headTextureID` FROM `players` WHERE `nick` = '".. nick .."' AND `pass`='".. password .."' LIMIT 1");
			
			if mysql_num_fields(result) < 2 then
				SendPlayerMessage(playerid, 255,255,255, "Неправильный пароль!");
			else
				local row = mysql_fetch_row(result);
				IsLogin[playerid] = 2;
				SetPlayerAcrobatic(playerid, row[3]);
				SetPlayerDexterity(playerid, row[4]);
				SetPlayerGold(playerid, row[7]);
				SetPlayerStrength(playerid, row[5]);
				SetPlayerMaxHealth(playerid, row[1]);
				SetPlayerMaxMana(playerid, row[2]);
				SetPlayerFatness(playerid, row[6]);
				SetPlayerAdditionalVisual(playerid,row[8],row[9],row[10],row[11]);
				SendPlayerMessage(playerid, 255,255,255, "Вы успешно вошли.");		
			end
			
			mysql_free_result(result);
		end
		
	elseif cmd == "/рег" then
	
		if IsLogin[playerid] ~= 0 then
			SendPlayerMessage(playerid, 255,255,255, "Вы уже зарегистрировались");
		else	
			if string.len(params) < 5 or string.len(params) > 25 then
				SendPlayerMessage(playerid, 255,255,255, "Пароль не может быть менее 5 символов и более 25");			
			else
				local nick = GetPlayerName(playerid);
				local password = MD5(MD5(params)); -- Double MD5, more safety
				mysql_query(hmysql,"INSERT INTO players (`nick`, `pass`) VALUES ('".. nick .."', '".. password .."')");
				IsLogin[playerid] = 2;
				SendPlayerMessage(playerid, 255,255,255, "Вы зарегистрировались. Ваш пароль: ".. params .." - запомни его!");		
			end
		end
	end

end

function SD_OnPlayerChangeMaxHealth(playerid, currMaxHealth, oldMaxHealth)
 
	if IsLogin[playerid] == 2 then
		local nick = GetPlayerName(playerid);
		mysql_query(hmysql, "UPDATE `players` SET `maxhp`=".. currMaxHealth .." WHERE `nick` = '".. nick .."' LIMIT 1");
	end
end

function SD_OnPlayerChangeMaxMana(playerid, currMaxMana, oldMaxMana)
 
	if IsLogin[playerid] == 2 then
		local nick = GetPlayerName(playerid);
		mysql_query(hmysql, "UPDATE `players` SET `maxmana`=".. currMaxMana .." WHERE `nick` = '".. nick .."' LIMIT 1");
	end
end

function SD_OnPlayerChangSetrength(playerid, currStrength, oldStrength)
 
	if IsLogin[playerid] == 2 then
		local nick = GetPlayerName(playerid);
		mysql_query(hmysql, "UPDATE `players` SET `strength`=".. currStrength .." WHERE `nick` = '".. nick .."' LIMIT 1");
	end
end

function SD_OnPlayerChangeDexterity(playerid, currDexterity, oldDexterity)
 
	if IsLogin[playerid] == 2 then
		local nick = GetPlayerName(playerid);
		mysql_query(hmysql, "UPDATE `players` SET `dex`=".. currDexterity .." WHERE `nick` = '".. nick .."' LIMIT 1");
	end
end

function SD_OnPlayerChangeAdditionalVisual(playerid, currBodyTexture, currHeadModel, currHeadTexture, oldBodyTexture, oldHeadModel, oldHeadTexture)
 
	if IsLogin[playerid] == 2 then
		local nick = GetPlayerName(playerid);
		mysql_query(hmysql, "UPDATE `players` SET `body`=".. currBodyTexture ..", `headmodel`=".. currHeadModel ..", `head`=".. currHeadTexture .." WHERE `nick` = '".. nick .."' LIMIT 1");
	end
end

function SD_OnPlayerChangeAcrobatic(playerid, currAcrobatic, oldAcrobatic)
 
	if IsLogin[playerid] == 2 then
		local nick = GetPlayerName(playerid);
		mysql_query(hmysql, "UPDATE `players` SET `acro`=".. currAcrobatic .." WHERE `nick` = '".. nick .."' LIMIT 1");
	end
end

function SD_OnPlayerChangeFatness(playerid, currFatness, oldFatness)
 
	if IsLogin[playerid] == 2 then
		local nick = GetPlayerName(playerid);
		mysql_query(hmysql, "UPDATE `players` SET `fat`=".. currFatness .." WHERE `nick` = '".. nick .."' LIMIT 1");
	end
end

function SD_OnPlayerChangeGold(playerid, currGold, oldGold)
 
	if IsLogin[playerid] == 2 then
		local nick = GetPlayerName(playerid);
		mysql_query(hmysql, "UPDATE `players` SET `gold`=".. currGold .." WHERE `nick` = '".. nick .."' LIMIT 1");
	end
end

function mysqlCheckConnection()
	if mysql_ping(hmysql) == false then
		LogString("mysql","Lost connection to the MySQL server, reconnecting ...");
		mysql_close(hmysql);
		hmysql = mysql_connect("31.220.49.197", "gmp", "000000", "game");
	end
end