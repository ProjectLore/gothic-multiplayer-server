Player = {};
local gui_stamina = CreateDraw(150, 7500, "Выносливость: 100%", "Font_Old_10_White_Hi.TGA", 255, 255, 255)

function PlayerParams()
for i = 0, GetMaxPlayers() do
	Player[i] = {};
	Player[i].Stamina = 100;
	Player[i].timer_stamina = 0;
end
end

function OnGamemodeInit()
PlayerParams();
end

function OnPlayerConnect(playerid)
--Player[playerid].Stamina = 100;
--Player[playerid].timer_stamina = 0;
end

function OnPlayerSpawn(playerid)
--UpdateDraw(gui_stamina,150, 7500, "Выносливость: "..Player[playerid].Stamina.."%", "Font_Old_10_White_Hi.TGA", 255, 255, 255);
--ShowDraw(playerid,gui_stamina);
end

function OnPlayerKey(playerid, keydown, keyup, keyname)
if keydown == KEY_N then
	if Player[playerid].Stamina > 4 then
		if Player[playerid].timer_stamina > 0 then
			KillTimer(Player[playerid].timer_stamina);
		end
		Player[playerid].timer_stamina = SetTimerEx("SprintOn",1000,1,playerid);
		SetPlayerWalk(playerid,"HumanS_Sprint.mds");
	end
elseif keyup == KEY_N then
	if Player[playerid].timer_stamina > 0 then
		KillTimer(Player[playerid].timer_stamina);
	end
	Player[playerid].timer_stamina = SetTimerEx("SprintOff",1000,1,playerid);
	SetPlayerWalk(playerid,"HumanS_Relaxed.mds");
end
end

function SprintOn(playerid)
Player[playerid].Stamina = Player[playerid].Stamina - 10;
UpdateDraw(gui_stamina,150, 7500, "Выносливость: "..Player[playerid].Stamina.."%", "Font_Old_10_White_Hi.TGA", 255, 255, 255)
if Player[playerid].Stamina == 0 then
	SetPlayerWalk(playerid,"HumanS_Relaxed.mds");
	KillTimer(Player[playerid].timer_stamina);
	Player[playerid].timer_stamina = SetTimerEx("SprintOff",1000,1,playerid);
end
end

function SprintOff(playerid)
Player[playerid].Stamina = Player[playerid].Stamina + 10;
UpdateDraw(gui_stamina,150, 7500, "Выносливость: "..Player[playerid].Stamina.."%", "Font_Old_10_White_Hi.TGA", 255, 255, 255)
if Player[playerid].Stamina == 100 then
	KillTimer(Player[playerid].timer_stamina);
end
end

function OnPlayerDisconnect(playerid,reason)
if Player[playerid].timer_stamina > 0 then
	KillTimer(Player[playerid].timer_stamina);
end
end