require "mob/herbs"
require "mob/createherbs"

local NPC = { TP = {} };

function InitN174Mob()

--������ ������
NPC.Bimbol = createNPC("������", "Hum_Body_Naked0", 1, "Hum_Head_Psionic", 59, "S_SIT", "PC_HERO", 1402.5584716797, 248.10528564453, 732.96948242188, 205, "WOM_COLONY.ZEN", "ITMW_SHORTSWORD4", nil, "ITAR_DIEGO");
NPC.Guard = createNPC("����", "Hum_Body_Naked0", 2, "Hum_Head_Psionic", 15, "S_HGUARD", "PC_HERO", 2829.1694335938, 248.02661132813, -1209.0316162109, 209, "WOM_COLONY.ZEN", "ITMW_1H_MIL_SWORD", nil, "GRD_ARMOR_L");
NPC.FireMage = createNPC("�����", "Hum_Body_Naked0", 0, "Hum_Head_Pony", 50, "S_HGUARD", "PC_HERO", 1402.6875, 897.91009521484, -3257.8752441406, 327, "WOM_COLONY.ZEN", nil, nil, "ITAR_KDF_L");
NPC.GateGuard = createNPC("�������", "Hum_Body_Naked0", 1, "Hum_Head_Bald", 22, "S_HGUARD", "PC_HERO", -3223.6682128906, -299.8053894043, 1406.3355712891, 353, "WOM_COLONY.ZEN", "ITMW_1H_MIL_SWORD", nil, "GRD_ARMOR_L");
NPC.DIEGO = createNPC("�����", "Hum_Body_Naked0", 0, "Hum_Head_Bald", 45, "S_BENCH_S1", "PC_HERO",-3681.0981445313, -615.69500732422, 3520.9948730469, 60, "WOM_COLONY.ZEN", "ITMW_1H_MIL_SWORD", nil, "ITAR_DIEGO");

--����� ������
NPC.Ariadna = createNPC("�������", "Hum_Body_Babe0", 5, "Hum_Head_Babe", 144, "S_LGUARD", "VLK_447_CASSIA", -60242.30859375, 4145.5791015625, 7860.208984375, 279, "WOM_COLONY.ZEN", nil, nil, nil);
NPC.WaterMage = createNPC("������", "Hum_Body_Naked0", 0, "Hum_Head_Pony", 51, "S_HGUARD", "PC_HERO", -57052.86328125, 2419.1987304688, 3091.3432617188, 60, "WOM_COLONY.ZEN", nil, nil, "ITAR_KDW_L");
NPC.NL1 = createNPC("��������", "Hum_Body_Naked0", 1, "Hum_Head_Pony", 58, "S_HGUARD", "PC_HERO", -39790.66796875, 892.06280517578, 7138.6235351563, 172, "WOM_COLONY.ZEN", "ITMW_1H_MIL_SWORD", nil, "ORG_ARMOR_L");
NPC.NL2 = createNPC("����", "Hum_Body_Naked0", 0, "Hum_Head_Pony", 56, "S_HGUARD", "PC_HERO", -48747.45703125, 2655.3962402344, 13975.125976563, 19, "WOM_COLONY.ZEN", "ITMW_1H_MIL_SWORD", nil, "ORG_ARMOR_L");
NPC.NLBAR = createNPC("�����", "Hum_Body_Naked0", 1, "Hum_Head_Pony", 35, "S_HGUARD", "PC_HERO", -53283.1796875, 2698.1877441406, 12512.560546875, 67, "WOM_COLONY.ZEN", "ITMW_1H_MIL_SWORD", nil, "ORG_ARMOR_L");


--���������
MENU_TEXTURE = CreateTexture(2800, 2500, 5000, 5500, "DLG_CONVERSATION.TGA")

SpawnHerbs();
SetTimer("RespawnItemOnGrund", 1000 * 60 * 20, 1); -- Respawn Herbs
end

function ShowMenuTexture(playerid)
ShowTexture(playerid,MENU_TEXTURE);
end

function HideMenuTexture(playerid)
HideTexture(playerid,MENU_TEXTURE);
end

function GetFocusDIEGO(playerid)
	return NPC.DIEGO;
end

function GetFocusBimbol(playerid)
	return NPC.Bimbol;
end

function GetFocusGateGuard(playerid)
	return NPC.GateGuard;
end

function GetFocusGuard(playerid)
	return NPC.Guard;
end

function GetFocusFireMage(playerid)
	return NPC.FireMage;
end

function GetFocusWaterMage(playerid)
	return NPC.WaterMage;
end

function GetFocusAriadna(playerid)
	return NPC.Ariadna;
end

function GetFocusNL1(playerid)
	return NPC.NL1;
end

function GetFocusNL2(playerid)
	return NPC.NL2;
end

function GetFocusNLBAR(playerid)
	return NPC.NLBAR;
end


function ShowMenuNPC(playerid)
ShowMenuTexture(playerid);
	if GetFocus(playerid) == GetFocusBimbol(playerid) then
	showGUIMenu(playerid, "NPC_BIMBOL");
	
	elseif GetFocus(playerid) == GetFocusGateGuard(playerid) then
	showGUIMenu(playerid, "NPC_GateGuard");
	
	elseif GetFocus(playerid) == GetFocusGuard(playerid) then
	showGUIMenu(playerid, "NPC_Guard");
	
	elseif GetFocus(playerid) == GetFocusFireMage(playerid) then
	showGUIMenu(playerid, "NPC_FireMage");
	
	elseif GetFocus(playerid) == GetFocusAriadna(playerid) then
	showGUIMenu(playerid, "NPC_Ariadna");
	
	elseif GetFocus(playerid) == GetFocusWaterMage(playerid) then
	showGUIMenu(playerid, "NPC_WaterMage");
	
	elseif GetFocus(playerid) == GetFocusNL1(playerid) then
	showGUIMenu(playerid, "NPC_NL1");
	elseif GetFocus(playerid) == GetFocusNL2(playerid) then
	showGUIMenu(playerid, "NPC_NL2");
	elseif GetFocus(playerid) == GetFocusNLBAR(playerid) then
	showGUIMenu(playerid, "NPC_NLBAR");
	
	elseif GetFocus(playerid) == GetFocusDIEGO(playerid) then
	showGUIMenu(playerid, "NPC_DIEGO");
end
end

function SwitchDownMenuNPC(playerid)

if GetFocus(playerid) == GetFocusBimbol(playerid) then
	SwithMenuDownBimbol(playerid);
elseif GetFocus(playerid) == GetFocusGateGuard(playerid) then
	SwithMenuDownGateGuard(playerid);
elseif GetFocus(playerid) == GetFocusGuard(playerid) then
	SwithMenuDownGuard(playerid);
elseif GetFocus(playerid) == GetFocusFireMage(playerid) then
	SwithMenuDownFireMage(playerid);
elseif GetFocus(playerid) == GetFocusAriadna(playerid) then
	SwithMenuDownAriadna(playerid);
elseif GetFocus(playerid) == GetFocusWaterMage(playerid) then
	SwithMenuDownWaterMage(playerid);
elseif GetFocus(playerid) == GetFocusNL1(playerid) then
	SwithMenuDownNL1(playerid);
elseif GetFocus(playerid) == GetFocusNL2(playerid) then
	SwithMenuDownNL2(playerid);
elseif GetFocus(playerid) == GetFocusNLBAR(playerid) then
	SwithMenuDownNLBAR(playerid);
	elseif GetFocus(playerid) == GetFocusDIEGO(playerid) then
	SwithMenuDownDIEGO(playerid);
		end
end

function SwitchUpMenuNPC(playerid)

if GetFocus(playerid) == GetFocusBimbol(playerid) then
	SwithMenuUpBimbol(playerid);
elseif GetFocus(playerid) == GetFocusGateGuard(playerid) then
	SwithMenuUpGateGuard(playerid);
elseif GetFocus(playerid) == GetFocusGuard(playerid) then
	SwithMenuUpGuard(playerid);
elseif GetFocus(playerid) == GetFocusFireMage(playerid) then
	SwithMenuUpFireMage(playerid);
elseif GetFocus(playerid) == GetFocusAriadna(playerid) then
	SwithMenuUpAriadna(playerid);
elseif GetFocus(playerid) == GetFocusWaterMage(playerid) then
	SwithMenuUpWaterMage(playerid);
elseif GetFocus(playerid) == GetFocusNL1(playerid) then
	SwithMenuUpNL1(playerid);
elseif GetFocus(playerid) == GetFocusNL2(playerid) then
	SwithMenuUpNL2(playerid);
elseif GetFocus(playerid) == GetFocusNLBAR(playerid) then
	SwithMenuUpNLBAR(playerid);
	elseif GetFocus(playerid) == GetFocusDIEGO(playerid) then
	SwithMenuUpDIEGO(playerid);
		end
end

function InitN174Menus()
createGUIMenu("NPC_BIMBOL", {
	{ "������!" },
    { "�� ���?" },
    { "��� �����������?" },
    { "������ ������� ���� ������?" },
    { "(�����)" },
    }, 255, 255, 255, 255, 255, 0, 3000, 3000, "Font_Old_10_White_Hi.TGA", nil, 5);
	
	createGUIMenu("NPC_GateGuard", {
	{ "������/������ ������" },
    { "(�����)" },
    }, 255, 255, 255, 255, 255, 0, 3000, 3000, "Font_Old_10_White_Hi.TGA", nil, 2);
	
	createGUIMenu("NPC_DIEGO", {
	{ "(�������� �����)" },
    { "(�����)" },
    }, 255, 255, 255, 255, 255, 0, 3000, 3000, "Font_Old_10_White_Hi.TGA", nil, 2);
	
	createGUIMenu("NPC_Ariadna", {
	{ "������� ������?" },
    { "(�����)" },
    }, 255, 255, 255, 255, 255, 0, 3000, 3000, "Font_Old_10_White_Hi.TGA", nil, 2);
	
	createGUIMenu("NPC_FireMage", {
	{ "�����������" },
	{ "��� �����������?" },
	{ "������ ����" },
    { "(�����)" },
    }, 255, 255, 255, 255, 255, 0, 3000, 3000, "Font_Old_10_White_Hi.TGA", nil, 4);
	
	createGUIMenu("NPC_WaterMage", {
	{ "�����������" },
	{ "��� �����������?" },
	{ "������ ����" },
    { "(�����)" },
    }, 255, 255, 255, 255, 255, 0, 3000, 3000, "Font_Old_10_White_Hi.TGA", nil, 4);
	
	createGUIMenu("NPC_Guard", {
	{ "��� �����������?" },
    { "(�����)" },
    }, 255, 255, 255, 255, 255, 0, 3000, 3000, "Font_Old_10_White_Hi.TGA", nil, 2);
	
	createGUIMenu("NPC_NL1", {
	{ "��� �����������?" },
    { "(�����)" },
    }, 255, 255, 255, 255, 255, 0, 3000, 3000, "Font_Old_10_White_Hi.TGA", nil, 2);
	
	createGUIMenu("NPC_NL2", {
	{ "��� �����������?" },
    { "(�����)" },
    }, 255, 255, 255, 255, 255, 0, 3000, 3000, "Font_Old_10_White_Hi.TGA", nil, 2);
	
	createGUIMenu("NPC_NLBAR", {
	{ "������� ����� ��������?" },
	{ "������ ������ | 2 ����� ����" },  --ItFo_Apple
	{ "������ ��� | 5 ������ ����" }, --ItFo_Cheese
	{ "������ ������ | 10 ������ ����" }, --ItFo_Bacon
	{ "������ ���� | 4 ����� ����" }, --ItFo_Bread
	{ "������ ���� | 1 ����� ����" },  --ItFo_Water
	{ "������ ���� | 8 ������ ����" }, --ItFo_Beer
	{ "������ ���� | 10 ������ ����" },  --ItFo_Booze
	{ "������ ���� | 15 ������ ����" }, --ItFo_Wine
	{ "������ �������� | 5 ������ ����" }, --ItFo_Stew
	{ "(�������� �����)" },
    { "(�����)" },
    }, 255, 255, 255, 255, 255, 0, 3000, 3000, "Font_Old_10_White_Hi.TGA", nil, 12);
end

function SwithMenuDownDIEGO(playerid)
switchGUIMenuDown(playerid, "NPC_DIEGO");
end

function SwithMenuUpDIEGO(playerid)
switchGUIMenuUp(playerid, "NPC_DIEGO");
end

function SwithMenuDownBimbol(playerid)
switchGUIMenuDown(playerid, "NPC_BIMBOL");
end

function SwithMenuUpBimbol(playerid)
switchGUIMenuUp(playerid, "NPC_BIMBOL");
end

function SwithMenuDownGateGuard(playerid)
switchGUIMenuDown(playerid, "NPC_GateGuard");
end

function SwithMenuUpGateGuard(playerid)
switchGUIMenuUp(playerid, "NPC_GateGuard");
end

function SwithMenuDownAriadna(playerid)
switchGUIMenuDown(playerid, "NPC_Ariadna");
end

function SwithMenuUpAriadna(playerid)
switchGUIMenuUp(playerid, "NPC_Ariadna");
end

function SwithMenuDownFireMage(playerid)
switchGUIMenuDown(playerid, "NPC_FireMage");
end

function SwithMenuUpFireMage(playerid)
switchGUIMenuUp(playerid, "NPC_FireMage");
end

function SwithMenuDownGuard(playerid)
switchGUIMenuDown(playerid, "NPC_Guard");
end

function SwithMenuUpGuard(playerid)
switchGUIMenuUp(playerid, "NPC_Guard");
end

function SwithMenuDownWaterMage(playerid)
switchGUIMenuDown(playerid, "NPC_WaterMage");
end

function SwithMenuUpWaterMage(playerid)
switchGUIMenuUp(playerid, "NPC_WaterMage");
end

function SwithMenuDownNL1(playerid)
switchGUIMenuDown(playerid, "NPC_NL1");
end

function SwithMenuUpNL1(playerid)
switchGUIMenuUp(playerid, "NPC_NL1");
end

function SwithMenuDownNL2(playerid)
switchGUIMenuDown(playerid, "NPC_NL2");
end

function SwithMenuUpNL2(playerid)
switchGUIMenuUp(playerid, "NPC_NL2");
end

function SwithMenuDownNLBAR(playerid)
switchGUIMenuDown(playerid, "NPC_NLBAR");
end

function SwithMenuUpNLBAR(playerid)
switchGUIMenuUp(playerid, "NPC_NLBAR");
end

function PickMenuOp(playerid)
HideMenuTexture(playerid);
local armor = GetEquippedArmor(playerid);


if isOpenMenu(playerid, "NPC_BIMBOL") then				
			local opmenu1 = getPlayerOptionID(playerid, "NPC_BIMBOL")
			if opmenu1 == 1 then
				GameTextForPlayer(playerid,50,5500,string.format("%s","������: � ���� �� �������"),"Font_Old_10_White_Hi.TGA",255,255,255,2500);
				hideGUIMenu(playerid, "NPC_BIMBOL");
				elseif opmenu1 == 2 then
				local dial = random(3)
				if dial == 0 or dial == 1 then
				GameTextForPlayer(playerid,50,5500,string.format("%s","������: ���� ����� ������, � ������� �������"),"Font_Old_10_White_Hi.TGA",255,255,255,2500);
				hideGUIMenu(playerid, "NPC_BIMBOL");
				elseif dial == 2 or dial == 3 then
				local name = GetPlayerName(playerid);
				GameTextForPlayer(playerid,50,5500,string.format("%s %s%s","������: ���� ����� ������. � ���� �����",name,", �����?"),"Font_Old_10_White_Hi.TGA",255,255,255,2500);
				hideGUIMenu(playerid, "NPC_BIMBOL");
				end
				elseif opmenu1 == 3 then
				GameTextForPlayer(playerid,50,5500,string.format("%s","������: ���� ���� � �������."),"Font_Old_10_White_Hi.TGA",255,255,255,2500);
				hideGUIMenu(playerid, "NPC_BIMBOL");
				elseif opmenu1 == 4 then
				GameTextForPlayer(playerid,50,5500,string.format("%s","������: ������, � ������ � ������ �� ���. � ��� ��� ����� ���������."),"Font_Old_10_White_Hi.TGA",255,255,255,2500);
				hideGUIMenu(playerid, "NPC_BIMBOL");
				elseif opmenu1 == 5 then
				GameTextForPlayer(playerid,50,5500,string.format("%s","������: �����!"),"Font_Old_10_White_Hi.TGA",255,255,255,2500);
				hideGUIMenu(playerid, "NPC_BIMBOL");
				end
	elseif isOpenMenu(playerid, "NPC_GateGuard") then				
	local opmenu2 = getPlayerOptionID(playerid, "NPC_GateGuard")
	if opmenu2 == 1 then
	if armor == "GRD_ARMOR_H" or armor == "GRD_ARMOR_M" or armor == "GRD_ARMOR_" or armor == "STT_ARMOR_H" or armor == "STT_ARMOR_M" or armor == "ITAR_GENERAL" then
				hideGUIMenu(playerid, "NPC_GateGuard");
				local movement = GetNearestMovement(playerid);
		if movement ~= nil then
		
				if movement.status == MovementStatus.STATUS_CLOSED or movement.status == MovementStatus.STATUS_MOVING_TO_CLOSE then
					OpenMovement(movement);
					GameTextForPlayer(playerid,50,5500,string.format("%s","�������!"),"FONT_OLD_10_WHITE_HI.TGA",0,255,0,3000);
				elseif movement.status == MovementStatus.STATUS_OPENED or movement.status == MovementStatus.STATUS_MOVING_TO_OPEN then
					CloseMovement(movement);
					GameTextForPlayer(playerid,50,5500,string.format("%s","�������!"),"FONT_OLD_10_WHITE_HI.TGA",255,255,0,3000);
				end
		end
	else
	GameTextForPlayer(playerid,50,5500,string.format("%s","�������: � �� �������� �����������."),"Font_Old_10_White_Hi.TGA",255,255,255,2500);
	hideGUIMenu(playerid, "NPC_GateGuard");
	end
	elseif opmenu2 == 2 then
	hideGUIMenu(playerid, "NPC_GateGuard");
				end
				
	elseif isOpenMenu(playerid, "NPC_Ariadna") then				
	local opmenu3 = getPlayerOptionID(playerid, "NPC_Ariadna")
	if opmenu3 == 1 then
	GameTextForPlayer(playerid,50,5500,string.format("%s","�������: ������, �������, ���� ������ ���� ������ ��� ��������, �� � �������� �������� ���� ����������"),"Font_Old_10_White_Hi.TGA",255,255,255,2500);
	hideGUIMenu(playerid, "NPC_Ariadna");
	elseif opmenu3 == 2 then
	hideGUIMenu(playerid, "NPC_Ariadna");
	end
	
	elseif isOpenMenu(playerid, "NPC_DIEGO") then				
	local opmenu3 = getPlayerOptionID(playerid, "NPC_DIEGO")
	if opmenu3 == 1 then
	GameTextForPlayer(playerid,500,5500,string.format("%s","(�� �������� �����)"),"Font_Old_10_White_Hi.TGA",0,255,0,2500);
	GiveItem(playerid,"ITMW_2H_AXE_L_01",1);
	hideGUIMenu(playerid, "NPC_DIEGO");
	elseif opmenu3 == 2 then
	hideGUIMenu(playerid, "NPC_DIEGO");
	end
	
	elseif isOpenMenu(playerid, "NPC_FireMage") then	
	local maxhealth = GetPlayerMaxHealth(playerid);
	local health = GetPlayerHealth(playerid);
	local pay = maxhealth-health;
	local heal = CreateSound("MFX_HEAL_CAST.WAV")
	local opmenu4 = getPlayerOptionID(playerid, "NPC_FireMage")
	if opmenu4 == 1 then
	if armor == "ITAR_KDW_L_ADDON" or armor == "ITAR_KDW_H" or armor == "ITAR_KDF_H" or armor == "ITAR_KDF_L" then
	GameTextForPlayer(playerid,50,5500,string.format("%s","�����: �� �����!"),"Font_Old_10_White_Hi.TGA",255,255,255,2500);
	else
	GameTextForPlayer(playerid,50,5500,string.format("%s","�����: ����������, ����������."),"Font_Old_10_White_Hi.TGA",255,255,255,2500);
	end
	hideGUIMenu(playerid, "NPC_FireMage");
	elseif opmenu4 == 2 then
	GameTextForPlayer(playerid,50,5500,string.format("%s","�����: � ������. ���� �� ����� - � ���� ���� ��������."),"Font_Old_10_White_Hi.TGA",255,255,255,2500);
	SendPlayerMessage(playerid,0,255,255,string.format("%s %d","����� �� �������:",pay));
	hideGUIMenu(playerid, "NPC_FireMage");
	elseif opmenu4 == 3 then --���
	if armor == "ITAR_KDW_L_ADDON" or armor == "ITAR_KDW_H" or armor == "ITAR_KDF_H" or armor == "ITAR_KDF_L" then
	GameTextForPlayer(playerid,50,5500,string.format("%s","�����: �� ������� ����� ���� ����."),"Font_Old_10_White_Hi.TGA",255,255,255,2500);
	CompleteHeal(playerid);
	PlayPlayerSound(playerid, heal)
	hideGUIMenu(playerid, "NPC_FireMage");
	else
		if pay == 0 then
		GameTextForPlayer(playerid,50,5500,string.format("%s","�����: �� ���� ������."),"Font_Old_10_White_Hi.TGA",255,255,255,2500);
		hideGUIMenu(playerid, "NPC_FireMage");
		else
		if pay > Player[playerid].bank then
		GameTextForPlayer(playerid,50,5500,string.format("%s","�����: � ���� �� ������� �����."),"Font_Old_10_White_Hi.TGA",255,255,255,2500);
		SendPlayerMessage(playerid,0,255,255,string.format("%s %d","����� �� �������:",pay));
		PlayPlayerSound(playerid, heal)
		hideGUIMenu(playerid, "NPC_FireMage");
		else
	GameTextForPlayer(playerid,50,5500,string.format("%s","�����: �� ������� ����� ���� ����."),"Font_Old_10_White_Hi.TGA",255,255,255,2500);
	Player[playerid].bank = Player[playerid].bank - pay;
	CompleteHeal(playerid);
	hideGUIMenu(playerid, "NPC_FireMage");
	SendPlayerMessage(playerid,0,255,255,string.format("%s %d","�� ��������� �� �������:",pay));
		end
		end
		end
	elseif opmenu4 == 4 then
	hideGUIMenu(playerid, "NPC_FireMage");
	end
	
	elseif isOpenMenu(playerid, "NPC_WaterMage") then	
	local maxhealth = GetPlayerMaxHealth(playerid);
	local health = GetPlayerHealth(playerid);
	local pay = maxhealth-health;
	local heal = CreateSound("MFX_HEAL_CAST.WAV")
	local opmenu6 = getPlayerOptionID(playerid, "NPC_WaterMage")
	if opmenu6 == 1 then
	if armor == "ITAR_KDW_L_ADDON" or armor == "ITAR_KDW_H" or armor == "ITAR_KDF_H" or armor == "ITAR_KDF_L" then
	GameTextForPlayer(playerid,50,5500,string.format("%s","������: �� �����!"),"Font_Old_10_White_Hi.TGA",255,255,255,2500);
	else
	GameTextForPlayer(playerid,50,5500,string.format("%s","������: ����������, ����������."),"Font_Old_10_White_Hi.TGA",255,255,255,2500);
	end
	hideGUIMenu(playerid, "NPC_WaterMage");
	elseif opmenu6 == 2 then
	GameTextForPlayer(playerid,50,5500,string.format("%s","������: � ������. ���� �� ����� - � ���� ���� ��������."),"Font_Old_10_White_Hi.TGA",255,255,255,2500);
	SendPlayerMessage(playerid,0,255,255,string.format("%s %d","����� �� �������:",pay));
	hideGUIMenu(playerid, "NPC_WaterMage");
	elseif opmenu6 == 3 then --���
	if armor == "KDW_ARMOR_A" or armor == "KDW_ARMOR_L" or armor == "KDW_ARMOR_H" or armor == "ITAR_KDW_L" or armor == "ITAR_KDW_H" then
	GameTextForPlayer(playerid,50,5500,string.format("%s","������: �� ������ ���� ������."),"Font_Old_10_White_Hi.TGA",255,255,255,2500);
	CompleteHeal(playerid);
	PlayPlayerSound(playerid, heal)
	hideGUIMenu(playerid, "NPC_WaterMage");
	else
		if pay == 0 then
		GameTextForPlayer(playerid,50,5500,string.format("%s","������: �� ���� ������."),"Font_Old_10_White_Hi.TGA",255,255,255,2500);
		hideGUIMenu(playerid, "NPC_WaterMage");
		else
		if pay > Player[playerid].bank then
		GameTextForPlayer(playerid,50,5500,string.format("%s","������: � ���� �� ������� �����."),"Font_Old_10_White_Hi.TGA",255,255,255,2500);
		SendPlayerMessage(playerid,0,255,255,string.format("%s %d","����� �� �������:",pay));
		PlayPlayerSound(playerid, heal)
		hideGUIMenu(playerid, "NPC_WaterMage");
		else
	GameTextForPlayer(playerid,50,5500,string.format("%s","������: �� ������ ���� ������."),"Font_Old_10_White_Hi.TGA",255,255,255,2500);
	Player[playerid].bank = Player[playerid].bank - pay;
	CompleteHeal(playerid);
	hideGUIMenu(playerid, "NPC_WaterMage");
	SendPlayerMessage(playerid,0,255,255,string.format("%s %d","�� ��������� �� �������:",pay));
		end
		end
		end
	elseif opmenu6 == 4 then
	hideGUIMenu(playerid, "NPC_WaterMage");
	end
	
	elseif isOpenMenu(playerid, "NPC_Guard") then	
	local opmenu5 = getPlayerOptionID(playerid, "NPC_Guard")
	if opmenu5 == 1 then
	GameTextForPlayer(playerid,50,5500,string.format("%s","����: ���� ���� - �� �������."),"Font_Old_10_White_Hi.TGA",255,255,255,2500);
	hideGUIMenu(playerid, "NPC_Guard");
	elseif opmenu5 == 2 then
	hideGUIMenu(playerid, "NPC_Guard");
	end
	
	elseif isOpenMenu(playerid, "NPC_NL1") then	
	local opmenu5 = getPlayerOptionID(playerid, "NPC_NL1")
	if opmenu5 == 1 then
	GameTextForPlayer(playerid,50,5500,string.format("%s","��������: ������."),"Font_Old_10_White_Hi.TGA",255,255,255,2500);
	hideGUIMenu(playerid, "NPC_NL1");
	elseif opmenu5 == 2 then
	hideGUIMenu(playerid, "NPC_NL1");
	end
	
	elseif isOpenMenu(playerid, "NPC_NL2") then	
	local opmenu5 = getPlayerOptionID(playerid, "NPC_NL2")
	if opmenu5 == 1 then
	GameTextForPlayer(playerid,50,5500,string.format("%s","����: ��� �� ����� �����, �� �����."),"Font_Old_10_White_Hi.TGA",255,255,255,2500);
	hideGUIMenu(playerid, "NPC_NL2");
	elseif opmenu5 == 2 then
	hideGUIMenu(playerid, "NPC_NL2");
	end
	
	elseif isOpenMenu(playerid, "NPC_NLBAR") then	
	local opmenu5 = getPlayerOptionID(playerid, "NPC_NLBAR")
	local money = Player[playerid].bank
	if opmenu5 == 1 then
	GameTextForPlayer(playerid,50,5500,string.format("%s","�����: �� ��, �������!"),"Font_Old_10_White_Hi.TGA",255,255,255,2500);
	hideGUIMenu(playerid, "NPC_NLBAR");
	elseif opmenu5 == 2 then
	hideGUIMenu(playerid, "NPC_NLBAR");
		if money >= 2 then
			Player[playerid].bank = Player[playerid].bank - 2
			GiveItem(playerid,"ITFO_APPLE",1);
			GameTextForPlayer(playerid,50,5500,string.format("%s","�� ������ ������"),"Font_Old_10_White_Hi.TGA",0,255,0,2500);
		else
		GameTextForPlayer(playerid,50,5500,string.format("%s","�����: � ���� �� ������� ����"),"Font_Old_10_White_Hi.TGA",255,255,255,2500);
		end
	elseif opmenu5 == 3 then
	hideGUIMenu(playerid, "NPC_NLBAR");
	if money >= 5 then
			Player[playerid].bank = Player[playerid].bank - 5
			GiveItem(playerid,"ITFO_CHEESE",1);
			GameTextForPlayer(playerid,50,5500,string.format("%s","�� ������ ���"),"Font_Old_10_White_Hi.TGA",0,255,0,2500);
		else
		GameTextForPlayer(playerid,50,5500,string.format("%s","�����: � ���� �� ������� ����"),"Font_Old_10_White_Hi.TGA",255,255,255,2500);
		end
	elseif opmenu5 == 4 then
	hideGUIMenu(playerid, "NPC_NLBAR");
	if money >= 10 then
			Player[playerid].bank = Player[playerid].bank - 10
			GiveItem(playerid,"ITFO_BACON",1);
			GameTextForPlayer(playerid,50,5500,string.format("%s","�� ������ ������"),"Font_Old_10_White_Hi.TGA",0,255,0,2500);
		else
		GameTextForPlayer(playerid,50,5500,string.format("%s","�����: � ���� �� ������� ����"),"Font_Old_10_White_Hi.TGA",255,255,255,2500);
		end
	elseif opmenu5 == 5 then
	hideGUIMenu(playerid, "NPC_NLBAR");
	if money >= 4 then
			Player[playerid].bank = Player[playerid].bank - 4
			GiveItem(playerid,"ITFO_BREAD",1);
			GameTextForPlayer(playerid,50,5500,string.format("%s","�� ������ ����"),"Font_Old_10_White_Hi.TGA",0,255,0,2500);
		else
		GameTextForPlayer(playerid,50,5500,string.format("%s","�����: � ���� �� ������� ����"),"Font_Old_10_White_Hi.TGA",255,255,255,2500);
		end
	elseif opmenu5 == 6 then
	hideGUIMenu(playerid, "NPC_NLBAR");
	if money >= 1 then
			Player[playerid].bank = Player[playerid].bank - 1
			GiveItem(playerid,"ITFO_WATER",1);
			GameTextForPlayer(playerid,50,5500,string.format("%s","�� ������ ������� ����"),"Font_Old_10_White_Hi.TGA",0,255,0,2500);
		else
		GameTextForPlayer(playerid,50,5500,string.format("%s","�����: � ���� �� ������� ����"),"Font_Old_10_White_Hi.TGA",255,255,255,2500);
		end
	elseif opmenu5 == 7 then
	hideGUIMenu(playerid, "NPC_NLBAR");
	if money >= 8 then
			Player[playerid].bank = Player[playerid].bank - 8
			GiveItem(playerid,"ITFO_BEER",1);
			GameTextForPlayer(playerid,50,5500,string.format("%s","�� ������ ����"),"Font_Old_10_White_Hi.TGA",0,255,0,2500);
		else
		GameTextForPlayer(playerid,50,5500,string.format("%s","�����: � ���� �� ������� ����"),"Font_Old_10_White_Hi.TGA",255,255,255,2500);
		end
	elseif opmenu5 == 8 then
	hideGUIMenu(playerid, "NPC_NLBAR");
	if money >= 10 then
			Player[playerid].bank = Player[playerid].bank - 10
			GiveItem(playerid,"ITFO_BOOZE",1);
			GameTextForPlayer(playerid,50,5500,string.format("%s","�� ������ ����"),"Font_Old_10_White_Hi.TGA",0,255,0,2500);
		else
		GameTextForPlayer(playerid,50,5500,string.format("%s","�����: � ���� �� ������� ����"),"Font_Old_10_White_Hi.TGA",255,255,255,2500);
		end
	elseif opmenu5 == 9 then
	hideGUIMenu(playerid, "NPC_NLBAR");
	if money >= 15 then
			Player[playerid].bank = Player[playerid].bank - 15
			GiveItem(playerid,"ITFO_WINE",1);
			GameTextForPlayer(playerid,50,5500,string.format("%s","�� ������ ����"),"Font_Old_10_White_Hi.TGA",0,255,0,2500);
		else
		GameTextForPlayer(playerid,50,5500,string.format("%s","�����: � ���� �� ������� ����"),"Font_Old_10_White_Hi.TGA",255,255,255,2500);
		end
	elseif opmenu5 == 10 then
	hideGUIMenu(playerid, "NPC_NLBAR");
	if money >= 5 then
			Player[playerid].bank = Player[playerid].bank - 5
			GiveItem(playerid,"ITFO_STEW",1);
			GameTextForPlayer(playerid,50,5500,string.format("%s","�� ������ ��������"),"Font_Old_10_White_Hi.TGA",0,255,0,2500);
		else
		GameTextForPlayer(playerid,50,5500,string.format("%s","�����: � ���� �� ������� ����"),"Font_Old_10_White_Hi.TGA",255,255,255,2500);
		end
	elseif opmenu5 == 11 then
	hideGUIMenu(playerid, "NPC_NLBAR");
	GameTextForPlayer(playerid,50,5500,string.format("%s","(�� �������� �����)"),"Font_Old_10_White_Hi.TGA",0,255,0,2500);
	GiveItem(playerid,"ITMW_2H_AXE_L_01",1);
	elseif opmenu5 == 12 then
	hideGUIMenu(playerid, "NPC_NLBAR");
	
	end
end
end