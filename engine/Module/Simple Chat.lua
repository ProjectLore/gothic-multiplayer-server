EnableChat(0);
Player = {};

function InitSimpleChatVars(playerid)
	Player[playerid] = {};
	Player[playerid].Chat = true;
end
AddEvent(InitSimpleChatVars, "OnPlayerConnect");

function PlayerWritesSomethingForNewChat(playerid, text)
	if playerid and text then
		if IsChatForPlayerEnabled(playerid) then
			SendMessageToAll(255, 255, 255, GetPlayerName(playerid)..":"..text);
		end
	end
end
AddEvent(PlayerWritesSomethingForNewChat, "OnPlayerText");

function EnablePlayerChat(playerid, value)
	print("function");
	print(playerid, value);
	if playerid and value ~= nil then
		if type(value) == "number" then
			value = value >= 1 ;
		end	
		print(value);
		Player[playerid].Chat = value;
	end
end

function IsChatForPlayerEnabled(playerid)
	if playerid then
		return Player[playerid].Chat;
	end
end

print("loaded");