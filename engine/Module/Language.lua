
local Version = "v0.2"
local Player = {};
local String = {};
local hash_RU = "3451eabfda92c7dbdf08c13e710c6719";
local hash_DE = "a463052cc32a9412feb9f869f188cc09";
local hash_PL = "f91707d73ec1198b8a1c6db87190b58e";
local autolanguage = true;
local ServerLanguage = "NONE";
-- 1 / DE / German 	= Deutsch
-- 2 / EN / English = English
-- 3 / PL / Polish 	= Polnisch
-- 4 / RU / Russian = Russian
-- Sprachenkürzel nach RFC 1766 =  Diese Sprachenkürzel basieren auf den Normen nach unten ISO 639-1 zur Kurzbezeichnung von Sprachen und nach unten ISO 3166-1 zur Kurzbezeichnung von Ländern.

function InitLanguageVars(playerid)
	Player[playerid] = {};
	Player[playerid].Language = GetDefaultLanguage();
	if autolanguage then
		GetMD5File(playerid, string.upper("_work\\data\\Textures\\_compiled\\Font_OLD_10_White.fnt"));
	end
end
AddEvent(InitLanguageVars, "OnPlayerConnect");

function EnableAutoLanguageCheck( value )
	if value and type(value) == "boolen" then
		autolanguage = value;
	end
end

function InitTranslation()
	String["DE"]=require("engine/Include/Language/DE");
	String["PL"]=require("engine/Include/Language/PL");
	String["RU"]=require("engine/Include/Language/RU");
end
AddEvent(InitTranslation, "OnServerStart");

function SetDefaultLanguage(lan)
	if type(lan) == "number" then
		if lan == 1 then
			ServerLanguage = "DE"; return true
		elseif lan == 2 then
			ServerLanguage = "EN"; return true
		elseif lan == 3 then 
			ServerLanguage = "PL"; return true
		elseif lan == 4 then
			ServerLanguage = "RU"; return true
		end
	elseif type(lan) == "string" then
		lan = string.upper(lan);
		if lan == "DE" or lan == "GERMAN" then
			ServerLanguage = "DE"; return true
		elseif lan == "EN" or lan == "ENGLISH" then
			ServerLanguage = "EN"; return true
		elseif lan == "PL" or lan == "POLISH" then
			ServerLanguage = "PL"; return true
		elseif lan == "RU" or lan == "RUSSIAN" then
			ServerLanguage = "RU"; return true
		end
	end
	return false
end

function AutoCheckLanguage(playerid, pathFile, hash)
	if string.upper(pathFile) ==  string.upper("_work\\data\\Textures\\_compiled\\Font_OLD_10_White.fnt") then
		if hash == hash_DE then
			SetPlayerLanguage(playerid, "DE");
		elseif hash == hash_PL then
			SetPlayerLanguage(playerid, "PL");
		elseif hash == hash_RU then
			SetPlayerLanguage(playerid, "RU");
		else
			SetPlayerLanguage(playerid, "EN");
		end
	end
end
AddEvent(AutoCheckLanguage, "OnPlayerMD5File");

function GetStringTranslation(text, lang)
	if text and lang then
		if lang == "DE" then
			if String["DE"][text] then return String["DE"][text] end
		elseif lang == "PL" then
			if String["PL"][text] then return String["PL"][text] end
		elseif lang == "RU" then
			if String["RU"][text] then return String["RU"][text] end
		end
	end
	return text;
end

function GetDefaultLanguage()
	return ServerLanguage;
end

function SetPlayerLanguage(playerid, ...)
	if playerid and arg[1] then
		if type(arg[1]) == "number" then
		if arg[1] == 1 then
			OnPlayerChangeLanguage(playerid, "DE", Player[playerid].Language);
			Player[playerid].Language = "DE"; return true
		elseif arg[1] == 2 then
			OnPlayerChangeLanguage(playerid, "EN", Player[playerid].Language);
			Player[playerid].Language = "EN"; return true
		elseif arg[1] == 3 then
			OnPlayerChangeLanguage(playerid, "PL", Player[playerid].Language);
			Player[playerid].Language = "PL"; return true
		elseif arg[1] == 4 then 
			OnPlayerChangeLanguage(playerid, "RU", Player[playerid].Language);
			Player[playerid].Language = "RU"; return true
		end
		elseif  type(arg[1]) == "string" then
			local lan = string.upper(arg[1]);
			if lan == "DE" or lan == "GERMAN" then
				OnPlayerChangeLanguage(playerid, "DE", Player[playerid].Language);
				Player[playerid].Language = "DE"; return true
			elseif lan == "EN" or lan == "ENGLISH" then
				OnPlayerChangeLanguage(playerid, "EN", Player[playerid].Language);
				Player[playerid].Language = "EN"; return true
			elseif lan == "PL" or lan == "POLISH" then
				OnPlayerChangeLanguage(playerid, "PL", Player[playerid].Language);
				Player[playerid].Language = "PL"; return true
			elseif lan == "RU" or lan == "RUSSIAN" then
				OnPlayerChangeLanguage(playerid, "RU", Player[playerid].Language);
				Player[playerid].Language = "RU"; return true
			end
		end
	end
	return false
end

function GetPlayerLanguage(playerid)
	if playerid then
		return Player[playerid].Language;
	end
end

print("Modul: Language\t\t"..Version.."\tloaded")