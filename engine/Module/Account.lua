--[[
	Account with Character Modul by
				Devel
]]
if not IsModuleLoaded("Scale") then print("Cannot load Module: 'Account' because required module: 'Scale' isnt loaded!"); return end
require("engine/Include/Account/Mysql");

--Strings for the System
local HEADLINE = "Account System";
local FOOTER = "Account System v0.1";
local bbg = "Menu_Choice_Back.TGA"; -- bbg = buttonbackground
local bf = "Inv_Slot_Focus.TGA"; --bf = buttonfocus
local fd = "FONT_DEFAULT.TGA";
local Spawn = {};

function GetPlayerSex(playerid)
	local a, b, c, d = GetPlayerAdditionalVisual(playerid);
	if a == "Hum_Body_Naked0" then return 1 else return 2 end
end

function GetPlayerSkinColor(playerid)
	local a, b, c, d = GetPlayerAdditionalVisual(playerid);
	if b == 0 or b == 4 then return 1 end --Pale
	if b == 1 or b == 5 or b == 8 or b== 9 or b == 10 or b == 11 or b == 12 then return 2 end --Normal
	if b == 2 or b == 6 then return 3 end --Latino
	if b == 3 or b == 7 then return 4 end --Black
end

function CheckForExistingChar(playerid)
	CheckForExistingChar2(playerid);
end

function CheckForCreateChar(playerid)
	CheckForCreateChar2(playerid);
end

local X, Y, WIDTH, HEIGHT = 3225,2825,4975,5450;
local BACKGROUND = CreateTexture(X, Y, WIDTH, HEIGHT,"Menu_Ingame.TGA", "Account");
local BACKGROUND_CREATE = CreateTexture(X+2550,Y-1650,WIDTH+2975,HEIGHT+1525,"FRAME_GMPA.tga");
local BUTTON_LEFT = CreateTexture(X+225, Y+2025, WIDTH-950, HEIGHT-350,bbg, "Account");
local BUTTON_LEFT_FOCUS = CreateTexture(X+175, Y+2025, WIDTH-900, HEIGHT-350,bf, "Account");
local BUTTON_RIGHT = CreateTexture(X+950, Y+2025, WIDTH-225, HEIGHT-350,bbg, "Account");
local BUTTON_RIGHT_FOCUS = CreateTexture(X+900, Y+2025, WIDTH-175, HEIGHT-350,bf, "Account");
local BUTTON = 
{
	[1] =
	{
		BACK = CreateTexture(X+225,Y+400,WIDTH-225,HEIGHT-1975, bbg, "Account");
		FOCUS = CreateTexture(X+175,Y+400,WIDTH-175,HEIGHT-1975,bf, "Account");
	},
	[2] =
	{
		BACK = CreateTexture(X+225,Y+700,WIDTH-225,HEIGHT-1675, bbg, "Account");
		FOCUS = CreateTexture(X+175,Y+700,WIDTH-175,HEIGHT-1675,bf, "Account");
	},
	[3] =
	{
		BACK = CreateTexture(X+225,Y+1000,WIDTH-225,HEIGHT-1375, bbg, "Account");
		FOCUS = CreateTexture(X+175,Y+1000,WIDTH-175,HEIGHT-1375,bf, "Account");
	},
	[4] =
	{
		BACK = CreateTexture(X+225,Y+1300,WIDTH-225,HEIGHT-1075, bbg, "Account");
		FOCUS = CreateTexture(X+175,Y+1300,WIDTH-175,HEIGHT-1075,bf, "Account");
	},
	[5] =
	{
		BACK = CreateTexture(X+225,Y+1600,WIDTH-225,HEIGHT-775, bbg, "Account");
		FOCUS = CreateTexture(X+175,Y+1600,WIDTH-175,HEIGHT-775,bf, "Account");
	},
	[6] =
	{
		BACK = CreateTexture(X+225,Y+1900,WIDTH-225,HEIGHT-475, bbg, "Account");
		FOCUS = CreateTexture(X+175,Y+1900,WIDTH-175,HEIGHT-475,bf, "Account");
	},
};

local SCREEN_MAIN = 1;
local SCREEN_LOGIN = 2;
local SCREEN_REGISTER = 3;
local SCREEN_CHARACTER = 4;
local SCREEN_CREATE = 5;


--Button Types: used for text placement. DONT CHANGE THESE
local TYPE_BUTTON = 1;
local TYPE_INPUT = 2;
local TYPE_SELECT_SIMPLE = 3;
local TYPE_SELECT = 4;

local MALE = 1;
local FEMALE = 2;

local PALE = 1;
local NORMAL = 2;
local LATINO = 3;
local BLACK = 4;

local CREATE_NAME = 1;
local CREATE_GENDER = 2;
local CREATE_SKIN = 3;
local CREATE_HEADMODEL = 4;
local CREATE_HEADTEX = 5;
local CREATE_WALK = 6;
local CREATE_VOICE = 7;
local CREATE_CREATE = 8;
local CREATE_EXIT = 9;

local SCREEN = 
{
	[SCREEN_MAIN] = -- mainscreen
	{
		HEADLINE = true;
		FOOTER = true;
		BUTTON =
		{
			[1]	=
			{
				TYPE = TYPE_BUTTON;
				BACK = BUTTON[1].BACK;
				FOCUS = BUTTON[1].FOCUS;
				TEXT = "Einloggen";
				RESPONSE = SCREEN_LOGIN;
				-- By responsing it calls Screen 2; Login Screen;
			},
			[2] =
			{
				TYPE = TYPE_BUTTON;
				BACK = BUTTON[2].BACK;
				FOCUS = BUTTON[2].FOCUS;
				TEXT = "Registrieren";
				RESPONSE = SCREEN_REGISTER;
				-- By responsing it calls Screen 3; Register Screen;
			},
			[3] =
			{
				TYPE = TYPE_BUTTON;
				BACK = BUTTON[3].BACK;
				FOCUS = BUTTON[3].FOCUS;
				TEXT = "Verlassen";
				RESPONSE = ExitGame;
				-- By responsing it calls the function 'ExitGame' ;
			},
		},
		SHOW = {BACKGROUND},
	},
	[SCREEN_LOGIN] = 
	{
		HEADLINE = true;
		FOOTER = true;
		BUTTON =
		{
			[1]	=
			{
				TYPE = TYPE_INPUT;
				BACK = BUTTON[2].BACK;
				FOCUS = BUTTON[2].FOCUS;
				TEXT = "Accountname";
			},
			[2]	=
			{
				TYPE = TYPE_INPUT;
				BACK = BUTTON[4].BACK;
				FOCUS = BUTTON[4].FOCUS;
				TEXT = "Passwort";
			},
			[3]	=
			{
				TYPE = TYPE_BUTTON;
				BACK = BUTTON_LEFT;
				FOCUS = BUTTON_LEFT_FOCUS;
				TEXT = "Login";
				RESPONSE = AccountLogin;
			},
			[4]	=
			{
				TYPE = TYPE_BUTTON;
				BACK = BUTTON_RIGHT;
				FOCUS = BUTTON_RIGHT_FOCUS;
				TEXT = "Zur�ck";
				RESPONSE = 1;
			},
		},
		SHOW = {BACKGROUND},
	},
	[SCREEN_REGISTER] = 
	{
		HEADLINE = true;
		FOOTER = true;
		BUTTON =
		{
			[1]	=
			{
				TYPE = TYPE_INPUT;
				BACK = BUTTON[1].BACK;
				FOCUS = BUTTON[1].FOCUS;
				TEXT = "Accountname";
			},
			[2]	=
			{
				TYPE = TYPE_INPUT;
				BACK = BUTTON[3].BACK;
				FOCUS = BUTTON[3].FOCUS;
				TEXT = "Passwort";
			},
			[3]	=
			{
				TYPE = TYPE_INPUT;
				BACK = BUTTON[5].BACK;
				FOCUS = BUTTON[5].FOCUS;
				TEXT = "Passwort wiederholen";
			},
			[4]	=
			{
				TYPE = TYPE_BUTTON;
				BACK = BUTTON_LEFT;
				FOCUS = BUTTON_LEFT_FOCUS;
				TEXT = "Login";
				RESPONSE = AccountRegister;
			},
			[5]	=
			{
				TYPE = TYPE_BUTTON;
				BACK = BUTTON_RIGHT;
				FOCUS = BUTTON_RIGHT_FOCUS;
				TEXT = "Zur�ck";
				RESPONSE = 1;
			},
		},
		SHOW = {BACKGROUND},
	},
	[SCREEN_CHARACTER] =
	{
		HEADLINE = false;
		FOOTER = false;
		SHOW = {BACKGROUND_CREATE},
		BUTTON = 
		{
			[1] = 
			{
				TYPE = TYPE_BUTTON;
				BACK = CreateTexture(X+2775,Y-1250,WIDTH+2750,HEIGHT-3000,"Inv_Back_Buy.tga");
				FOCUS = CreateTexture(X+2775,Y-1250,WIDTH+2750,HEIGHT-3000,"Inv_Slot_Focus.TGA");
				TEXT = "+ Charakter erstellen";
				RESPONSE = CheckForExistingChar;
			},
			[2] = 
			{
				TYPE = TYPE_BUTTON;
				BACK = CreateTexture(X+2775,Y+25,WIDTH+2750,HEIGHT-1725,"Inv_Back_Buy.tga");
				FOCUS = CreateTexture(X+2775,Y+25,WIDTH+2750,HEIGHT-1725,"Inv_Slot_Focus.TGA");
				TEXT = "+ Charakter erstellen";
				RESPONSE = CheckForExistingChar;
			},
			[3] = 
			{
				TYPE = TYPE_BUTTON;
				BACK = CreateTexture(X+2775,Y+1300,WIDTH+2750,HEIGHT-450,"Inv_Back_Buy.tga");
				FOCUS = CreateTexture(X+2775,Y+1300,WIDTH+2750,HEIGHT-450,"Inv_Slot_Focus.TGA");
				TEXT = "+ Charakter erstellen";
				RESPONSE = CheckForExistingChar;
			},
			[4] = 
			{
				TYPE = TYPE_BUTTON;
				BACK = CreateTexture(X+2775,Y+2475,WIDTH+2750,HEIGHT+825,"Inv_Back_Buy.tga");
				FOCUS = CreateTexture(X+2775,Y+2475,WIDTH+2750,HEIGHT+825,"Inv_Slot_Focus.TGA");
				TEXT = "+ Charakter erstellen";
				RESPONSE = CheckForExistingChar;
			},
		},
	},
	[SCREEN_CREATE] =
	{
		HEADLINE = false;
		FOOTER = false;
		SHOW = {BACKGROUND_CREATE},
		BUTTON =
		{
			[CREATE_NAME] =
			{
				TYPE = TYPE_INPUT;
				TEXT = "Name:";
				BACK = CreateTexture(6200,1475,7525,1775,"Menu_Choice_Back.TGA");
				FOCUS = CreateTexture(6175,1475,7550,1775,"Inv_Slot_Focus.TGA");
			},
			[CREATE_GENDER] =
			{
				TYPE = TYPE_SELECT;
				TEXT = "Geschlecht:";
				BACK = CreateTexture(6200,2025,7525,2325,"Menu_Choice_Back.TGA");
				FOCUS = CreateTexture(6175,2025,7550,2325,"Inv_Slot_Focus.TGA");
				SELECT = 
				{
					[MALE] = 
					{
						TEXT = "M�nnlich",
						VALUE = "Hum_Body_Naked0",
					},
					[FEMALE] = 
					{
						TEXT = "Weiblich",
						VALUE = "Hum_Body_Babe0",
					},
				},
			},
			[CREATE_SKIN] =
			{
				TYPE = TYPE_SELECT;
				TEXT = "Hautfarbe:";
				BACK = CreateTexture(6200,2575,7525,2875,"Menu_Choice_Back.TGA");
				FOCUS = CreateTexture(6175,2575,7550,2875,"Inv_Slot_Focus.TGA");
				SELECTTABLE = {GetPlayerSex},
				SELECT = 
				{
					[MALE] = 
					{
						[1] =
						{
							TEXT = "blass", --PALE
							VALUE = 0,
						},
						[2] =
						{
							TEXT = "normal", --NORMAL
							VALUE = 1,
						},
						[3] =
						{
							TEXT = "normal", --NORMAL
							VALUE = 10,
						},
						[4] = 
						{
							TEXT = "normal", --NORMAL
							VALUE = 8,
						},
						[5] =
						{
							TEXT = "latino", --LATINO
							VALUE = 2,
						},
						[6] = 
						{
							TEXT = "schwarz", --BLACK
							VALUE = 3,
						},
					},
					[FEMALE] = 
					{
						[1] =
						{
							TEXT = "blass", --PALE
							VALUE = 4,
						},
						[2] =
						{
							TEXT = "normal", --NORMAL
							VALUE = 5,
						},
						[3] = 
						{
							TEXT = "normal", --NORMAL
							VALUE = 11,
						},
						[4] = 
						{
							TEXT = "normal", --NORMAL
							VALUE = 12,
						},
						[5] =
						{
							TEXT = "latino", --LATINO
							VALUE = 6,
						},
						[6] =
						{
							TEXT = "schwarz", --BLACK
							VALUE = 7,
						},
					},
				},
			},
			[CREATE_HEADMODEL] =
			{
				TYPE = TYPE_SELECT_SIMPLE;
				TEXT = "Kopfform";
				BACK = CreateTexture(6200,2925,7525,3275,"Menu_Choice_Back.TGA");
				FOCUS = CreateTexture(6175,2925,7550,3275,"Inv_Slot_Focus.TGA");
				SELECTTABLE = {GetPlayerSex};
				SELECT = 
				{
					[MALE] = 
					{
						"Hum_Head_FatBald",
						"Hum_Head_Fighter",
						"Hum_Head_Pony",
						"Hum_Head_Bald",
						"Hum_Head_Thief",
						"Hum_Head_Psionic",
					},
					[FEMALE] = 
					{
						"Hum_Head_Babe",
						"Hum_Head_FatBald",
						"Hum_Head_Fighter",
						"Hum_Head_Pony",
						"Hum_Head_Bald",
						"Hum_Head_Thief",
						"Hum_Head_Psionic",
					},
				},
			},
			[CREATE_HEADTEX] =
			{
				TYPE = TYPE_SELECT_SIMPLE;
				TEXT = "Gesicht";
				BACK = CreateTexture(6200,3325,7525,3625,"Menu_Choice_Back.TGA");
				FOCUS = CreateTexture(6175,3325,7550,3625,"Inv_Slot_Focus.TGA");
				SELECTTABLE = {GetPlayerSex, GetPlayerSkinColor};
				SELECT = 
				{
					[MALE] = 
					{
						[PALE] =
						{
							19, 39, 41, 42, 43, 44, 44, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 
						},
						[NORMAL] =
						{
							0, 1, 2, 3, 5, 6, 7, 9, 10, 13, 14, 16, 18, 20, 21, 22, 23, 24, 25, 26, 27, 31, 32, 33, 34, 35, 36, 37, 38, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75 , 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92 ,93, 94, 95, 96, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 159
						},
						[LATINO] =
						{
							8, 15, 29, 30, 40, 120, 121, 122, 123, 124, 125, 126, 127, 128,
						},
						[BLACK] =
						{
							4, 11, 12, 17, 28, 129, 130, 131, 132, 133, 134, 135, 136,
						},
					},
					[FEMALE] = 
					{
						[PALE] =
						{
							151,
						},
						[NORMAL] =
						{
							137, 138, 139, 140, 143, 144, 145, 146, 147, 148, 149, 150, 152, 153, 154, 155, 156,
						},
						[LATINO] =
						{
							141, 158,
						},
						[BLACK] =
						{
							142, 157
						},
					},
				},
			},
			[CREATE_WALK] =
			{
				TYPE = TYPE_SELECT;
				TEXT = "Gangart:";
				BACK = CreateTexture(6200,3875,7525,4175,"Menu_Choice_Back.TGA");
				FOCUS = CreateTexture(6175,3875,7550,4175,"Inv_Slot_Focus.TGA");
				SELECTTABLE = {GetPlayerSex};
				SELECT = 
				{
					[MALE] = 
					{
						[1] = 
						{
							TEXT = "Standart",
							VALUE = "NULL",
						},
						[2] = 
						{
							TEXT = "Arrogant",
							VALUE = "HumanS_Arrogance.mds",
						},
						[3] = 
						{
							TEXT = "R�ckhaltend",
							VALUE = "Humans_Mage.mds",
						},
						[4] = 
						{
							TEXT = "Relaxed",
							VALUE = "HumanS_Relaxed.mds",
						},
						[5] = 
						{
							TEXT = "M�de",
							VALUE = "Humans_Tired.mds",
						},
						[6] =
						{
							TEXT = "Millit�risch",
							VALUE = "HumanS_Militia.mds",
						},
					},
					[FEMALE] =
					{
						[1] = 
						{
							TEXT = "Standart",
							VALUE = "NULL",
						},
						[2] = 
						{
							TEXT = "Arrogant",
							VALUE = "HumanS_Arrogance.mds",
						},
						[3] = 
						{
							TEXT = "Weiblich",
							VALUE = "Humans_Babe.mds",
						},
						[4] = 
						{
							TEXT = "R�ckhaltend",
							VALUE = "Humans_Mage.mds",
						},
						[5] = 
						{
							TEXT = "Relaxed",
							VALUE = "HumanS_Relaxed.mds",
						},
						[6] = 
						{
							TEXT = "M�de",
							VALUE = "Humans_Tired.mds",
						},
						[7] =
						{
							TEXT = "Millit�risch",
							VALUE = "HumanS_Militia.mds",
						},
					},
				},
			},
			[CREATE_VOICE] =
			{
				TYPE = TYPE_SELECT_SIMPLE;
				TEXT = "Stimme";
				BACK = CreateTexture(6200,4225,7525,4525,"Menu_Choice_Back.TGA");
				FOCUS = CreateTexture(6175,4225,7550,4525,"Inv_Slot_Focus.TGA");
				SELECTTABLE = {GetPlayerSex};
				SELECT = 
				{
					[MALE] = 
					{
						1, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14,
					},
					[FEMALE] =
					{
						16, 17,
					},
				},
			},
			[CREATE_CREATE] = 
			{
				TYPE = TYPE_BUTTON;
				TEXT = "Erstellen";
				BACK = CreateTexture(6200,6200,7525,6500,"Menu_Choice_Back.TGA");
				FOCUS = CreateTexture(6175,6200,7550,6500,"Inv_Slot_Focus.TGA");
				RESPONSE = CheckForCreateChar;
			},
			[CREATE_EXIT] =
			{
				TYPE = TYPE_BUTTON;
				TEXT = "Zur�ck";
				BACK = CreateTexture(6200,6600,7525,6900,"Menu_Choice_Back.TGA");
				FOCUS = CreateTexture(6175,6600,7550,6900,"Inv_Slot_Focus.TGA");
				RESPONSE = CheckForExistingChar;
			},
		},
	}
};
local SHOW = {};
local HIDE = {};

local Player = {};

local function RemoveIndex(tab, value)
	for k,v in pairs(tab) do
		if v == value then
			tab[k] = nil;
		end
	end
end

local function FindIndex(tab,value)
	for k,v in pairs(tab) do
		if v == value then
			return k;
		end
	end
	return false
end

local function CalculateShowAndHideTexture(from, to)
	if from and to and type(from)=="table" and type(to)=="table" then
		local show = to;
		local hide = {};
		for k,v in pairs(from) do
			local index = FindIndex(to,v);
			if not index then
				table.insert(hide, v);
			end
		end
		return show, hide;
	end
end

function CalculateTexturesForAcc()
	for k in pairs(SCREEN) do
		for v in pairs(SCREEN[k].BUTTON) do
			if SCREEN[k].BUTTON[v].BACK then
				table.insert(SCREEN[k].SHOW, SCREEN[k].BUTTON[v].BACK);
			end
		end
		SCREEN[k].HIDE = SCREEN[k].SHOW;
	end
	for k in pairs(SCREEN) do
		SHOW[k] = {};
		HIDE[k] = {};
		for v in pairs(SCREEN) do	
			if k ~= v then
				local t, b = CalculateShowAndHideTexture(SCREEN[k].SHOW, SCREEN[v].SHOW);
				SHOW[k][v] = t;
				HIDE[k][v] = b;
			end
		end
	end
end
AddEvent(CalculateTexturesForAcc, "OnServerStart");

function InitPlayerVarsForAccAndChar(playerid)
	Player[playerid] = {};
	Player[playerid].Accountname = nil;
	Player[playerid].LoggedIn = false;
	Player[playerid].Menu = false;
	Player[playerid].ListItem = 1;
	Player[playerid].Screen = 1;
	Player[playerid].Char = {};
	Player[playerid].SelectPos = {};
	Player[playerid].Draws = {};
	Player[playerid].Draws.Screen = {};
	
	for k in pairs(SCREEN) do
		Player[playerid].Draws.Screen[k]={};
		Player[playerid].Draws.Screen[k].All = {};
		Player[playerid].Draws.Screen[k].Button = {};
		Player[playerid].SelectPos[k] = {};
		Player[playerid].SelectPos[k].Button = {};
		for v in pairs(SCREEN[k].BUTTON) do
			Player[playerid].SelectPos[k].Button[v] = 1;
			Player[playerid].Draws.Screen[k].Button[v]={};
		end
	end
end
AddEvent(InitPlayerVarsForAccAndChar, "OnPlayerConnect");

function SetPlayerAccountname(playerid, name)
	if playerid and name then
		Player[playerid].Accountname = name;
	end
end

function GetPlayerAccountname(playerid)
	if playerid then
		return Player[playerid].Accountname;
	end	
	return false
end

function SetPlayerLoggedIn(playerid)
	if playerid then
		Player[playerid].LoggedIn = true;
	end
end

function IsPlayerLoggedIn(playerid)
	if playerid then
		return Player[playerid].LoggedIn;
	end
	return false
end

local function CreateDraws(playerid)
	if Player[playerid].Draws.Headline then
		local x, y = SetAlignText(playerid, "Font_Old_20_White_Hi.TGA", HEADLINE,X, WIDTH,Y-200,Y, "middle", "middle");
		UpdatePlayerDraw(playerid, Player[playerid].Draws.Headline, x, y, HEADLINE, "Font_Old_20_White_Hi.TGA", 255, 255, 255, "Account");
	else
		local x, y = SetAlignText(playerid, "Font_Old_20_White_Hi.TGA", HEADLINE,X, WIDTH,Y-200,Y, "middle", "middle");
		Player[playerid].Draws.Headline = CreatePlayerDraw(playerid, x, y, HEADLINE, "Font_Old_20_White_Hi.TGA", 255, 255, 255, "Account");
		if Player[playerid].Draws.Headline == -1 then
			print("FATAL ERROR: Can not create player draw!");
		end
	end
	if Player[playerid].Draws.Footer then
		local x, y = SetAlignText(playerid, fd, FOOTER,X, WIDTH,Y+2350,HEIGHT, "middle", "middle");
		UpdatePlayerDraw(playerid, Player[playerid].Draws.Footer, x, y, FOOTER, fd, 255, 255, 255, "Account");
	else
		local x, y = SetAlignText(playerid, fd, FOOTER,X, WIDTH,Y+2350,HEIGHT, "middle", "middle");
		Player[playerid].Draws.Footer = CreatePlayerDraw(playerid, x, y, FOOTER, fd, 255, 255, 255, "Account");
		if Player[playerid].Draws.Footer == -1 then
			print("FATAL ERROR: Can not create player draw!");
		end
	end
	if not Player[playerid].Draws.Left then
		Player[playerid].Draws.Left = CreatePlayerDraw(playerid, 0, 0, "<=", "Font_Old_20_White_Hi.TGA", 255, 255, 255, "Account");
		if Player[playerid].Draws.Left == -1 then
			print("FATAL ERROR: Can not create player draw!");
		end
	end
	if not Player[playerid].Draws.Right then
		Player[playerid].Draws.Right = CreatePlayerDraw(playerid, 0, 0, "=>", "Font_Old_20_White_Hi.TGA", 255, 255, 255, "Account");
		if Player[playerid].Draws.Right == -1 then
			print("FATAL ERROR: Can not create player draw!");
		end
	end
	for k in pairs(SCREEN) do
		if SCREEN[k].HEADLINE 	then table.insert(Player[playerid].Draws.Screen[k].All, Player[playerid].Draws.Headline); end
		if SCREEN[k].FOOTER		then table.insert(Player[playerid].Draws.Screen[k].All, Player[playerid].Draws.Footer);	end
		for v in pairs(SCREEN[k].BUTTON) do
			if SCREEN[k].BUTTON[v].TEXT then
				local id = Player[playerid].Draws.Screen[k].Button[v].Text;
				local typ = SCREEN[k].BUTTON[v].TYPE;
				local x, y, width, height, x_align, y_align;
				local info = GetTextureInfo(SCREEN[k].BUTTON[v].BACK)
				if typ == TYPE_BUTTON or typ == TYPE_SELECT_SIMPLE then
					x,y,width, height = info.X, info.Y, info.Width, info.Height;
					x_align = "middle";
					y_align = "middle";
				elseif typ == TYPE_INPUT then
					x,y,width, height = info.X, info.Y-200, info.Width, info.Y;
					x_align = "left";
					y_align = "down";
					local x, y = SetAlignText(playerid, fd, "",info.X, info.Width,info.Y,info.Height, "left", "middle");
					if Player[playerid].Draws.Screen[k].Button[v].Input then
						UpdatePlayerDraw(playerid, Player[playerid].Draws.Screen[k].Button[v].Input, x, y, "", fd, 255, 255, 255, "Account");
					else
						Player[playerid].Draws.Screen[k].Button[v].Input = CreatePlayerDraw(playerid, x,y, "", fd, 255, 255, 255, "Account");
						table.insert(Player[playerid].Draws.Screen[k].All, Player[playerid].Draws.Screen[k].Button[v].Input);
					end
				elseif typ == TYPE_SELECT then
					x,y,width, height = info.X, info.Y-200, info.Width, info.Y;
					x_align = "left";
					y_align = "down";
					local text;
					if SCREEN[k].BUTTON[v].SELECTTABLE then
						local t = table.copy(SCREEN[k].BUTTON[v]);
						for l in pairs(SCREEN[k].BUTTON[v].SELECTTABLE) do
							t.SELECT = t.SELECT[SCREEN[k].BUTTON[v].SELECTTABLE[l](playerid)];
							if t.SELECT[1].TEXT then
								text = t.SELECT[1].TEXT;
							end
						end	
					else
						text = SCREEN[k].BUTTON[v].SELECT[1].TEXT;
					end
					local x, y = SetAlignText(playerid, fd, text,info.X, info.Width,info.Y,info.Height, "middle", "middle");
					if Player[playerid].Draws.Screen[k].Button[v].Input then
						UpdatePlayerDraw(playerid, Player[playerid].Draws.Screen[k].Button[v].Input, x, y, text, fd, 255, 255, 255, "Account");
					else
						Player[playerid].Draws.Screen[k].Button[v].Input = CreatePlayerDraw(playerid, x,y,text, fd, 255, 255, 255, "Account");
						table.insert(Player[playerid].Draws.Screen[k].All, Player[playerid].Draws.Screen[k].Button[v].Input);
					end
				end
				local x, y = SetAlignText(playerid, fd, SCREEN[k].BUTTON[v].TEXT,x, width,y,height, x_align, y_align);
				if id then
					UpdatePlayerDraw(playerid, id, x, y, SCREEN[k].BUTTON[v].TEXT, fd, 255, 255, 255, "Account");
				else
					Player[playerid].Draws.Screen[k].Button[v].Text = CreatePlayerDraw(playerid, x,y, SCREEN[k].BUTTON[v].TEXT, fd, 255, 255, 255, "Account");
					table.insert(Player[playerid].Draws.Screen[k].All, Player[playerid].Draws.Screen[k].Button[v].Text);
				end
			end
		end
	end
end

local function GetTexturesToShow(from, to)
	if from and to then
		if from == to then
			return SCREEN[from].SHOW, {};
		else
			return SHOW[from][to], HIDE[from][to];
		end
	end
end

function CleanInput(playerid)
	local screen = Player[playerid].Screen;
	for k in pairs(Player[playerid].Draws.Screen[screen].Button) do
		if Player[playerid].Draws.Screen[screen].Button[k].Input then
			local id = Player[playerid].Draws.Screen[screen].Button[k].Input;
			UpdatePlayerDraw(playerid, id, {Text=""});
		end
	end
end

function ChangeScreen(playerid, from, to)
	UpdatePlayerDraw(playerid, Player[playerid].Draws.Screen[from].Button[Player[playerid].ListItem].Text, {Green = 255, Blue = 255});
	if SCREEN[from].BUTTON[Player[playerid].ListItem].FOCUS then
		HideTexture(playerid, SCREEN[from].BUTTON[Player[playerid].ListItem].FOCUS);
	end
	CleanInput(playerid);
	Player[playerid].Screen = to;
	Player[playerid].ListItem = 1;
	UpdatePlayerDraw(playerid, Player[playerid].Draws.Screen[to].Button[1].Text, {Green = 0, Blue = 0});
	if SCREEN[to].BUTTON[1].FOCUS then
		ShowTexture(playerid, SCREEN[to].BUTTON[1].FOCUS);
	end
	local show, hide = GetTexturesToShow(from, to);
	for k,v in pairs(show) do
		ShowTexture(playerid, v);
	end
	for k,v in pairs(hide) do
		HideTexture(playerid, v);
	end
	for k,v in pairs(Player[playerid].Draws.Screen[to].Button) do
		local ID = Player[playerid].Draws.Screen[to].Button[k].Text;
		if ID then
			ShowPlayerDraw(playerid, ID);
		end	
		ID = Player[playerid].Draws.Screen[to].Button[k].Input;
		if ID then
			ShowPlayerDraw(playerid, ID);
		end
	end
	for k,v in pairs(Player[playerid].Draws.Screen[from].Button) do
		local ID = Player[playerid].Draws.Screen[from].Button[k].Text;
		if ID then
			HidePlayerDraw(playerid, ID);
		end	
		ID = Player[playerid].Draws.Screen[from].Button[k].Input;
		if ID then
			HidePlayerDraw(playerid, ID);
		end
	end
	if not SCREEN[to].HEADLINE then
		HidePlayerDraw(playerid, Player[playerid].Draws.Headline);
	end
	if not SCREEN[to].FOOTER then
		HidePlayerDraw(playerid, Player[playerid].Draws.Footer);
	end
	if to == SCREEN_CREATE then
		for k,v in pairs(Player[playerid].Char) do
			local ID = SCREEN[SCREEN_CREATE].BUTTON[k].BACK;
			local info = GetTextureInfo(ID);
			local x, y = SetAlignText(playerid, fd, v,info.X, info.Width,info.Y,info.Height, "middle", "middle");
			local r,g,b = 255, 255, 255;
			if k == 1 then r,g,b = 255, 255, 0; end
			UpdatePlayerDraw(playerid, Player[playerid].Draws.Screen[SCREEN_CREATE].Button[k].Text, {Text=v, X=x, Y=y, Red=r, Green=g, Blue=b});
		end
	end
end

function StartAccountSytem(playerid)
	for v,k in pairs(SCREEN[Player[playerid].Screen].SHOW) do
		ShowTexture(playerid, k);
	end
	CreateDraws(playerid);
	Player[playerid].Menu = true;
	FreezePlayer(playerid, 1);
	UpdatePlayerDraw(playerid, Player[playerid].Draws.Screen[Player[playerid].Screen].Button[1].Text, {Green = 0, Blue = 0});
	if SCREEN[Player[playerid].Screen].BUTTON[1].FOCUS then
		ShowTexture(playerid, SCREEN[Player[playerid].Screen].BUTTON[1].FOCUS);
	end
	if SCREEN[Player[playerid].Screen].BUTTON[1].TYPE == TYPE_SELECT or SCREEN[Player[playerid].Screen].BUTTON[1].TYPE == TYPE_SELECT_SIMPLE then
		local info = GetTextureInfo(SCREEN[screen].BUTTON[nb].FOCUS);
		local x, y = SetAlignText(playerid, "Font_Old_20_White_Hi.TGA", "<=",info.X - 75, info.X,info.Y,info.Height, "left", "middle");
		UpdatePlayerDraw(playerid, Player[playerid].Draws.Left, {X = x, Y = y});
		x, y = SetAlignText(playerid, "Font_Old_20_White_Hi.TGA", "=>",info.Width, info.Width + 75,info.Y,info.Height, "right", "middle");
		UpdatePlayerDraw(playerid, Player[playerid].Draws.Right, {X = x, Y = y});
		ShowPlayerDraw(playerid, Player[playerid].Draws.Left);
		ShowPlayerDraw(playerid, Player[playerid].Draws.Right);
	end
	for k,v in pairs(Player[playerid].Draws.Screen[Player[playerid].Screen].All) do
		ShowPlayerDraw(playerid, v);
	end
	--SetPlayerSpawnSelection(playerid);
end

local function UpdateButton(playerid, ob, nb)
	local screen = Player[playerid].Screen;
	UpdatePlayerDraw(playerid, Player[playerid].Draws.Screen[screen].Button[ob].Text, {Green = 255, Blue = 255});
	if SCREEN[screen].BUTTON[ob].FOCUS then
		HideTexture(playerid, SCREEN[screen].BUTTON[ob].FOCUS);
	end
	if (SCREEN[screen].BUTTON[ob].TYPE == TYPE_SELECT or SCREEN[screen].BUTTON[ob].TYPE == TYPE_SELECT_SIMPLE) and not (SCREEN[screen].BUTTON[nb].TYPE == TYPE_SELECT or SCREEN[screen].BUTTON[nb].TYPE == TYPE_SELECT_SIMPLE) then 
		HidePlayerDraw(playerid, Player[playerid].Draws.Left);
		HidePlayerDraw(playerid, Player[playerid].Draws.Right);
	end
	if (SCREEN[screen].BUTTON[ob].TYPE == TYPE_SELECT or SCREEN[screen].BUTTON[ob].TYPE == TYPE_SELECT_SIMPLE) and SCREEN[screen].BUTTON[nb].TYPE == TYPE_SELECT or SCREEN[screen].BUTTON[nb].TYPE == TYPE_SELECT_SIMPLE then
		local info = GetTextureInfo(SCREEN[screen].BUTTON[nb].FOCUS);
		local x, y = SetAlignText(playerid, "Font_Old_20_White_Hi.TGA", "<=",info.X - 75, info.X,info.Y,info.Height, "right", "middle");
		UpdatePlayerDraw(playerid, Player[playerid].Draws.Left, {X = x, Y = y});
		x, y = SetAlignText(playerid, "Font_Old_20_White_Hi.TGA", "=>",info.Width, info.Width + 75,info.Y,info.Height, "left", "middle");
		UpdatePlayerDraw(playerid, Player[playerid].Draws.Right, {X = x, Y = y});
	end
	if (not (SCREEN[screen].BUTTON[ob].TYPE == TYPE_SELECT) or not ( SCREEN[screen].BUTTON[ob].TYPE == TYPE_SELECT_SIMPLE)) and (SCREEN[screen].BUTTON[nb].TYPE == TYPE_SELECT or SCREEN[screen].BUTTON[nb].TYPE == TYPE_SELECT_SIMPLE) then 
		local info = GetTextureInfo(SCREEN[screen].BUTTON[nb].FOCUS);
		local x, y = SetAlignText(playerid, "Font_Old_20_White_Hi.TGA", "<=",info.X - 75, info.X,info.Y,info.Height, "right", "middle");
		UpdatePlayerDraw(playerid, Player[playerid].Draws.Left, {X = x, Y = y});
		x, y = SetAlignText(playerid, "Font_Old_20_White_Hi.TGA", "=>",info.Width, info.Width + 75,info.Y,info.Height, "left", "middle");
		UpdatePlayerDraw(playerid, Player[playerid].Draws.Right, {X = x, Y = y});
		ShowPlayerDraw(playerid, Player[playerid].Draws.Left);
		ShowPlayerDraw(playerid, Player[playerid].Draws.Right);
	end
	
	UpdatePlayerDraw(playerid, Player[playerid].Draws.Screen[screen].Button[nb].Text, {Green = 0, Blue = 0});
	if SCREEN[screen].BUTTON[nb].FOCUS then
		ShowTexture(playerid, SCREEN[screen].BUTTON[nb].FOCUS);
	end
end

function AccountControl(playerid, keyDown, keyUp)
	if Player[playerid].Menu then
		if keyDown == KEY_UP then
			if Player[playerid].ListItem > 1 then
				UpdateButton(playerid, Player[playerid].ListItem, Player[playerid].ListItem -1);
				Player[playerid].ListItem = Player[playerid].ListItem - 1;
			end
			return
		end
		if keyDown == KEY_DOWN then
			if Player[playerid].ListItem < table.getn(SCREEN[Player[playerid].Screen].BUTTON) then
				UpdateButton(playerid, Player[playerid].ListItem, Player[playerid].ListItem + 1);
				Player[playerid].ListItem = Player[playerid].ListItem + 1;
			end
			return
		end
		if keyDown == KEY_LEFT then
			if SCREEN[Player[playerid].Screen].BUTTON[Player[playerid].ListItem].TYPE == TYPE_SELECT or SCREEN[Player[playerid].Screen].BUTTON[Player[playerid].ListItem].TYPE == TYPE_SELECT_SIMPLE then
				local b = table.copy(SCREEN[Player[playerid].Screen].BUTTON[Player[playerid].ListItem]);
				local pos = Player[playerid].SelectPos[Player[playerid].Screen].Button[Player[playerid].ListItem];
				local t = {};
				if b.SELECTTABLE then
					t = b;
					for l in pairs(SCREEN[Player[playerid].Screen].BUTTON[Player[playerid].ListItem].SELECTTABLE) do
						t.SELECT = t.SELECT[SCREEN[Player[playerid].Screen].BUTTON[Player[playerid].ListItem].SELECTTABLE[l](playerid)];
					end	
				else
					t.SELECT = b.SELECT;
				end
				if t.SELECT[pos-1] then
					Player[playerid].SelectPos[Player[playerid].Screen].Button[Player[playerid].ListItem] = pos-1;
				else
					Player[playerid].SelectPos[Player[playerid].Screen].Button[Player[playerid].ListItem] = table.getn(t.SELECT);
				end
				if SCREEN[Player[playerid].Screen].BUTTON[Player[playerid].ListItem].TYPE == TYPE_SELECT then
					pos = Player[playerid].SelectPos[Player[playerid].Screen].Button[Player[playerid].ListItem];
					local text = t.SELECT[pos].TEXT;
					local info = GetTextureInfo(SCREEN[Player[playerid].Screen].BUTTON[Player[playerid].ListItem].BACK);
					local x, y = SetAlignText(playerid, fd, text,info.X, info.Width,info.Y,info.Height, "middle", "middle");
					UpdatePlayerDraw(playerid, Player[playerid].Draws.Screen[Player[playerid].Screen].Button[Player[playerid].ListItem].Input, {Text = text, X = x, Y = y});
				end
				if Player[playerid].Screen == SCREEN_CREATE then
					UpdateCreatePlayer(playerid, Player[playerid].ListItem, pos);
				end
			end
			return
		end
		if keyDown == KEY_RIGHT then
			if SCREEN[Player[playerid].Screen].BUTTON[Player[playerid].ListItem].TYPE == TYPE_SELECT or SCREEN[Player[playerid].Screen].BUTTON[Player[playerid].ListItem].TYPE == TYPE_SELECT_SIMPLE then
				local b = table.copy(SCREEN[Player[playerid].Screen].BUTTON[Player[playerid].ListItem]);
				local pos = Player[playerid].SelectPos[Player[playerid].Screen].Button[Player[playerid].ListItem];
				local t = {};
				if b.SELECTTABLE then
					t = b;
					for l in pairs(SCREEN[Player[playerid].Screen].BUTTON[Player[playerid].ListItem].SELECTTABLE) do
						t.SELECT = t.SELECT[SCREEN[Player[playerid].Screen].BUTTON[Player[playerid].ListItem].SELECTTABLE[l](playerid)];
					end	
				else
					t.SELECT = b.SELECT;
				end
				if t.SELECT[pos+1] then
					Player[playerid].SelectPos[Player[playerid].Screen].Button[Player[playerid].ListItem] = pos+1;
				else
					Player[playerid].SelectPos[Player[playerid].Screen].Button[Player[playerid].ListItem] = 1;
				end
				if SCREEN[Player[playerid].Screen].BUTTON[Player[playerid].ListItem].TYPE == TYPE_SELECT then
					pos = Player[playerid].SelectPos[Player[playerid].Screen].Button[Player[playerid].ListItem];
					local text = t.SELECT[pos].TEXT;
					local info = GetTextureInfo(SCREEN[Player[playerid].Screen].BUTTON[Player[playerid].ListItem].BACK);
					local x, y = SetAlignText(playerid, fd, text,info.X, info.Width,info.Y,info.Height, "middle", "middle");
					UpdatePlayerDraw(playerid, Player[playerid].Draws.Screen[Player[playerid].Screen].Button[Player[playerid].ListItem].Input, {Text = text, X = x, Y = y});
				end
				if Player[playerid].Screen == SCREEN_CREATE then
					UpdateCreatePlayer(playerid, Player[playerid].ListItem, pos);
				end
			end
			return
		end
		if keyDown == KEY_RETURN then
			local screen = Player[playerid].Screen;
			local b = Player[playerid].ListItem;
			if SCREEN[screen].BUTTON[b].RESPONSE then
				if type(SCREEN[screen].BUTTON[b].RESPONSE) == "number" then
					ChangeScreen(playerid, screen, SCREEN[screen].BUTTON[b].RESPONSE);
				elseif type(SCREEN[screen].BUTTON[b].RESPONSE) == "function" then
					SCREEN[screen].BUTTON[b].RESPONSE(playerid);
				end
			end
			return
		end
	end
end
AddEvent(AccountControl, "OnPlayerKey");

function PlayerInsertForChat(playerid, text)
	if Player[playerid].Menu then
		if Player[playerid].Draws.Screen[Player[playerid].Screen].Button[Player[playerid].ListItem].Input then
			UpdatePlayerDraw(playerid, Player[playerid].Draws.Screen[Player[playerid].Screen].Button[Player[playerid].ListItem].Input, {Text = text});
		end
	end
end
AddEvent(PlayerInsertForChat, "OnPlayerText");

function table.copy(t)
  local u = { }
  for k, v in pairs(t) do u[k] = v end
  return setmetatable(u, getmetatable(t))
end

function UpdateCreatePlayer(playerid, button, item)
	local bodyModel, bodyTex, headModel, headTex = GetPlayerAdditionalVisual(playerid);
	local gender = GetPlayerSex(playerid);
	local skin = GetPlayerSkinColor(playerid);
	if button == CREATE_GENDER then -- Gender
		bodyModel = SCREEN[SCREEN_CREATE].BUTTON[button].SELECT[item].VALUE;
		bodyTex = SCREEN[SCREEN_CREATE].BUTTON[CREATE_SKIN].SELECT[item][1].VALUE;
		headModel = SCREEN[SCREEN_CREATE].BUTTON[CREATE_HEADMODEL].SELECT[item][1];
		headTex = SCREEN[SCREEN_CREATE].BUTTON[CREATE_HEADTEX].SELECT[item][1][1];
		
		local info = GetTextureInfo(SCREEN[Player[playerid].Screen].BUTTON[CREATE_SKIN].BACK);
		local x, y = SetAlignText(playerid, fd, SCREEN[Player[playerid].Screen].BUTTON[CREATE_SKIN].SELECT[item][1].TEXT,info.X, info.Width,info.Y,info.Height, "middle", "middle");
		UpdatePlayerDraw(playerid, Player[playerid].Draws.Screen[Player[playerid].Screen].Button[CREATE_SKIN].Input, {Text = SCREEN[Player[playerid].Screen].BUTTON[CREATE_SKIN].SELECT[item][1].TEXT, X = x, Y = y});
		
		Player[playerid].SelectPos[SCREEN_CREATE].Button[CREATE_SKIN] = 1;
		Player[playerid].SelectPos[SCREEN_CREATE].Button[CREATE_HEADMODEL] = 1;
		Player[playerid].SelectPos[SCREEN_CREATE].Button[CREATE_HEADTEX] = 1;
	end
	if button == CREATE_SKIN then -- SkinColor
		bodyTex = SCREEN[SCREEN_CREATE].BUTTON[button].SELECT[gender][item].VALUE;
		if bodyTex == 0 or bodyTex == 4 then skin = 1 end --Pale
		if bodyTex == 1 or bodyTex == 5 or bodyTex == 8 or bodyTex== 9 or bodyTex == 10 or bodyTex == 11 or bodyTex == 12 then skin = 2 end --Normal
		if bodyTex == 2 or bodyTex == 6 then skin = 3 end --Latino
		if bodyTex == 3 or bodyTex == 7 then skin = 4 end --Black
		headModel = SCREEN[SCREEN_CREATE].BUTTON[CREATE_HEADMODEL].SELECT[gender][1];
		headTex = SCREEN[SCREEN_CREATE].BUTTON[CREATE_HEADTEX].SELECT[gender][skin][1];
		
		Player[playerid].SelectPos[SCREEN_CREATE].Button[CREATE_HEADMODEL] = 1;
		Player[playerid].SelectPos[SCREEN_CREATE].Button[CREATE_HEADTEX] = 1;
	end 
	if button == CREATE_HEADMODEL then -- HeadModel
		headModel = SCREEN[SCREEN_CREATE].BUTTON[button].SELECT[gender][item];
	end 
	if button == CREATE_HEADTEX then -- HeadTex
		headTex = SCREEN[SCREEN_CREATE].BUTTON[button].SELECT[gender][skin][item];
	end 
	if button == 6 then -- Walk
		
		return
	end 
	if button == 7 then -- Voice
	
		return
	end 
	SetPlayerAdditionalVisual(playerid, bodyModel, bodyTex, headModel, headTex);
end

function UpdatePlayerChars(playerid, name)
	table.insert(Player[playerid].Char, name);
	mysql_UpdatePlayerChars(playerid, table.concat(Player[playerid].Char, ";"));
	SetPlayerSpawnSelection(playerid);
end

function SetPlayerChars(playerid, tab)
	Player[playerid].Char = tab;
end

function SetPlayerSpawnSelection(playerid)
	local from = Player[playerid].Screen;
	if from ~= SCREEN_CHARACTER then
		ChangeScreen(playerid, from, SCREEN_CHARACTER);
	end
	local ID = Player[playerid].Spawn or GetFreeSpawnSelection(playerid);
	if ID ~= -1 then
		Player[playerid].Spawn = ID;
		if Player[playerid].Char[1] then
			LoadPlayer(playerid, Player[playerid].Char[1]);
			SetPlayerPos(playerid, Spawn[ID].X, Spawn[ID].Y, Spawn[ID].Z);
			SetPlayerAngle(playerid, Spawn[ID].Rotation+180);
			SetCameraBehindVob(playerid, Spawn[ID].Camera);
		else
			SetPlayerPos(playerid, Spawn[ID].X, Spawn[ID].Y+1100, Spawn[ID].Z);
			SetPlayerAngle(playerid, Spawn[ID].Rotation+180);
			SetCameraBehindVob(playerid, Spawn[ID].Camera);
		end
	else
		if IsPlayerConnected(playerid) == 1 then
			SetTimerEx("SetPlayerSpawnSelection", 2000, 0, playerid);
			print("no free spawn selection");
		end
	end
end

function GetFreeSpawnSelection(playerid)
	for k in pairs(Spawn) do
		if Spawn[k].Free then
			Spawn[k].Free = false;
			Spawn[k].Player = playerid;
			return k;
		else
			if IsPlayerConnected(Spawn[k].Player) == 0 then
				Spawn[k].Player = playerid;
				return k;
			end
		end
	end
	return -1;
end

function GetPlayerInputDrawsAccount(playerid)
	return Player[playerid].Draws.Screen[Player[playerid].Screen].Button;
end

function CreateSpawnSelection(x, y, z, rot, world)
	if x and y and z and rot then
		world = world or "NEWWORLD\\NEWWORLD.ZEN";
		local spawn = Vob.Create("EVT_CORNERROOMS_LIFT_01.3DS",world, x, y-100, z); spawn:SetRotation(0,rot,0);
		local outside = Vob.Create("EVT_CORNERROOMS_LIFT_01.3DS",world, x, y+1000, z); outside:SetRotation(0,rot,0);
		local cam = Vob.Create("CRDIVEINS.TGA",world, x+50, y-75, z); cam:SetRotation(0,rot,0);
		if spawn == -1 or outside == -1 or cam == -1 then print("Cannot Create SpawnSelection = Cannot Create Vob!"); return end
		local t = {X=x, Y=y, Z=z, Rotation=rot, World = world, Spawn = spawn, Outside = outside, Camera = cam, Free = true};
		table.insert(Spawn, t);
	end
end

function CheckForExistingChar2(playerid)
	local pos = Player[playerid].ListItem;
	if Player[playerid].Char[pos] then
		
	else
		ChangeScreen(playerid, SCREEN_CHARACTER, SCREEN_CREATE);
		UpdateCreatePlayer(playerid, 2, 1);
		local ID = Player[playerid].Spawn;
		SetPlayerPos(playerid, Spawn[ID].X, Spawn[ID].Y, Spawn[ID].Z);
		SetPlayerAngle(playerid, Spawn[ID].Rotation+180);
		SetCameraBehindVob(playerid, Spawn[ID].Camera);
	end
end

function CheckForCreateChar2(playerid)
	local ID = Player[playerid].Draws.Screen[SCREEN_CREATE].Button[CREATE_NAME].Input;
	local info = GetPlayerDrawInfo(playerid, ID);
	local name = info.Text;
	if #name >= 3 then
		if not DoesPlayerExist(name) then
			UpdatePlayerChars(playerid, name);
		else
			-- Existiert bereits
		end
	else
		-- Kein Name!
	
	end
end

print("Module: Account\t\tv0.1\tloaded");