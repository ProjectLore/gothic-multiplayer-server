if not IsModuleLoaded("Scale") then print("Can not load Modul: 'Dialog' , because Modul 'Scale' is not loaded!");return false end
local Player={};

local Button_Left = CreateTexture(3100, 5500, 4000, 5800,"menu_choice_back.Tga", "Hide");
local Button_Left_Focus = CreateTexture(3090, 5500, 4010, 5800,"Inv_slot_focus.Tga", "Hide");

local Button_Middle = CreateTexture(3650, 5500, 4550, 5800,"menu_choice_back.Tga", "Hide");
local Button_Middle_Focus = CreateTexture(3640, 5500, 4560, 5800,"Inv_slot_focus.Tga", "Hide");

local Button_Right = CreateTexture(4200, 5500, 5100, 5800,"menu_choice_back.Tga", "Hide");
local Button_Right_Focus = CreateTexture(4190, 5500, 5110, 5800,"Inv_slot_focus.Tga", "Hide");

local Arrow_Up = CreateTexture(4925, 3150, 5075, 3300,"O.Tga", "Hide");
local Arrow_Up2 = CreateTexture(4925, 3900, 5075, 4050,"O.Tga", "Hide");
local Arrow_Down = CreateTexture(4925, 5250, 5075, 5400,"U.Tga", "Hide");

local BB = CreateTexture(3000, 2700, 5200, 5950,"Menu_ingame.Tga", "Dialog"); -- Gro�e Box
local MB = CreateTexture(3100, 3100, 5100, 5450,"Menu_ingame.Tga", "Dialog"); -- Mittlere Box
local Input = CreateTexture(3100, 3600, 5100, 3800,"menu_choice_back.Tga", "Hide"); -- Input
local Input_Box = CreateTexture(3100, 3850, 5100, 5450,"Menu_ingame.Tga", "Hide"); -- Input Box

DIALOG_STYLE_LISTITEM = 1;
DIALOG_STYLE_MSGBOX = 2;
DIALOG_STYLE_INPUT = 3;
DIALOG_STYLE_INPUT_PASSWORD = 4;

function InitDialogPlayerVars(playerid)
	Player[playerid]={};
	Player[playerid].Menu = false;
	Player[playerid].Style = nil;
	Player[playerid].ID = nil;
	Player[playerid].ListItem = nil;
	Player[playerid].Button = 1;
	Player[playerid].Line = 1;
	Player[playerid].Start = 1;
	Player[playerid].End = nil;
	Player[playerid].Headline = nil;
	Player[playerid].UnderHeadline = nil;
	Player[playerid].MenuLine = {};
	Player[playerid].MenuContent = {};
	Player[playerid].Draw = {};
end
AddEvent(InitDialogPlayerVars, "OnPlayerConnect");

local function CreateButtons(playerid, button1, button2)
	if button1 and button2 then
		local x, y = SetAlignText(playerid, "font_default.tga", button1,3100, 4000,5500,5800, "middle", "middle");
		if Player[playerid].Menu and Player[playerid].Draw.Button1 then
			UpdatePlayerDraw(playerid, Player[playerid].Draw.Button1,x, y, button1, "font_default.tga", 255, 0,0, "Dialog");
		else
			Player[playerid].Draw.Button1 = CreatePlayerDraw(playerid, x, y, button1, "font_default.tga", 255, 0,0, "Dialog");
		end
		Player[playerid].Button = 1;
		ShowTexture(playerid, Button_Left);
		ShowTexture(playerid, Button_Left_Focus);
		x, y = SetAlignText(playerid, "font_default.tga", button2,4200, 5100,5500,5800, "middle", "middle");
		if Player[playerid].Menu and Player[playerid].Draw.Button2 then
			UpdatePlayerDraw(playerid, Player[playerid].Draw.Button2,x, y, button2, "font_default.tga", 255, 255,255, "Dialog");
		else
			Player[playerid].Draw.Button2 = CreatePlayerDraw(playerid, x, y, button2, "font_default.tga", 255, 255,255, "Dialog");
			ShowPlayerDraw(playerid, Player[playerid].Draw.Button2);
		end
		ShowTexture(playerid, Button_Right);
		HideTexture(playerid, Button_Middle);
		HideTexture(playerid, Button_Middle_Focus);
		HideTexture(playerid, Button_Right_Focus);
			
	elseif button1 and not button2 then
		local x, y = SetAlignText(playerid, "font_default.tga", button1,3650, 4550,5500,5800, "middle", "middle");
		if Player[playerid].Menu and Player[playerid].Draw.Button1 then
			UpdatePlayerDraw(playerid, Player[playerid].Draw.Button1,x, y, button1, "font_default.tga", 255, 0,0, "Dialog");
		else
			Player[playerid].Draw.Button1 = CreatePlayerDraw(playerid, x, y, button1, "font_default.tga", 255, 0,0, "Dialog");
		end
		if Player[playerid].Menu and Player[playerid].Draw.Button2 then
			DestroyPlayerDraw(playerid, Player[playerid].Draw.Button2);
			HideTexture(playerid, Button_Left);
			HideTexture(playerid, Button_Left_Focus);
			HideTexture(playerid, Button_Right);
			HideTexture(playerid, Button_Right_Focus);
		end
		Player[playerid].Draw.Button2 = nil;
		Player[playerid].Button = 1;
		ShowTexture(playerid, Button_Middle);
		ShowTexture(playerid, Button_Middle_Focus);
	end
end

local function ArrowUpAndDown(playerid)
	if playerid then
		local tstart = Player[playerid].Start;
		local tend = Player[playerid].End;
		if Player[playerid].Style == DIALOG_STYLE_INPUT or Player[playerid].Style == DIALOG_STYLE_INPUT_PASSWORD then
		if tstart > 1 then ShowTexture(playerid, Arrow_Up2); else HideTexture(playerid, Arrow_Up2); end
		else
		if tstart > 1 then ShowTexture(playerid, Arrow_Up); else HideTexture(playerid, Arrow_Up); end
		end
		if tend < table.getn(Player[playerid].MenuContent) then ShowTexture(playerid, Arrow_Down); else HideTexture(playerid, Arrow_Down); end
	end
end

local function CreateDialog(playerid, info)
	if playerid and info then
		local y = 3150; dev = 2300;
		if (Player[playerid].Style == DIALOG_STYLE_INPUT or Player[playerid].Style == DIALOG_STYLE_INPUT_PASSWORD) and (info.Style == DIALOG_STYLE_LISTITEM or info.Style ~= DIALOG_STYLE_MSGBOX) then
			HideTexture(playerid, Input);
			HideTexture(playerid, Input_Box);
			DestroyPlayerDraw(playerid, Player[playerid].UnderHeadline);
			Player[playerid].UnderHeadline = nil;
		end
		Player[playerid].Style = info.Style;
		if info.Style == DIALOG_STYLE_LISTITEM then
			Player[playerid].ListItem = 1;
			Player[playerid].Line = 1;
			ShowTexture(playerid, "Dialog");
			HideTexture(playerid, Arrow_Up2);
			Player[playerid].Inputtext = nil;
			if Player[playerid].InputtextDraw then
				DestroyPlayerDraw(playerid, Player[playerid].InputtextDraw);
				Player[playerid].InputtextDraw = nil;
			end
			HideTexture(playerid, Input);
			HideTexture(playerid, Input_Box);
		elseif info.Style == DIALOG_STYLE_MSGBOX then
			Player[playerid].ListItem = nil;
			Player[playerid].Line = nil;
			Player[playerid].Inputtext = nil;
			if Player[playerid].InputtextDraw then
				DestroyPlayerDraw(playerid, Player[playerid].InputtextDraw);
				Player[playerid].InputtextDraw = nil;
			end
			ShowTexture(playerid, "Dialog");
			HideTexture(playerid, Arrow_Up2);
			HideTexture(playerid, Input);
			HideTexture(playerid, Input_Box);
		elseif info.Style == DIALOG_STYLE_INPUT or info.Style == DIALOG_STYLE_INPUT_PASSWORD then
			Player[playerid].ListItem = nil;
			Player[playerid].Line = nil;
			y=3900; dev = 1600;
			ShowTexture(playerid, Input);
			ShowTexture(playerid, Input_Box);
			ShowTexture(playerid, BB);
			HideTexture(playerid, Arrow_Up);
			HideTexture(playerid, MB);
			if Player[playerid].InputtextDraw then
				DestroyPlayerDraw(playerid, Player[playerid].InputtextDraw);
				Player[playerid].InputtextDraw = nil;
			end
		else
			return
		end
		if Player[playerid].Headline then
			UpdatePlayerDraw(playerid, Player[playerid].Headline,3150, 2850, info.Headline, "font_old_20_white.tga", 255, 255,255, "Dialog");
		else
			Player[playerid].Headline = CreatePlayerDraw(playerid, 3150, 2850, info.Headline, "font_old_20_white.tga", 255, 255,255, "Dialog");
		end
		if info.Style == DIALOG_STYLE_INPUT or info.Style == DIALOG_STYLE_INPUT_PASSWORD then
			if Player[playerid].UnderHeadline then
				UpdatePlayerDraw(playerid, Player[playerid].UnderHeadline,3150, 3250, info.UnderHeadline, "font_old_20_white.tga", 255, 255,255, "Dialog");
			else
				Player[playerid].UnderHeadline = CreatePlayerDraw(playerid, 3150, 3250, info.UnderHeadline, "font_old_20_white.tga", 255, 255,255, "Dialog");
			end
		end
		local height = GetPlayerTextY(playerid, "font_default.tga");
		local lines = math.floor(dev / height);
		local items = info.ListItem; if info.Style ~= DIALOG_STYLE_LISTITEM then items = FormatText(playerid, info.Text, 1900, "font_default.tga"); end
		for k=1, lines do
			local text = ""; if items[k] then text = tostring(items[k]);else break end
			local r,g,b = 255, 255, 255; if k == 1 and info.Style == DIALOG_STYLE_LISTITEM then r,g,b = 255, 0, 0; end
			if Player[playerid].MenuLine[k] then
				UpdatePlayerDraw(playerid, Player[playerid].MenuLine[k],{Y=y, Text=text, Red=r, Green=g,Blue=b});
			else
				Player[playerid].MenuLine[k] = CreatePlayerDraw(playerid, 3150, y, text, "font_default.tga", r, g,b, "Dialog");
			end
			y = y + height;
		end
		if table.getn(Player[playerid].MenuLine) > lines then
			for k = lines+1, table.getn(Player[playerid].MenuLine) do
				v = Player[playerid].MenuLine[k];
				DestroyPlayerDraw(playerid, v);
				Player[playerid].MenuLine[k] = nil;
			end
		end
		if Player[playerid].MenuContent and table.getn(Player[playerid].MenuContent) > table.getn(items) and table.getn(items) < lines then
			for k = table.getn(items)+1, lines do
				UpdatePlayerDraw(playerid, Player[playerid].MenuLine[k],{Text = ""});
			end
		end
		CreateButtons(playerid, info.Button1, info.Button2);
		ShowPlayerDraw(playerid, "Dialog");
		FreezePlayer(playerid, 1);
		Player[playerid].Start = 1;
		Player[playerid].End = lines;
		Player[playerid].Menu = true;
		Player[playerid].ID = info.ID;
		Player[playerid].MenuContent = items;
		ArrowUpAndDown(playerid);
	end
end

local function UpdateButton(playerid)
	if Player[playerid].Button == 1 then
		ShowTexture(playerid, Button_Left_Focus);
		HideTexture(playerid, Button_Right_Focus);
		UpdatePlayerDraw(playerid, Player[playerid].Draw.Button1, {Red=255, Green=0, Blue=0});
		UpdatePlayerDraw(playerid, Player[playerid].Draw.Button2, {Red=255, Green=255, Blue=255});
	else
		HideTexture(playerid, Button_Left_Focus);
		ShowTexture(playerid, Button_Right_Focus);
		UpdatePlayerDraw(playerid, Player[playerid].Draw.Button2, {Red=255, Green=0, Blue=0});
		UpdatePlayerDraw(playerid, Player[playerid].Draw.Button1, {Red=255, Green=255, Blue=255});
	end
end

function ShowPlayerDialog(playerid, ID, style,...)
	if playerid and ID and style and arg then
		local headline, items, button1, button2;
		if style == DIALOG_STYLE_LISTITEM then
			headline = arg[1];
			items = arg[2];
			button1 = arg[3];
			button2 = arg[4];
			if not type(headline)=="string" and not type(items)=="table" and not type(button1)=="string" then 
				print("Wrong Parameters: ShowPlayerDialog", playerid, ID, style,headline, items, button1);
				return
			end
		elseif style == DIALOG_STYLE_MSGBOX then
			headline = arg[1];
			text = arg[2];
			button1 = arg[3];
			button2 = arg[4];
			if not type(headline)=="string" and not type(text)=="string" and not type(button1)=="string" then 
				print("Wrong Parameters: ShowPlayerDialog", playerid, ID, style,headline, text, button1);
				return
			end
		elseif style == DIALOG_STYLE_INPUT or style == DIALOG_STYLE_INPUT_PASSWORD then
			headline = arg[1];
			underheadline = arg[2];
			text = arg[3];
			button1 = arg[4];
			button2 = arg[5];
			if not type(headline)=="string" and not type(underheadline)=="string" and not type(text)=="string" and not type(button1)=="string" then 
				print("Wrong Parameters: ShowPlayerDialog", playerid, ID, style,headline, underheadline,text, button1);
				return
			end
		else return
		end
		CreateDialog(playerid, {ID=ID, Style=style, Headline = headline, UnderHeadline = underheadline, ListItem = items, Text=text, Button1=button1, Button2=button2});
	end
end

function CloseDialog(playerid)
	HideTexture(playerid, "Dialog");
	DestroyPlayerDraw(playerid, "Dialog");
	DestroyAllPlayerDraws(playerid);
	InitDialogPlayerVars(playerid);
	HideTexture(playerid, "Hide");
	FreezePlayer(playerid, 0);
end

local function cutstring(playerid, text)
	local tab = {};
	for i=1, #text do
		local c = string.sub(text, i, i);
		if GetPlayerTextX(playerid, "FONT_DEFAULT.TGA", table.concat({table.concat(tab), c, "..."})) < 1900 then
				table.insert(tab, c);
		else
			return table.concat({table.concat(tab), c, "..."});
		end
	end
end

local function UpdateInsert(playerid, text)
	OnPlayerChangeInputtext(playerid, text, Player[playerid].Inputtext);
	Player[playerid].Inputtext = text;
	if Player[playerid].Style == DIALOG_STYLE_INPUT_PASSWORD then
		text = string.rep("X", string.len(text));
	end
	if GetPlayerTextX(playerid, "FONT_DEFAULT.TGA", text) > 1900 then
		text = cutstring(playerid, text);
	end
	if Player[playerid].InputtextDraw then
		UpdatePlayerDraw(playerid, Player[playerid].InputtextDraw, {Text= text});
	else
		Player[playerid].InputtextDraw = CreatePlayerDraw(playerid, 3150, 3625, text, "Font_Default.tga", 255, 255, 255, "Dialog");
		ShowPlayerDraw(playerid, Player[playerid].InputtextDraw);
	end
end

local function Update(playerid)
		local line = 1; 
		for k = Player[playerid].Start, Player[playerid].End do
			if Player[playerid].Line and Player[playerid].Line == line and Player[playerid].Style==DIALOG_STYLE_LISTITEM then
				UpdatePlayerDraw(playerid, Player[playerid].MenuLine[line], {Text=tostring(Player[playerid].MenuContent[k]), Red=255, Green=0, Blue=0});
			else
				UpdatePlayerDraw(playerid, Player[playerid].MenuLine[line], {Text=tostring(Player[playerid].MenuContent[k]), Red=255, Green=255, Blue=255});
			end
			line=line+1;
		end
end

local function ControlMSGBox(playerid, keyDown, keyUp)
	local tstart = Player[playerid].Start;
	local tend = Player[playerid].End;
	if keyDown == KEY_UP then
		if tstart ~= 1 then
			Player[playerid].Start = tstart -1;
			Player[playerid].End = tend -1;
			Update(playerid);ArrowUpAndDown(playerid);
		end
	elseif keyDown == KEY_DOWN then
		if tend < table.getn(Player[playerid].MenuContent) then
			Player[playerid].Start = tstart +1;
			Player[playerid].End = tend +1;
			Update(playerid);ArrowUpAndDown(playerid);
		end
	end
end

local function ControlListitem(playerid, keyDown, keyUp)
	local oldline = Player[playerid].Line;
	local tstart = Player[playerid].Start;
	local tend = Player[playerid].End;
	local oldlistitem = Player[playerid].ListItem;
	if keyDown == KEY_UP then
		if oldline == 1 and tstart == 1 then return end
		if oldline == 1 and oldlistitem > 1 then
			Player[playerid].ListItem = oldlistitem - 1;
			OnPlayerChangeListItem(playerid, Player[playerid].ListItem, oldlistitem);
			Player[playerid].Start = tstart -1;
			Player[playerid].End = tend -1;
			Update(playerid);ArrowUpAndDown(playerid);
			return
		end
		if oldline > 1 then 
			UpdatePlayerDraw(playerid, Player[playerid].MenuLine[oldline], {Text=tostring(Player[playerid].MenuContent[oldlistitem]),Blue=255, Green=255});
			Player[playerid].Line = oldline - 1;
			Player[playerid].ListItem = oldlistitem - 1;
			OnPlayerChangeListItem(playerid, Player[playerid].ListItem, oldlistitem);
			UpdatePlayerDraw(playerid, Player[playerid].MenuLine[oldline-1], {Text=tostring(Player[playerid].MenuContent[oldlistitem-1]),Blue=0, Green=0});
			ArrowUpAndDown(playerid);
		end
	elseif keyDown == KEY_DOWN then
		if oldline == table.getn(Player[playerid].MenuLine) and tstart >= table.getn(Player[playerid].MenuContent) then return end
		if oldline == table.getn(Player[playerid].MenuLine) and oldlistitem < table.getn(Player[playerid].MenuContent) then
			Player[playerid].ListItem = oldlistitem + 1;
			OnPlayerChangeListItem(playerid, Player[playerid].ListItem, oldlistitem);
			Player[playerid].Start = tstart +1;
			Player[playerid].End = tend +1;
			Update(playerid);ArrowUpAndDown(playerid);
			return
		end
		if oldline < table.getn(Player[playerid].MenuLine) and Player[playerid].MenuContent[oldlistitem +1] then 
			UpdatePlayerDraw(playerid, Player[playerid].MenuLine[oldline], {Text=tostring(Player[playerid].MenuContent[oldlistitem]), Blue=255, Green=255});
			Player[playerid].Line = oldline + 1;
			Player[playerid].ListItem = oldlistitem + 1;
			OnPlayerChangeListItem(playerid, Player[playerid].ListItem, oldlistitem);
			UpdatePlayerDraw(playerid, Player[playerid].MenuLine[oldline+1], {Text=tostring(Player[playerid].MenuContent[oldlistitem+1]), Blue=0, Green=0});
			ArrowUpAndDown(playerid);
		end
	end
end

function MenuControl(playerid, keyDown, keyUp)
	if Player[playerid].Menu then
		local tstart = Player[playerid].Start;
		local tend = Player[playerid].End;
		if Player[playerid].Style == DIALOG_STYLE_LISTITEM then ControlListitem(playerid, keyDown, keyUp) 
		elseif Player[playerid].Style == DIALOG_STYLE_MSGBOX or Player[playerid].Style == DIALOG_STYLE_INPUT or Player[playerid].Style == DIALOG_STYLE_INPUT_PASSWORD then ControlMSGBox(playerid, keyDown, keyUp)
		end
		if keyDown == KEY_LEFT or keyDown == KEY_RIGHT then
			if Player[playerid].Draw.Button1 and Player[playerid].Draw.Button2 then
				if Player[playerid].Button == 1 then
					Player[playerid].Button = 2;
				elseif Player[playerid].Button == 2 then
					Player[playerid].Button = 1;
				end
				UpdateButton(playerid);
			end
		end
		if keyDown == KEY_RETURN then
			OnPlayerResponseDialog(playerid, Player[playerid].ID, Player[playerid].Button, Player[playerid].ListItem, Player[playerid].Inputtext);
		end
		if keyDown == KEY_ESCAPE then
			CloseDialog(playerid);
		end
	end
end
AddEvent(MenuControl, "OnPlayerKey");

function InsertInputText(playerid, text)
	if Player[playerid].Menu and Player[playerid].Style == DIALOG_STYLE_INPUT or Player[playerid].Style == DIALOG_STYLE_INPUT_PASSWORD then
		UpdateInsert(playerid, text);
	end
end
AddEvent(InsertInputText, "OnPlayerText");

print("Modul: Dialog\t\tv0.1\tloaded")