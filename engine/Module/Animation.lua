local Animation = {};
local Other = {};
local x, y, z;
local start = 1;
local test = {};
local rep =1;
function InitAnimation()
	local t = scandir("Animationen", false);
	--for k,file in pairs(t) do
		for line in io.lines("Animationen/HumanS.mds") do
			if string.find(line, "ani") and string.find(line, string.char(34)) then
				local s, e = string.find(line, string.char(34))
				local new = string.sub(line, s+1);
				s, e = string.find(new, string.char(34))
				new = string.upper(string.sub(new, 1, e-1));
				if not Animation[new] then
					Animation[new] = {};
				end
			end
		end
	--end
	local i= 1;
	for k,v in pairs(Animation) do
		Other[i] = k;
		i=i+1;
	end
	print(i);
end
AddEvent(InitAnimation, "OnServerStart");

function LetsGo(playerid, cmdtext)
	if cmdtext == "/go" then
		FreezePlayer(playerid, 1);
		SetPlayerInstance(playerid, "BAU_900_Onar");
		x, y, z = GetPlayerPos(playerid);
		PlayAnimation(playerid,  Other[start]);
		OldSetTimerEx("GetID", 1000, 0, playerid);
	elseif cmdtext == "/print" then
		io.output(io.close("ani", "a"));
		for k,v in pairs(Animation) do
			io.write(k..";"..v, "\n");
		end
		io.close();
	end
end
AddEvent(LetsGo, "OnPlayerCommandText");

function GetID(playerid)
	local ID = GetPlayerAnimationID(playerid);
	if ID == 265 then
		if rep < 25 then 
			PlayAnimation(playerid, Other[start]);
			OldSetTimerEx("GetID", 250-(rep*10), 0, playerid);
			print(start..":"..ID..":"..Other[start]..":"..rep);
			rep = rep +1;
			return
		end
	end
	rep = 1;
	Animation[Other[start]] = ID;
	test[ID] = 1;
	print(start..":"..ID..":"..Other[start]);
	start = start +1;
	SetPlayerPos(playerid, x, y, z);
	if Other[start] then
		PlayAnimation(playerid, "S_RUN");
		OldSetTimerEx("GetID2", 500, 0, playerid);
	else	
		FreezePlayer(playerid, 0);
		PlayAnimation(playerid, "S_RUN");
		local i = 0;
		for k,v in pairs(test) do
			i = i+1;
		end
		print("Animationen:", i);
	end
end

function GetID2(playerid)
	PlayAnimation(playerid, Other[start]);
	OldSetTimerEx("GetID", 500, 0, playerid);
end

function scandir(directory, recursive, dirStr, t)
	if(t == nil)then
		t = {};
	end
	
    local popen = io.popen;
	
	if(dirStr == nil)then
		dirStr = "";
	end
	
	if(OPERATING_SYSTEM == nil or OPERATING_SYSTEM == 1)then
		for filename in popen('dir "'..directory..'" /b /a-d'):lines() do
			if(recursive == true)then
				table.insert(t, directory..filename);
			else
				table.insert(t, directory.."/"..filename);
			end
		end
		if(recursive == true)then
			for dirname in popen('dir "'..directory..'" /b /ad'):lines() do
				scandir(directory.."/"..dirname.."/", true, dirStr..dirname.."/", t);
			end
		end
	elseif(OPERATING_SYSTEM == 2)then
		if(recursive == true)then
			os.execute('find "'..directory..'" -type f > temp_bigrequire.tempfile');
			local handle = io.open("temp_bigrequire.tempfile");

			for filename in handle:lines() do
				table.insert(t, filename);
			end
			handle:close();
			os.remove("temp_bigrequire.tempfile");
		else
			os.execute('find "'..directory..'" -maxdepth 1 -type f > temp_bigrequire.tempfile');
			local handle = io.open("temp_bigrequire.tempfile");
			for filename in handle:lines() do
				table.insert(t, filename);
			end
			handle:close();
			os.remove("temp_bigrequire.tempfile");
		end
	end
    return t
end