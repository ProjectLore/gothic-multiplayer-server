if not IsModuleLoaded("Language") then print("ERROR: Can not load modul: Chat , because modul 'Language' is not loaded.") return false end
if not IsModuleLoaded("Scale") then print("ERROR: Can not load modul: Chat , because modul 'Scale' is not loaded.") return false end

local body = CreateTexture(0, 0, 3600, 2000, "MENU_INPUT_BACK.TGA", "Chat");
CreateTexture(3625, 0, 4000, 2000, "MENU_INPUT_BACK.TGA", "Chat");
local Player = {};
function InitNewChat()
	EnableChat(0);
end
AddEvent(InitNewChat, "OnServerStart");

function InitPlayerVarsChat(playerid)
	ShowTexture(playerid, "Chat");
	Player[playerid] = {};
	Player[playerid]["Default"] = {};
	Player[playerid]["All"] = {};
	Player[playerid].Chats = {[0] = "Default", [1] ="All"};
	Player[playerid].Chat = 1;
	Player[playerid].Line = {};
	Player[playerid].Start = 1;
	Player[playerid].End = nil;
	Player[playerid].MaxLines = nil;
	ClearChat(playerid);
	InitLinesForChat(playerid);
	SendPlayerMessage(playerid, 255, 0,0, "Das ist ein Test");
	SendPlayerMessage(playerid, 255, 0,0, "Das ist ein zweiter Test");
	SendPlayerMessage(playerid, 255, 0,0, "linie 3");
	SendPlayerMessage(playerid, 255, 0,0, "linie 4");
	SendPlayerMessage(playerid, 255, 0,0,  "linie 5");
	SendPlayerMessage(playerid, 255, 0,0,  "linie 6");
	SendPlayerMessage(playerid, 255, 0,0,  "linie 7");
	SendPlayerMessage(playerid, 255, 0,0,  "linie 8");
	SendPlayerMessage(playerid, 255, 0,0,  "linie 9");
	SendPlayerMessage(playerid, 255, 0,0,  "linie 10");
	SendPlayerMessage(playerid, 255, 0,0, "linie 11");
end
AddEvent(InitPlayerVarsChat, "OnPlayerConnect");

function ClearChat(playerid)
	for i=1, 40 do
		OldSendPlayerMessage(playerid, 255, 255, 255, "");
	end
end

function InitLinesForChat(playerid)
	local height = GetPlayerTextY(playerid, "font_default.tga");
	local lines = math.floor(2000 / height);
	local y = 25;
	Player[playerid].End = lines;
	Player[playerid].MaxLines = lines;
	for i = 1, lines do
		Player[playerid].Line[i] = CreatePlayerDraw(playerid, 25, y, "", "Font_Default.Tga", 255, 255, 255, "Chat");
		y = y+height;
	end
	ShowPlayerDraw(playerid, "Chat");
end

function GetPlayerChat(playerid)
	return Player[playerid].Chats[Player[playerid].Chat];
end

function PlayerWritesSomethingInTheChat(playerid, text)
	SendMessageToAll(255, 255, 255, GetPlayerName(playerid)..":"..text);
end
AddEvent(PlayerWritesSomethingInTheChat, "OnPlayerText");

local function UpdatePlayerChat(playerid, ft, r, g, b, chat)
	local displayed = (chat == Player[playerid].Chat);
	chat = Player[playerid].Chats[chat] or Player[playerid].Chats[1];
	for k,v in pairs(ft) do
		table.insert(Player[playerid][chat], {Text=v, Red=r,Green=g, Blue=b});
	end
	if displayed then
		local tstart = table.getn(Player[playerid][chat])-Player[playerid].MaxLines+1;
		local tend = table.getn(Player[playerid][chat]);
		local line = 1;
		for i = tstart, tend do
			if Player[playerid][chat][i] then
				UpdatePlayerDraw(playerid, Player[playerid].Line[line],  {Text=Player[playerid][chat][i].Text, Red=Player[playerid][chat][i].Red, Green=Player[playerid][chat][i].Green, Blue=Player[playerid][chat][i].Blue });
				line = line +1;
			end
		end		
	else
	
	end
end

OldSendPlayerMessage = SendPlayerMessage;
function _SendPlayerMessage(playerid, r,g, b, text, ...)
	local chat = arg[1] or 1;
	local ft = FormatText(playerid, text, 4000, "Font_Default.TGA");
	if playerid and r and g and b and text then
		UpdatePlayerChat(playerid, ft, r, g, b, chat);
	end
end
SendPlayerMessage = _SendPlayerMessage;

OldSendMessageToAll = SendMessageToAll;
function _SendMessageToAll(r,g, b, text, ...)
	for playerid in pairs(GetConnectedPlayers()) do
		SendPlayerMessage(playerid, r, g, b, text);
	end
end
SendMessageToAll = _SendMessageToAll;