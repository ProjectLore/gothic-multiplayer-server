if not IsModuleLoaded("Scale") then print("Cannot load Module: 'Account' because required module: 'Scale' isnt loaded!"); return end

local HEADLINE = "Account System";
local FOOTER = "Account System v0.1";

--[[#################################
#	Main Coordinates for Menu		#
###################################]]
local X, Y, WIDTH, HEIGHT = 3225,2825,4975,5450;
local BACKGROUND = CreateTexture(X, Y, WIDTH, HEIGHT,"Menu_Ingame.TGA", "Account"); -- Main Menu

--[[#################################
#	Main Coordinates for Creation	#
###################################]]
local C_X, C_Y, C_WIDTH, C_HEIGHT = 5775,1175,7950,6975;
local BACKGROUND_CREATE = CreateTexture(C_X, C_Y, C_WIDTH, C_HEIGHT,"FRAME_GMPA.tga", "Account");

--[[#################################
#	Two Buttons: Background + Focus	#
###################################]]
local BUTTON_LEFT = CreateTexture(X+225, Y+2025, WIDTH-950, HEIGHT-350,"Menu_Choice_Back.TGA", "Account");
local BUTTON_LEFT_FOCUS = CreateTexture(X+175, Y+2025, WIDTH-900, HEIGHT-350,"Inv_Slot_Focus.TGA", "Account");
local BUTTON_RIGHT = CreateTexture(X+950, Y+2025, WIDTH-225, HEIGHT-350,"Menu_Choice_Back.TGA", "Account");
local BUTTON_RIGHT_FOCUS = CreateTexture(X+900, Y+2025, WIDTH-175, HEIGHT-350,"Inv_Slot_Focus.TGA", "Account");

--[[#################################
#	Predefined Buttons: Back+Focus	#
###################################]]
local BUTTON = 
{
	[1] =
	{
		BACK = CreateTexture(X+225,Y+400,WIDTH-225,HEIGHT-1975, "Menu_Choice_Back.TGA", "Account");
		FOCUS = CreateTexture(X+175,Y+400,WIDTH-175,HEIGHT-1975,"Inv_Slot_Focus.TGA", "Account");
	},
	[2] =
	{
		BACK = CreateTexture(X+225,Y+700,WIDTH-225,HEIGHT-1675, "Menu_Choice_Back.TGA", "Account");
		FOCUS = CreateTexture(X+175,Y+700,WIDTH-175,HEIGHT-1675,"Inv_Slot_Focus.TGA", "Account");
	},
	[3] =
	{
		BACK = CreateTexture(X+225,Y+1000,WIDTH-225,HEIGHT-1375, "Menu_Choice_Back.TGA", "Account");
		FOCUS = CreateTexture(X+175,Y+1000,WIDTH-175,HEIGHT-1375,"Inv_Slot_Focus.TGA", "Account");
	},
	[4] =
	{
		BACK = CreateTexture(X+225,Y+1300,WIDTH-225,HEIGHT-1075, "Menu_Choice_Back.TGA", "Account");
		FOCUS = CreateTexture(X+175,Y+1300,WIDTH-175,HEIGHT-1075,"Inv_Slot_Focus.TGA", "Account");
	},
	[5] =
	{
		BACK = CreateTexture(X+225,Y+1600,WIDTH-225,HEIGHT-775, "Menu_Choice_Back.TGA", "Account");
		FOCUS = CreateTexture(X+175,Y+1600,WIDTH-175,HEIGHT-775,"Inv_Slot_Focus.TGA", "Account");
	},
	[6] =
	{
		BACK = CreateTexture(X+225,Y+1900,WIDTH-225,HEIGHT-475, "Menu_Choice_Back.TGA", "Account");
		FOCUS = CreateTexture(X+175,Y+1900,WIDTH-175,HEIGHT-475,"Inv_Slot_Focus.TGA", "Account");
	},
};

--[[################################# Globale "Predefined" Values #################################]]

--[[#################################
#	Predefined Screens				#
###################################]]
SCREEN_MAIN = 1;
SCREEN_LOGIN = 2;
SCREEN_REGISTER = 3;
SCREEN_SETTINGS = 4;
SCREEN_CHARACTER = 5;
SCREEN_CREATE = 6;


--[[#################################
#	Predefined Buttontypes			#
###################################]]
TYPE_BUTTON = 1;
TYPE_INPUT = 2;
TYPE_SELECT_SIMPLE = 3;
TYPE_SELECT = 4;

--[[#################################
#	Predefined Gender				#
###################################]]
MALE = 1;
FEMALE = 2;

--[[#################################
#	Predefined Skin					#
###################################]]
PALE = 1;
NORMAL = 2;
LATINO = 3;
BLACK = 4;

CREATE_NAME = 1;
CREATE_GENDER = 2;
CREATE_SKIN = 3;
CREATE_HEADMODEL = 4;
CREATE_HEADTEX = 5;
CREATE_WALK = 6;
CREATE_VOICE = 7;
CREATE_CREATE = 8;
CREATE_EXIT = 9;


require("engine/Include/Account/Functions");



SCREEN =
{
	[SCREEN_MAIN] = 
	{
		HEADLINE = true;
		FOOTER = true;
		BUTTON =
		{
			[1]	=
			{
				ENABLE = true;
				TYPE = TYPE_BUTTON;
				BACK = BUTTON[1].BACK;
				FOCUS = BUTTON[1].FOCUS;
				TEXT = "Einloggen";
				RESPONSE = SCREEN_LOGIN;
			},
			[2] =
			{
				ENABLE = true;
				TYPE = TYPE_BUTTON;
				BACK = BUTTON[2].BACK;
				FOCUS = BUTTON[2].FOCUS;
				TEXT = "Registrieren";
				RESPONSE = SCREEN_REGISTER;
			},
			[3] =
			{
				ENABLE = true;
				TYPE = TYPE_BUTTON;
				BACK = BUTTON[3].BACK;
				FOCUS = BUTTON[3].FOCUS;
				TEXT = "Verlassen";
				RESPONSE = ExitGame;
			},
		},
		SHOW = {BACKGROUND},
	},
	[SCREEN_LOGIN] = 
	{
		HEADLINE = true;
		FOOTER = true;
		BUTTON =
		{
			[1]	=
			{
				ENABLE = true;
				TYPE = TYPE_INPUT;
				BACK = BUTTON[2].BACK;
				FOCUS = BUTTON[2].FOCUS;
				TEXT = "Accountname";
			},
			[2]	=
			{
				ENABLE = true;
				TYPE = TYPE_INPUT;
				BACK = BUTTON[4].BACK;
				FOCUS = BUTTON[4].FOCUS;
				TEXT = "Passwort";
			},
			[3]	=
			{
				ENABLE = true;
				TYPE = TYPE_BUTTON;
				BACK = BUTTON_LEFT;
				FOCUS = BUTTON_LEFT_FOCUS;
				TEXT = "Login";
				RESPONSE = AccountLogin;
			},
			[4]	=
			{
				ENABLE = true;
				TYPE = TYPE_BUTTON;
				BACK = BUTTON_RIGHT;
				FOCUS = BUTTON_RIGHT_FOCUS;
				TEXT = "Zurück";
				RESPONSE = SCREEN_MAIN;
			},
		},
		SHOW = {BACKGROUND},
	},
	[SCREEN_REGISTER] = 
	{
		HEADLINE = true;
		FOOTER = true;
		BUTTON =
		{
			[1]	=
			{
				ENABLE = true;
				TYPE = TYPE_INPUT;
				BACK = BUTTON[1].BACK;
				FOCUS = BUTTON[1].FOCUS;
				TEXT = "Accountname";
			},
			[2]	=
			{
				ENABLE = true;
				TYPE = TYPE_INPUT;
				BACK = BUTTON[3].BACK;
				FOCUS = BUTTON[3].FOCUS;
				TEXT = "Passwort";
			},
			[3]	=
			{
				ENABLE = true;
				TYPE = TYPE_INPUT;
				BACK = BUTTON[5].BACK;
				FOCUS = BUTTON[5].FOCUS;
				TEXT = "Passwort wiederholen";
			},
			[4]	=
			{
				ENABLE = true;
				TYPE = TYPE_BUTTON;
				BACK = BUTTON_LEFT;
				FOCUS = BUTTON_LEFT_FOCUS;
				TEXT = "Login";
				RESPONSE = AccountRegister;
			},
			[5]	=
			{
				ENABLE = true;
				TYPE = TYPE_BUTTON;
				BACK = BUTTON_RIGHT;
				FOCUS = BUTTON_RIGHT_FOCUS;
				TEXT = "Zurück";
				RESPONSE = SCREEN_MAIN;
			},
		},
		SHOW = {BACKGROUND},
	},
	[SCREEN_SETTINGS] = 
	{
		HEADLINE = true;
		FOOTER = true;
		BUTTON =
		{
			[1]	=
			{
				ENABLE = true;
				TYPE = TYPE_SELECT;
				BACK = BUTTON[1].BACK;
				FOCUS = BUTTON[1].FOCUS;
				LISTITEM = GetPlayerLanguage;
				TEXT = "Sprache";
				SELECT = 
				{
					[1] = 
					{
						TEXT = "Deutsch";
						VALUE = "DE";
					},
					[2] =
					{
						TEXT = "English";
						VALUE = "EN";
					},
				},
			},
			[2]	=
			{
				ENABLE = true;
				TYPE = TYPE_SELECT;
				BACK = BUTTON[3].BACK;
				FOCUS = BUTTON[3].FOCUS;
				TEXT = "Auflösung";
				SELECT = 
				{
					[1] = 
					{
						TEXT = "Deutsch";
						VALUE = "DE";
					},
					[2] =
					{
						TEXT = "English";
						VALUE = "EN";
					},
				},
			},
			[3]	=
			{
				ENABLE = true;
				TYPE = TYPE_INPUT;
				BACK = BUTTON[5].BACK;
				FOCUS = BUTTON[5].FOCUS;
				TEXT = "Passwort wiederholen";
			},
			[4]	=
			{
				ENABLE = true;
				TYPE = TYPE_BUTTON;
				BACK = BUTTON_LEFT;
				FOCUS = BUTTON_LEFT_FOCUS;
				TEXT = "Login";
				RESPONSE = AccountRegister;
			},
			[5]	=
			{
				ENABLE = true;
				TYPE = TYPE_BUTTON;
				BACK = BUTTON_RIGHT;
				FOCUS = BUTTON_RIGHT_FOCUS;
				TEXT = "Zurück";
				RESPONSE = SCREEN_MAIN;
			},
		},
		SHOW = {BACKGROUND},
	},
	[SCREEN_CHARACTER] =
	{
		HEADLINE = false;
		FOOTER = false;
		SHOW = {BACKGROUND_CREATE},
		BUTTON = 
		{
			[1] = 
			{
				ENABLE = IsButtonEnabled;
				TYPE = TYPE_BUTTON;
				BACK = CreateTexture(X+2775,Y-1250,WIDTH+2750,HEIGHT-3000,"Inv_Back_Buy.tga");
				FOCUS = CreateTexture(X+2775,Y-1250,WIDTH+2750,HEIGHT-3000,"Inv_Slot_Focus.TGA");
				TEXT = GetScreenCharacterText;
				RESPONSE = CheckForExistingChar;
			},
			[2] = 
			{
				ENABLE = IsButtonEnabled;
				TYPE = TYPE_BUTTON;
				BACK = CreateTexture(X+2775,Y+25,WIDTH+2750,HEIGHT-1725,"Inv_Back_Buy.tga");
				FOCUS = CreateTexture(X+2775,Y+25,WIDTH+2750,HEIGHT-1725,"Inv_Slot_Focus.TGA");
				TEXT = GetScreenCharacterText;
				RESPONSE = CheckForExistingChar;
			},
			[3] = 
			{
				ENABLE = IsButtonEnabled;
				TYPE = TYPE_BUTTON;
				BACK = CreateTexture(X+2775,Y+1300,WIDTH+2750,HEIGHT-450,"Inv_Back_Buy.tga");
				FOCUS = CreateTexture(X+2775,Y+1300,WIDTH+2750,HEIGHT-450,"Inv_Slot_Focus.TGA");
				TEXT = GetScreenCharacterText;
				RESPONSE = CheckForExistingChar;
			},
			[4] = 
			{
				ENABLE = IsButtonEnabled;
				TYPE = TYPE_BUTTON;
				BACK = CreateTexture(X+2775,Y+2475,WIDTH+2750,HEIGHT+825,"Inv_Back_Buy.tga");
				FOCUS = CreateTexture(X+2775,Y+2475,WIDTH+2750,HEIGHT+825,"Inv_Slot_Focus.TGA");
				TEXT = GetScreenCharacterText;
				RESPONSE = CheckForExistingChar;
			},
		},
	},
	[SCREEN_CREATE] =
	{
		HEADLINE = false;
		FOOTER = false;
		SHOW = {BACKGROUND_CREATE},
		BUTTON =
		{
			[CREATE_NAME] =
			{
				TYPE = TYPE_INPUT;
				TEXT = "Name:";
				BACK = CreateTexture(6200,1475,7525,1775,"Menu_Choice_Back.TGA");
				FOCUS = CreateTexture(6175,1475,7550,1775,"Inv_Slot_Focus.TGA");
			},
			[CREATE_GENDER] =
			{
				TYPE = TYPE_SELECT;
				TEXT = "Geschlecht:";
				BACK = CreateTexture(6200,2025,7525,2325,"Menu_Choice_Back.TGA");
				FOCUS = CreateTexture(6175,2025,7550,2325,"Inv_Slot_Focus.TGA");
				SELECT = 
				{
					[MALE] = 
					{
						TEXT = "Männlich",
						VALUE = "Hum_Body_Naked0",
					},
					[FEMALE] = 
					{
						TEXT = "Weiblich",
						VALUE = "Hum_Body_Babe0",
					},
				},
			},
			[CREATE_SKIN] =
			{
				TYPE = TYPE_SELECT;
				TEXT = "Hautfarbe:";
				BACK = CreateTexture(6200,2575,7525,2875,"Menu_Choice_Back.TGA");
				FOCUS = CreateTexture(6175,2575,7550,2875,"Inv_Slot_Focus.TGA");
				SELECTTABLE = {GetPlayerSex},
				SELECT = 
				{
					[MALE] = 
					{
						[1] =
						{
							TEXT = "blass", --PALE
							VALUE = 0,
						},
						[2] =
						{
							TEXT = "normal", --NORMAL
							VALUE = 1,
						},
						[3] =
						{
							TEXT = "normal", --NORMAL
							VALUE = 10,
						},
						[4] = 
						{
							TEXT = "normal", --NORMAL
							VALUE = 8,
						},
						[5] =
						{
							TEXT = "latino", --LATINO
							VALUE = 2,
						},
						[6] = 
						{
							TEXT = "schwarz", --BLACK
							VALUE = 3,
						},
					},
					[FEMALE] = 
					{
						[1] =
						{
							TEXT = "blass", --PALE
							VALUE = 4,
						},
						[2] =
						{
							TEXT = "normal", --NORMAL
							VALUE = 5,
						},
						[3] = 
						{
							TEXT = "normal", --NORMAL
							VALUE = 11,
						},
						[4] = 
						{
							TEXT = "normal", --NORMAL
							VALUE = 12,
						},
						[5] =
						{
							TEXT = "latino", --LATINO
							VALUE = 6,
						},
						[6] =
						{
							TEXT = "schwarz", --BLACK
							VALUE = 7,
						},
					},
				},
			},
			[CREATE_HEADMODEL] =
			{
				TYPE = TYPE_SELECT_SIMPLE;
				TEXT = "Kopfform";
				BACK = CreateTexture(6200,2925,7525,3275,"Menu_Choice_Back.TGA");
				FOCUS = CreateTexture(6175,2925,7550,3275,"Inv_Slot_Focus.TGA");
				SELECTTABLE = {GetPlayerSex};
				SELECT = 
				{
					[MALE] = 
					{
						"Hum_Head_FatBald",
						"Hum_Head_Fighter",
						"Hum_Head_Pony",
						"Hum_Head_Bald",
						"Hum_Head_Thief",
						"Hum_Head_Psionic",
					},
					[FEMALE] = 
					{
						"Hum_Head_Babe",
						"Hum_Head_FatBald",
						"Hum_Head_Fighter",
						"Hum_Head_Pony",
						"Hum_Head_Bald",
						"Hum_Head_Thief",
						"Hum_Head_Psionic",
					},
				},
			},
			[CREATE_HEADTEX] =
			{
				TYPE = TYPE_SELECT_SIMPLE;
				TEXT = "Gesicht";
				BACK = CreateTexture(6200,3325,7525,3625,"Menu_Choice_Back.TGA");
				FOCUS = CreateTexture(6175,3325,7550,3625,"Inv_Slot_Focus.TGA");
				SELECTTABLE = {GetPlayerSex, GetPlayerSkinColor};
				SELECT = 
				{
					[MALE] = 
					{
						[PALE] =
						{
							19, 39, 41, 42, 43, 44, 44, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 
						},
						[NORMAL] =
						{
							0, 1, 2, 3, 5, 6, 7, 9, 10, 13, 14, 16, 18, 20, 21, 22, 23, 24, 25, 26, 27, 31, 32, 33, 34, 35, 36, 37, 38, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75 , 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92 ,93, 94, 95, 96, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 159
						},
						[LATINO] =
						{
							8, 15, 29, 30, 40, 120, 121, 122, 123, 124, 125, 126, 127, 128,
						},
						[BLACK] =
						{
							4, 11, 12, 17, 28, 129, 130, 131, 132, 133, 134, 135, 136,
						},
					},
					[FEMALE] = 
					{
						[PALE] =
						{
							151,
						},
						[NORMAL] =
						{
							137, 138, 139, 140, 143, 144, 145, 146, 147, 148, 149, 150, 152, 153, 154, 155, 156,
						},
						[LATINO] =
						{
							141, 158,
						},
						[BLACK] =
						{
							142, 157
						},
					},
				},
			},
			[CREATE_WALK] =
			{
				TYPE = TYPE_SELECT;
				TEXT = "Gangart:";
				BACK = CreateTexture(6200,3875,7525,4175,"Menu_Choice_Back.TGA");
				FOCUS = CreateTexture(6175,3875,7550,4175,"Inv_Slot_Focus.TGA");
				SELECTTABLE = {GetPlayerSex};
				SELECT = 
				{
					[MALE] = 
					{
						[1] = 
						{
							TEXT = "Standart",
							VALUE = "NULL",
						},
						[2] = 
						{
							TEXT = "Arrogant",
							VALUE = "HumanS_Arrogance.mds",
						},
						[3] = 
						{
							TEXT = "Rückhaltend",
							VALUE = "Humans_Mage.mds",
						},
						[4] = 
						{
							TEXT = "Relaxed",
							VALUE = "HumanS_Relaxed.mds",
						},
						[5] = 
						{
							TEXT = "Müde",
							VALUE = "Humans_Tired.mds",
						},
						[6] =
						{
							TEXT = "Millitärisch",
							VALUE = "HumanS_Militia.mds",
						},
					},
					[FEMALE] =
					{
						[1] = 
						{
							TEXT = "Standart",
							VALUE = "NULL",
						},
						[2] = 
						{
							TEXT = "Arrogant",
							VALUE = "HumanS_Arrogance.mds",
						},
						[3] = 
						{
							TEXT = "Weiblich",
							VALUE = "Humans_Babe.mds",
						},
						[4] = 
						{
							TEXT = "Rückhaltend",
							VALUE = "Humans_Mage.mds",
						},
						[5] = 
						{
							TEXT = "Relaxed",
							VALUE = "HumanS_Relaxed.mds",
						},
						[6] = 
						{
							TEXT = "Müde",
							VALUE = "Humans_Tired.mds",
						},
						[7] =
						{
							TEXT = "Millitärisch",
							VALUE = "HumanS_Militia.mds",
						},
					},
				},
			},
			[CREATE_VOICE] =
			{
				TYPE = TYPE_SELECT_SIMPLE;
				TEXT = "Stimme";
				BACK = CreateTexture(6200,4225,7525,4525,"Menu_Choice_Back.TGA");
				FOCUS = CreateTexture(6175,4225,7550,4525,"Inv_Slot_Focus.TGA");
				SELECTTABLE = {GetPlayerSex};
				SELECT = 
				{
					[MALE] = 
					{
						1, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14,
					},
					[FEMALE] =
					{
						16, 17,
					},
				},
			},
			[CREATE_CREATE] = 
			{
				TYPE = TYPE_BUTTON;
				TEXT = "Erstellen";
				BACK = CreateTexture(6200,6200,7525,6500,"Menu_Choice_Back.TGA");
				FOCUS = CreateTexture(6175,6200,7550,6500,"Inv_Slot_Focus.TGA");
				RESPONSE = CheckForCreateChar;
			},
			[CREATE_EXIT] =
			{
				TYPE = TYPE_BUTTON;
				TEXT = "Zurück";
				BACK = CreateTexture(6200,6600,7525,6900,"Menu_Choice_Back.TGA");
				FOCUS = CreateTexture(6175,6600,7550,6900,"Inv_Slot_Focus.TGA");
				RESPONSE = CheckForExistingChar;
			},
		},
	}
};











