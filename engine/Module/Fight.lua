local Player = {};
local amateur = 30;
local profi = 60;
--[[
0 = WEAPON_NONE
1 = WEAPON_FIST
3 = WEAPON_1H
4 = WEAPON_2H
5 = WEAPON_BOW
6 = WEAPON_CBOW
]]
local walks = {
	[3]={"Humans_1hST1.mds", "Humans_1hST2.mds"},
	[4]={"Humans_2hST1.mds", "Humans_2hST2.mds"},
	[5]={"Humans_BowT1.mds", "Humans_BowT2.mds"},
	[6]={"Humans_CBowT1.mds", "Humans_CBowT2.mds"},
};

function InitPlayerVarsFight(playerid)
	if playerid then
		Player[playerid] = {};
		Player[playerid].Default = "NONE";
	end
end
AddEvent(InitPlayerVarsFight, "OnPlayerConnect");

function SynchronizesFight(playerid, mode)
	if mode > 1 and mode < 7 then
		local skill = GetPlayerSkillWeapon(playerid, mode);
		if skill >= amateur and skill < profi then -- Ist Amateur
			SetPlayerWalk(playerid, walks[mode][1]);	
		elseif skill >= profi then
			SetPlayerWalk(playerid, walks[mode][2]);	
		end
	end
	if mode == WEAPON_NONE then
		SetPlayerWalk(playerid, Player[playerid].Default);	
	end
end
AddEvent(SynchronizesFight, "OnPlayerWeaponMode");

function SetPlayerDefaultWalk(playerid, walk)
	if playerid and walk then
		Player[playerid].Default = walk;
	end
end

function GetPlayerDefaultWalk(playerid)
	return Player[playerid].Default;
end

function SetAmateurSkill(value)
	if value and type(value)=="number" then
		amateur = value;
	end
end

function SetProfiSkill(value)
	if value and type(value)=="number" then
		profi = value;
	end
end

print("Modul: Fight\t\tv0.1\tloaded")