

function Command(playerid, cmdtext)
	local cmd, params = GetCommand(cmdtext);
	if cmdtext == "/new" then
		local x , y, z = GetPlayerPos(playerid);
		local rot = GetPlayerPos(playerid);
		local world = GetPlayerWorld(playerid);
		CreateSpawnSelection(x, y, z, rot, world)
		StartAccountSytem(playerid);
	elseif cmdtext == "/new2" then
		ChangeScreen(playerid, 2, 1)
	elseif cmdtext == "/new3" then
		ChangeScreen(playerid, 1, 2)
	elseif cmdtext == "/clear" then
		print("\n", "------------------------------------", "\n");
	elseif cmdtext == "/print" then
		GetPlayerSkillWeapon(playerid, 0);
		GetPlayerSkillWeaponWithArmor(playerid, 0);
	elseif cmd == "/pos" then
		local x, y, z = GetPlayerPos(playerid);
		local angle = GetPlayerAngle(playerid);
		local arg = params or "";
		local str = string.format("%s,%s,%s %s \n%s %s", x,y,z,arg,angle,arg);
		LogString("pos", str);
		SendPlayerMessage(playerid, 255, 255, 255, str);
	elseif cmd == "/kloster" then
		SetPlayerPos(playerid, 46909.79296875,4990.9619140625,18605.71875);
	elseif cmd == "/open" then
		local a, b, c, d = GetPlayerAdditionalVisual(playerid);
		SetPlayerAdditionalVisual(playerid, a, 1, c, d);
		print("Skin:", GetPlayerSkinColor(playerid));
	elseif cmd == "/update" then
		SetPlayerInstance(playerid, "PS_HERO");
		print(GetPlayerAdditionalVisual(playerid));
	elseif cmd == "/instance" then
		params = string.upper(params);
		SetPlayerInstance(playerid, params);
	elseif cmdtext == "/addr" then
		GiveItem(playerid, "Itmi_Gold", 20);
	elseif cmdtext == "/lol" then
		if DoesPlayerOwnItem(playerid, "Itmi_Gold", 20) then
			print("true")
		else
			print("false")
		end
	end
end
AddEvent(Command, "OnPlayerCommandText");