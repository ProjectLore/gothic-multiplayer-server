

local HOSTNAME = "localhost";
local USERNAME = "root";
local PASSWORD = "";
local DATABASE = "lol";
--local HANDLER;
local check = {};
local need = {
	["player"]={},
	["inventory"]={},
	["account"]={},
};
local correct = {};

local function db_connect()
	local HANDLER = mysql_connect( HOSTNAME, USERNAME, PASSWORD, DATABASE);
	if not HANDLER then
		print("Cannot Connect:\n", "Change datas in engine/Include/Account/Mysql.lua");
		return
	end
	--SetTimerEx("CheckMysqlConnection", 5000, 1);
	--CheckDatabase();
	return HANDLER;
end
AddEvent(ConnectToMysqlServer, "OnServerStart");

function CheckDatabase()
	for k in pairs(need) do
		local tab = {};
		for line in io.lines("engine/Include/Account/"..k..".txt") do
			table.insert(tab, line);
		end
		need[k] = table.concat(tab, "\n")
		local sql = "SHOW CREATE TABLE "..k.."";
		local result = mysql_query(HANDLER, sql);
		if not result then
			print(mysql_error(HANDLER), mysql_errno(HANDLER)); 
		else
			local row =  mysql_fetch_row(result);
			check[row[1]]=row[2];
			if need[k] == check[k] then
				CheckDatabaseStructure(k);
				--print("table:", k, "is correct!");
			else
				LoadCorrectStructure(k);
				CheckDatabaseStructure(k);
				--print("table:", k, "is wrong!");
			end
		end
	end
end

function LoadCorrectStructure(k)
	correct[k]={};
	for line in io.lines("engine/Include/Account/"..k.."structure.txt") do
		local row = line:split(";");
			--print(row[1], row[2], row[3], row[4], row[5], row[6]);
		correct[k][row[1]] = {Type = row[2], Null = row[3], Key = row[4], Default = row[5], Extra=row[6]};
	end
end

function CheckDatabaseStructure(k)
	--io.output(io.open("engine/Include/Account/playerstructure.txt", "a"));
	local res = mysql_query(HANDLER, "describe " .. tostring(k) .. ";");
    if res then
        local row = mysql_fetch_row(res);
        while row do
			if #row[4] < 1 then
				row[4] = " ";
			end
			if type(row[5]) =="userdata" then
				row[5] = false;
			end
			if #row[6] < 1 then
				row[6] = " ";
			end
			
			table.insert(correct, {row[1],row[2], row[3], row[4], tostring(row[5]), row[6]});
            row = mysql_fetch_row(res);
        end
    end
	for k in pairs(correct) do
		--io.write(table.concat(correct[k], ";"), "\n");
	end
	--io.close();
end

local function DoesPlayerAccountExist(name)
	local handler = db_connect();
	if handler and name then
		local sql = "SELECT * FROM `account` WHERE Accountname='"..name.."'";
		local result = mysql_query(handler, sql);
		if result then
			local found = mysql_num_rows(result);
			mysql_close(handler);
			if found > 0 then
				return true
			end
			return false
		end
	end
end

function DoesPlayerExist(name)
	local handler = db_connect();
	if handler and name then
		local sql = "SELECT * FROM `player` WHERE Name='"..name.."'";
		local result = mysql_query(handler, sql);
		if result then
			local found = mysql_num_rows(result);
			mysql_close(handler);
			if found > 0 then
				return true
			end
			return false
		end
	end
end

function AccountLogin(playerid)
	local draw = GetPlayerInputDrawsAccount(playerid);
	local info = GetPlayerDrawInfo(playerid, draw[1].Input);
	local accname = info.Text;
	info = GetPlayerDrawInfo(playerid, draw[2].Input);
	local passwort = info.Text;
	local handler = db_connect();
	if handler and #accname >= 4 then
		local sql = "SELECT * FROM `account` WHERE Accountname='"..accname.."'";
		local result = mysql_query(handler, sql);
		if result then
			local found = mysql_num_rows(result);
			if found > 0 then
				local row = mysql_fetch_assoc(result);
				if MD5(passwort)== row.Password then
					mysql_close(handler);
					SetPlayerLoggedIn(playerid);
					SetPlayerAccountname = accname;
					SetPlayerSpawnSelection(playerid);
					SetPlayerChars(playerid, GetPlayerChars(playerid));
					return
				else
					--Passwörter stimmen nicht überein
				end
			end
			--Account existiert nicht
		end
		mysql_close(handler);
	end
end

function AccountRegister(playerid)
	local draw = GetPlayerInputDrawsAccount(playerid);
	local info = GetPlayerDrawInfo(playerid, draw[1].Input);
	local accname = info.Text;
	info = GetPlayerDrawInfo(playerid, draw[2].Input);
	local passwort = info.Text;
	info = GetPlayerDrawInfo(playerid, draw[3].Input);
	local passwort_again = info.Text;
	if #accname >= 4 then
		if #passwort >= 8 then
			if passwort == passwort_again then
				if not DoesPlayerAccountExist(accname) then
					local handler = db_connect();
					
					if handler then
						local sql = "INSERT INTO `account` (Accountname,Password) VALUES ('"..accname.."', '"..passwort.."')";
						local result = mysql_query(handler, sql);
						if result then
							SetPlayerAccountname = accname;
							SetPlayerLoggedIn(playerid);
							SetPlayerChars(playerid, {});
							SetPlayerSpawnSelection(playerid);
						else
							-- Fehler
						end
						mysql_close(handler);
					else
						-- Verbindung konnte nicht aufgebaut werden.
					end
				else
					--Account existiert schon
				end
			else
				--passwörter nicht identisch
			end
		else
			--Passwort zu kurz
		end
	else
		--Accountname zu kurz
	end
end

function LoadPlayer(playerid, name)
	if playerid and name then
		local handler = db_connect();
		if handler then
			local sql = "SELECT * FROM 'player' WHERE Name='"..name.."'";
			local result = mysql_query(handler, sql);
			if result then
				local found = mysql_num_rows(result);
				mysql_close(handler);
				if found > 0 then
					local row = mysql_fetch_assoc(result);
					if Player[playerid].Screen then
						local data = 
						{
							Name = row.Name, 
							BodyModel = row.BodyModel,
							BodyTexture = row.BodyTexture,
							HeadModel = row.HeadModel,
							HeadTexture = row.HeadTexture,
							Dexterity = row.Dexterity, 
							Fatness = row.Fatness,
							ScaleX = row.ScaleX,
							ScaleZ = row.ScaleZ,
							Strength = row.Strength,
							Armor = row.Armor,
							MeleeWeapon = row.MeleeWeapon,
							RangedWeapon = row.RangedWeapon,
						};
						SetPlayerData(playerid, data);
					else
						SetPlayerData(playerid, row);
					end
				end
			end
		end
	end
end

function GetPlayerChars(playerid)
	local accname = GetPlayerAccountname(playerid);
	if DoesPlayerAccountExist(playerid) then
		local handler = db_connect();
		if handler then
			local sql = "SELECT 'Characters' FROM 'account' WHERE 'Accountname' = '"..accname.."'";
			local result = mysql_query(handler, sql);
			if result then
				local row = mysql_fetch_assoc(result);
				if #row.Characters >= 3 then
					mysql_close(handler);
					return row.Characters:split(";");
				end
			else
				-- Fehler
			end
			mysql_close(handler);
		else
			--  Verbindung konnte nicht aufgebaut werden.
		end
	end
	return {};
end

function mysql_UpdatePlayerChars(playerid, value)
	local accname = GetPlayerAccountname(playerid);
	if DoesPlayerAccountExist(playerid) and value then
		local handler = db_connect();
		if handler then
			local sql = "UPDATE 'account' SET 'Characters'='"..value.."' WHERE 'Accountname' = '"..accname.."'";
			local result = mysql_query(handler, sql);
			if result then
				
			else
				-- Fehler
			end
			mysql_close(handler);
		else
			--  Verbindung konnte nicht aufgebaut werden.
		end
	end
end

--[[
local res = runSQL("SELECT caption FROM skills WHERE id = " .. tostring(k) .. ";");
        
    if res then
        local row = mysql_fetch_row(res);
        
        while row do
            skillCaption = row[1];
            row = mysql_fetch_row(res);
        end
    end
]]