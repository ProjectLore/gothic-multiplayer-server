local Show = {};
local Hide = {};

local function FindIndex(tab,value)
	for k,v in pairs(tab) do
		if v == value then
			return k;
		end
	end
	return false
end

function CalculateTexturesForAcc()
	for k in pairs(SCREEN) do
		for v in pairs(SCREEN[k].BUTTON) do
			if SCREEN[k].BUTTON[v].BACK then
				table.insert(SCREEN[k].SHOW, SCREEN[k].BUTTON[v].BACK);
			end
		end
		SCREEN[k].HIDE = table.copy(SCREEN[k].SHOW);
	end
	for from in pairs(SCREEN) do
		Show[from] = {};
		Hide[from] = {};
		for to in pairs(SCREEN) do
			if from ~= to then
				Show[from][to] = {};
				Hide[from][to] = {};
				for k,v in pairs(SCREEN[from].SHOW) do
					local find = FindIndex(SCREEN[to].SHOW, v);
					if not find then
						table.insert(Hide[from][to], v);
					end
				end
				for k,v in pairs(SCREEN[to].SHOW) do
					local find = FindIndex(SCREEN[from].SHOW, v);
					if not find then
						table.insert(Show[from][to], v);
					end
				end
			end
		end
	end
end
AddEvent(CalculateTexturesForAcc, "OnServerStart");


function InitPlayerVarsForAccAndChar(playerid)
	Player[playerid] = {};
	Player[playerid].Accountname = nil;
	Player[playerid].LoggedIn = false;
	Player[playerid].Menu = false;
	Player[playerid].ListItem = 1;
	Player[playerid].Screen = 1;
	Player[playerid].Char = {};
	Player[playerid].SelectPos = {};
	Player[playerid].Draws = {};
	Player[playerid].Draws.Screen = {};
	
	for k in pairs(SCREEN) do
		Player[playerid].Draws.Screen[k]={};
		Player[playerid].Draws.Screen[k].All = {};
		Player[playerid].Draws.Screen[k].Button = {};
		Player[playerid].SelectPos[k] = {};
		Player[playerid].SelectPos[k].Button = {};
		for v in pairs(SCREEN[k].BUTTON) do
			Player[playerid].SelectPos[k].Button[v] = 1;
			Player[playerid].Draws.Screen[k].Button[v]={};
		end
	end
end
AddEvent(InitPlayerVarsForAccAndChar, "OnPlayerConnect");

function SetPlayerAccountname(playerid, name)
	if playerid and name then
		Player[playerid].Accountname = name;
	end
end

function GetPlayerAccountname(playerid)
	if playerid then
		return Player[playerid].Accountname;
	end	
	return false
end

function SetPlayerLoggedIn(playerid)
	if playerid then
		Player[playerid].LoggedIn = true;
	end
end

function IsPlayerLoggedIn(playerid)
	if playerid then
		return Player[playerid].LoggedIn;
	end
	return false
end

function IsPlayerInAccountMenu(playerid)
	return Player[playerid].Menu;
end

function IsButtonEnabled(playerid, from)
	local screen = GetPlayerScreen(playerid);
	local item = from or GetPlayerListItem(playerid);
	if SCREEN[screen].BUTTON[item].ENABLE == nil then return true end
	if type(SCREEN[screen].BUTTON[item].ENABLE) == "function" and SCREEN[screen].BUTTON[item].ENABLE(playerid, from) then return true end
	return false
end

local function CreatePlayerDraws(playerid)
	local x, y = SetAlignText(playerid, "Font_Old_20_White_Hi.TGA", HEADLINE,X, WIDTH,Y-200,Y, "middle", "middle");
	if Player[playerid].Draws.Headline then
		UpdatePlayerDraw(playerid, Player[playerid].Draws.Headline, x, y, HEADLINE, "Font_Old_20_White_Hi.TGA", 255, 255, 255, "Account");
	else
		Player[playerid].Draws.Headline = CreatePlayerDraw(playerid, x, y, HEADLINE, "Font_Old_20_White_Hi.TGA", 255, 255, 255, "Account");
		if Player[playerid].Draws.Headline == -1 then
			print("FATAL ERROR: Can not create player draw!");
		end
	end
	
	x, y = SetAlignText(playerid, "FONT_DEFAULT.TGA", FOOTER,X, WIDTH,Y+2350,HEIGHT, "middle", "middle");
	if Player[playerid].Draws.Footer then
		UpdatePlayerDraw(playerid, Player[playerid].Draws.Footer, x, y, FOOTER, "FONT_DEFAULT.TGA", 255, 255, 255, "Account");
	else
		Player[playerid].Draws.Footer = CreatePlayerDraw(playerid, x, y, FOOTER, "FONT_DEFAULT.TGA", 255, 255, 255, "Account");
		if Player[playerid].Draws.Footer == -1 then
			print("FATAL ERROR: Can not create player draw!");
		end
	end
	
	if not Player[playerid].Draws.Left then
		Player[playerid].Draws.Left = CreatePlayerDraw(playerid, 0, 0, "<=", "Font_Old_20_White_Hi.TGA", 255, 255, 255, "Account");
		if Player[playerid].Draws.Left == -1 then
			print("FATAL ERROR: Can not create player draw!");
		end
	end
	if not Player[playerid].Draws.Right then
		Player[playerid].Draws.Right = CreatePlayerDraw(playerid, 0, 0, "=>", "Font_Old_20_White_Hi.TGA", 255, 255, 255, "Account");
		if Player[playerid].Draws.Right == -1 then
			print("FATAL ERROR: Can not create player draw!");
		end
	end
	
	for k in pairs(SCREEN) do
		if SCREEN[k].HEADLINE 	then table.insert(Player[playerid].Draws.Screen[k].All, Player[playerid].Draws.Headline); end
		if SCREEN[k].FOOTER		then table.insert(Player[playerid].Draws.Screen[k].All, Player[playerid].Draws.Footer);	end
		for v in pairs(SCREEN[k].BUTTON) do
			local id = Player[playerid].Draws.Screen[k].Button[v].Text;
			local typ = SCREEN[k].BUTTON[v].TYPE;
			local x, y, width, height, x_align, y_align;
			local info = GetTextureInfo(SCREEN[k].BUTTON[v].BACK)
			if typ == TYPE_BUTTON or typ == TYPE_SELECT_SIMPLE then
				x,y,width, height = info.X, info.Y, info.Width, info.Height;
				x_align = "middle";
				y_align = "middle";
			elseif typ == TYPE_INPUT then
				x,y,width, height = info.X, info.Y-200, info.Width, info.Y;
				x_align = "left";
				y_align = "down";
				local x, y = SetAlignText(playerid, "FONT_DEFAULT.TGA", "",info.X, info.Width,info.Y,info.Height, "left", "middle");
				if Player[playerid].Draws.Screen[k].Button[v].Input then
					UpdatePlayerDraw(playerid, Player[playerid].Draws.Screen[k].Button[v].Input, x, y, "", "FONT_DEFAULT.TGA", 255, 255, 255, "Account");
				else
					Player[playerid].Draws.Screen[k].Button[v].Input = CreatePlayerDraw(playerid, x,y, "", "FONT_DEFAULT.TGA", 255, 255, 255, "Account");
					table.insert(Player[playerid].Draws.Screen[k].All, Player[playerid].Draws.Screen[k].Button[v].Input);
				end
			elseif typ == TYPE_SELECT then
				x,y,width, height = info.X, info.Y-200, info.Width, info.Y;
				x_align = "left";
				y_align = "down";
				local text;
				if SCREEN[k].BUTTON[v].SELECTTABLE then
					local t = table.copy(SCREEN[k].BUTTON[v]);
					for l in pairs(SCREEN[k].BUTTON[v].SELECTTABLE) do
						t.SELECT = t.SELECT[SCREEN[k].BUTTON[v].SELECTTABLE[l](playerid)];
						if t.SELECT[1].TEXT then
							text = t.SELECT[1].TEXT;
						end
					end	
				else
					text = SCREEN[k].BUTTON[v].SELECT[1].TEXT;
				end
				x, y = SetAlignText(playerid, "FONT_DEFAULT.TGA", text,info.X, info.Width,info.Y,info.Height, "middle", "middle");
				if Player[playerid].Draws.Screen[k].Button[v].Input then
					UpdatePlayerDraw(playerid, Player[playerid].Draws.Screen[k].Button[v].Input, x, y, text, "FONT_DEFAULT.TGA", 255, 255, 255, "Account");
				else
					Player[playerid].Draws.Screen[k].Button[v].Input = CreatePlayerDraw(playerid, x,y,text, "FONT_DEFAULT.TGA", 255, 255, 255, "Account");
					table.insert(Player[playerid].Draws.Screen[k].All, Player[playerid].Draws.Screen[k].Button[v].Input);
				end
			end
			local x, y = SetAlignText(playerid, "FONT_DEFAULT.TGA", SCREEN[k].BUTTON[v].TEXT,x, width,y,height, x_align, y_align);
			if id then
				UpdatePlayerDraw(playerid, id, x, y, SCREEN[k].BUTTON[v].TEXT, "FONT_DEFAULT.TGA", 255, 255, 255, "Account");
			else
				Player[playerid].Draws.Screen[k].Button[v].Text = CreatePlayerDraw(playerid, x,y, SCREEN[k].BUTTON[v].TEXT, "FONT_DEFAULT.TGA", 255, 255, 255, "Account");
				table.insert(Player[playerid].Draws.Screen[k].All, Player[playerid].Draws.Screen[k].Button[v].Text);
			end
		end
	end
end

function GetPlayerButtonDrawID(playerid, from)
	local screen = GetPlayerScreen(playerid);
	local item = from or GetPlayerListItem(playerid);
	return Player[playerid].Draws.Screen[screen].Button[item].Text;
end

function GetPlayerScreen(playerid)
	return Player[playerid].Screen;
end

function GetPlayerListItem(playerid)
	return Player[playerid].ListItem;
end

local function GetTexturesToShow(from, to)
	if from and to then
		if from == to then
			return SCREEN[from].SHOW, {};
		else
			return SHOW[from][to], HIDE[from][to];
		end
	end
end

local function ShowPlayerTextures(playerid, from , to)
	local show, hide = GetTexturesToShow(from, to);
	for k,v in pairs(show) do
		ShowTexture(playerid, v);
	end
	for k,v in pairs(hide) do
		HideTexture(playerid, v);
	end
end

local function ShowPlayerDraws(playerid, from , to)
	local show, hide = Player[playerid].Draws.Screen[to].All, Player[playerid].Draws.Screen[from].All;
	local item = GetPlayerListItem(playerid);
	if from == to then hide = {}; end
	for k,v in pairs(show) do
		ShowPlayerDraw(playerid, v);
	end
	for k,v in pairs(hide) do
		HidePlayerDraw(playerid, v);
	end
	if SCREEN[from].HEADLINE and not SCREEN[to].HEADLINE then
		HidePlayerDraw(playerid,  Player[playerid].Draws.Headline);
	end
	if SCREEN[to].HEADLINE and (from == to or not SCREEN[to].HEADLINE)then
		ShowPlayerDraw(playerid,  Player[playerid].Draws.Headline);
	end
	
	if SCREEN[from].FOOTER and not SCREEN[to].FOOTER then
		HidePlayerDraw(playerid,  Player[playerid].Draws.FOOTER);
	end
	if SCREEN[to].FOOTER and  (from == to or not SCREEN[to].FOOTER) then
		ShowPlayerDraw(playerid,  Player[playerid].Draws.FOOTER);
	end
	for k in pairs(SCREEN[to].BUTTON) do
		if IsButtonEnabled(playerid, k) then 
			if item == k then
				UpdatePlayerDraw(playerid, GetPlayerButtonDrawID(playerid, to), {Red = 255, Green = 0, Blue = 0});
			else
				UpdatePlayerDraw(playerid, GetPlayerButtonDrawID(playerid, to), {Red = 255, Green = 255, Blue = 255});
			end
		else
			if item == k then
				UpdatePlayerDraw(playerid, GetPlayerButtonDrawID(playerid, to), {Red = 55, Green = 0, Blue = 0});
			else
				UpdatePlayerDraw(playerid, GetPlayerButtonDrawID(playerid, to), {Red = 55, Green = 55, Blue = 55});
			end
		end
		if type(SCREEN[to].BUTTON[k].TEXT)=="function" then
		
		end
	end
end

local function SetButtonFocus(playerid, b)
	local screen = GetPlayerScreen(playerid);
	local item = b or GetPlayerListItem(playerid);
	local r, g, b = 255, 0, 0;
	ShowTexture(playerid, SCREEN[screen].BUTTON[item].FOCUS);
	if SCREEN[screen].BUTTON[item].ENABLE == nil or SCREEN[screen].BUTTON[item].ENABLE == false or ( type(SCREEN[screen].BUTTON[item].ENABLE) == "function" and SCREEN[screen].BUTTON[item].ENABLE(playerid, item) == false)then
		r, g, b = 55, 0, 0;
	end
	UpdatePlayerDraw(playerid, GetPlayerButtonDrawID(playerid), {Red = r, Green =g, Blue =b});
	
end	

local function SetButtonUnfocus(playerid, b)
	local screen = GetPlayerScreen(playerid);
	local item = b or GetPlayerListItem(playerid);
	local r, g, b = 255, 0, 0;
	HideTexture(playerid, SCREEN[screen].BUTTON[item].FOCUS);
	if SCREEN[screen].BUTTON[item].ENABLE == nil or SCREEN[screen].BUTTON[item].ENABLE == false or ( type(SCREEN[screen].BUTTON[item].ENABLE) == "function" and SCREEN[screen].BUTTON[item].ENABLE(playerid, item) == false)then
		r, g, b = 55, 0, 0;
	end
	UpdatePlayerDraw(playerid, GetPlayerButtonDrawID(playerid), {Red = r, Green =g, Blue =b});
end	

local function UpdateButton(playerid, nb, ob)
	SetButtonFocus(playerid, nb);
	SetButtonUnfocus(playerid, nb);
	if (SCREEN[screen].BUTTON[ob].TYPE == TYPE_SELECT or SCREEN[screen].BUTTON[ob].TYPE == TYPE_SELECT_SIMPLE) and not (SCREEN[screen].BUTTON[nb].TYPE == TYPE_SELECT or SCREEN[screen].BUTTON[nb].TYPE == TYPE_SELECT_SIMPLE) then 
		HidePlayerDraw(playerid, Player[playerid].Draws.Left);
		HidePlayerDraw(playerid, Player[playerid].Draws.Right);
	end
	if (SCREEN[screen].BUTTON[ob].TYPE == TYPE_SELECT or SCREEN[screen].BUTTON[ob].TYPE == TYPE_SELECT_SIMPLE) and SCREEN[screen].BUTTON[nb].TYPE == TYPE_SELECT or SCREEN[screen].BUTTON[nb].TYPE == TYPE_SELECT_SIMPLE then
		local info = GetTextureInfo(SCREEN[screen].BUTTON[nb].FOCUS);
		local x, y = SetAlignText(playerid, "Font_Old_20_White_Hi.TGA", "<=",info.X - 75, info.X,info.Y,info.Height, "right", "middle");
		UpdatePlayerDraw(playerid, Player[playerid].Draws.Left, {X = x, Y = y});
		x, y = SetAlignText(playerid, "Font_Old_20_White_Hi.TGA", "=>",info.Width, info.Width + 75,info.Y,info.Height, "left", "middle");
		UpdatePlayerDraw(playerid, Player[playerid].Draws.Right, {X = x, Y = y});
	end
	if (not (SCREEN[screen].BUTTON[ob].TYPE == TYPE_SELECT) or not ( SCREEN[screen].BUTTON[ob].TYPE == TYPE_SELECT_SIMPLE)) and (SCREEN[screen].BUTTON[nb].TYPE == TYPE_SELECT or SCREEN[screen].BUTTON[nb].TYPE == TYPE_SELECT_SIMPLE) then 
		local info = GetTextureInfo(SCREEN[screen].BUTTON[nb].FOCUS);
		local x, y = SetAlignText(playerid, "Font_Old_20_White_Hi.TGA", "<=",info.X - 75, info.X,info.Y,info.Height, "right", "middle");
		UpdatePlayerDraw(playerid, Player[playerid].Draws.Left, {X = x, Y = y});
		x, y = SetAlignText(playerid, "Font_Old_20_White_Hi.TGA", "=>",info.Width, info.Width + 75,info.Y,info.Height, "left", "middle");
		UpdatePlayerDraw(playerid, Player[playerid].Draws.Right, {X = x, Y = y});
		ShowPlayerDraw(playerid, Player[playerid].Draws.Left);
		ShowPlayerDraw(playerid, Player[playerid].Draws.Right);
	end
end

function StartAccountSystem(playerid)
	FreezePlayer(playerid);
	SetPlayerEnable_OnPlayerKey(playerid,1);
	CreatePlayerDraws(playerid);
	SetButtonFocus(playerid);
	ShowPlayerTextures(playerid, GetPlayerScreen(playerid), GetPlayerScreen(playerid));
	ShowPlayerDraws(playerid, GetPlayerScreen(playerid), GetPlayerScreen(playerid));
end

function AccountControl(playerid, keyUp, keyDown)
	if IsPlayerInAccountMenu(playerid) then
		if keyDown == KEY_UP then
			if Player[playerid].ListItem > 1 then
				UpdateButton(playerid, Player[playerid].ListItem, Player[playerid].ListItem -1);
				Player[playerid].ListItem = Player[playerid].ListItem - 1;
			end
			return
		end
		if keyDown == KEY_DOWN then
			if Player[playerid].ListItem < table.getn(SCREEN[Player[playerid].Screen].BUTTON) then
				UpdateButton(playerid, Player[playerid].ListItem, Player[playerid].ListItem + 1);
				Player[playerid].ListItem = Player[playerid].ListItem + 1;
			end
			return
		end
		if keyDown == KEY_LEFT then
			if SCREEN[Player[playerid].Screen].BUTTON[Player[playerid].ListItem].TYPE == TYPE_SELECT or SCREEN[Player[playerid].Screen].BUTTON[Player[playerid].ListItem].TYPE == TYPE_SELECT_SIMPLE and IsButtonEnabled(playerid, Player[playerid].ListItem) then
				local b = table.copy(SCREEN[Player[playerid].Screen].BUTTON[Player[playerid].ListItem]);
				local pos = Player[playerid].SelectPos[Player[playerid].Screen].Button[Player[playerid].ListItem];
				local t = {};
				if b.SELECTTABLE then
					t = b;
					for l in pairs(SCREEN[Player[playerid].Screen].BUTTON[Player[playerid].ListItem].SELECTTABLE) do
						t.SELECT = t.SELECT[SCREEN[Player[playerid].Screen].BUTTON[Player[playerid].ListItem].SELECTTABLE[l](playerid)];
					end	
				else
					t.SELECT = b.SELECT;
				end
				if t.SELECT[pos-1] then
					Player[playerid].SelectPos[Player[playerid].Screen].Button[Player[playerid].ListItem] = pos-1;
				else
					Player[playerid].SelectPos[Player[playerid].Screen].Button[Player[playerid].ListItem] = table.getn(t.SELECT);
				end
				if SCREEN[Player[playerid].Screen].BUTTON[Player[playerid].ListItem].TYPE == TYPE_SELECT then
					pos = Player[playerid].SelectPos[Player[playerid].Screen].Button[Player[playerid].ListItem];
					local text = t.SELECT[pos].TEXT;
					local info = GetTextureInfo(SCREEN[Player[playerid].Screen].BUTTON[Player[playerid].ListItem].BACK);
					local x, y = SetAlignText(playerid, fd, text,info.X, info.Width,info.Y,info.Height, "middle", "middle");
					UpdatePlayerDraw(playerid, Player[playerid].Draws.Screen[Player[playerid].Screen].Button[Player[playerid].ListItem].Input, {Text = text, X = x, Y = y});
				end
				if Player[playerid].Screen == SCREEN_CREATE then
					UpdateCreatePlayer(playerid, Player[playerid].ListItem, pos);
				end
			end
			return
		end
		if keyDown == KEY_RIGHT then
			if SCREEN[Player[playerid].Screen].BUTTON[Player[playerid].ListItem].TYPE == TYPE_SELECT or SCREEN[Player[playerid].Screen].BUTTON[Player[playerid].ListItem].TYPE == TYPE_SELECT_SIMPLE and IsButtonEnabled(playerid, Player[playerid].ListItem) then
				local b = table.copy(SCREEN[Player[playerid].Screen].BUTTON[Player[playerid].ListItem]);
				local pos = Player[playerid].SelectPos[Player[playerid].Screen].Button[Player[playerid].ListItem];
				local t = {};
				if b.SELECTTABLE then
					t = b;
					for l in pairs(SCREEN[Player[playerid].Screen].BUTTON[Player[playerid].ListItem].SELECTTABLE) do
						t.SELECT = t.SELECT[SCREEN[Player[playerid].Screen].BUTTON[Player[playerid].ListItem].SELECTTABLE[l](playerid)];
					end	
				else
					t.SELECT = b.SELECT;
				end
				if t.SELECT[pos+1] then
					Player[playerid].SelectPos[Player[playerid].Screen].Button[Player[playerid].ListItem] = pos+1;
				else
					Player[playerid].SelectPos[Player[playerid].Screen].Button[Player[playerid].ListItem] = 1;
				end
				if SCREEN[Player[playerid].Screen].BUTTON[Player[playerid].ListItem].TYPE == TYPE_SELECT then
					pos = Player[playerid].SelectPos[Player[playerid].Screen].Button[Player[playerid].ListItem];
					local text = t.SELECT[pos].TEXT;
					local info = GetTextureInfo(SCREEN[Player[playerid].Screen].BUTTON[Player[playerid].ListItem].BACK);
					local x, y = SetAlignText(playerid, fd, text,info.X, info.Width,info.Y,info.Height, "middle", "middle");
					UpdatePlayerDraw(playerid, Player[playerid].Draws.Screen[Player[playerid].Screen].Button[Player[playerid].ListItem].Input, {Text = text, X = x, Y = y});
				end
				if Player[playerid].Screen == SCREEN_CREATE then
					UpdateCreatePlayer(playerid, Player[playerid].ListItem, pos);
				end
			end
			return
		end
	end
end
AddEvent(AccountControl, "OnPlayerKey");

















