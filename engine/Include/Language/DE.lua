local String = {};
local str1 = [[This is a test dialog with the style "MSGBOX", it is displayed perfectly for the player.
You need atleast the resolution and the language of the player.

If you got both, the "Dialog" system works perfectly and you can create and multilanguage server.

With best regards,


Devel
]]
local str2 = [[Das ist ein Test-Dialog mit dem Stil "MSGBOX", es kann perfekt f�r den Spieler angezeigt werden.
Man ben�tigt lediglich die Aufl�sung des Spielers und dessen Sprache.

Wenn das vorhanden ist, kann man das "Dialog" System perfekt benutzen und einem Internationalen Server steht nichts im Wege.

Mit freundlichen Gr��en


Devel 
]]
String[str1]=str2;
return String;