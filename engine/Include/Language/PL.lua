local String = {};
local str1 = [[This is a test dialog with the style "MSGBOX", it is displayed perfectly for the player.
You need atleast the resolution and the language of the player.

If you got both, the "Dialog" system works perfectly and you can create and multilanguage server.

With best regards,


Devel
]]
local str2 = [[To test okno dialogowe ze stylem "MSGBOX", to jest doskonale wy�wietlane na gracza.

Trzeba przynajmniej rozdzielczo�� i j�zyk odtwarzacza.

Je�li masz zar�wno, System z "Dialog" dzia�a idealnie i mo�na stworzy� serwer wieloj�zyczny.

Pozdrawiam,

Devel
]]
String[str1]=str2;
return String;