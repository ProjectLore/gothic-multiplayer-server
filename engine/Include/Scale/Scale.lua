--[[
	Russische Schrift : Kodierung Windows 1251
	Polnische Schrift : Kodierung Windows 1250
	Deutche Schrift : Kodierung Windows 1250
]]

if not IsModuleLoaded("Language") then
	print("ERROR: Can not load modul: Scale , because modul 'Language' is not loaded.")
	return false
end

local F = {};
local H = {};
local Player={};
-- Deutsch
H["FONT_DEFAULT.TGA"] = 18; 
H["FONT_OLD_10_WHITE.TGA"] = 18; 
H["FONT_OLD_10_WHITE_HI.TGA"] = 18; 
H["FONT_OLD_20_WHITE.TGA"] = 32; 
H["FONT_OLD_20_WHITE_HI.TGA"] = 32; 
H["FONT_10_BOOK.TGA"] = 17; 
H["FONT_10_BOOK_HI.TGA"] = 17; 
H["FONT_20_BOOK.TGA"] = 32; 
H["FONT_20_BOOK_HI.TGA"] = 32; 

-- Polnisch
H["FONT_DEFAULT_PL.TGA"] = 18; 
H["FONT_OLD_10_WHITE_PL.TGA"] = 18; 
H["FONT_OLD_10_WHITE_HI_PL.TGA"] = 18; 
H["FONT_OLD_20_WHITE_PL.TGA"] = 32; 
H["FONT_OLD_20_WHITE_HI_PL.TGA"] = 32; 
H["FONT_10_BOOK_PL.TGA"] = 17; 
H["FONT_10_BOOK_HI_PL.TGA"] = 17; 
H["FONT_20_BOOK_PL.TGA"] = 32; 
H["FONT_20_BOOK_HI_PL.TGA"] = 32; 

--Russisch
H["FONT_DEFAULT_RU.TGA"] = 19; 
H["FONT_OLD_10_WHITE_RU.TGA"] = 19; 
H["FONT_OLD_10_WHITE_HI_RU.TGA"] = 19; 
H["FONT_OLD_20_WHITE_RU.TGA"] = 36; 
H["FONT_OLD_20_WHITE_HI_RU.TGA"] = 36; 
H["FONT_10_BOOK_RU.TGA"] = 18; 
H["FONT_10_BOOK_HI_RU.TGA"] = 18; 
H["FONT_20_BOOK_RU.TGA"] = 36; 
H["FONT_20_BOOK_HI_RU.TGA"] = 36; 

dofile("engine/Include/Scale/Windows-1251.luac");

function InitTextureFontData()
	local t = {"FONT_DEFAULT.TGA", "FONT_10_BOOK.TGA", "FONT_10_BOOK_HI.TGA","FONT_20_BOOK.TGA", "FONT_20_BOOK_HI.TGA", "FONT_OLD_10_WHITE.TGA", "FONT_OLD_10_WHITE_HI.TGA", "FONT_OLD_20_WHITE.TGA", "FONT_OLD_20_WHITE_HI.TGA",}
	local l = {".luac", "_PL.luac", "_RU.luac"};
	for l, m in pairs(l) do
		for k,v in pairs(t) do
			local file = table.concat({string.sub(v, 1, -5), m});
			local font = table.concat({string.sub(file, 1, -6), ".TGA"});
			F[string.upper(font)]=dofile("engine/Include/Scale/"..file.."");
		end
	end
end
AddEvent(InitTextureFontData, "OnServerStart");

function InitScalePlayerVars(playerid)
	Player[playerid]={};
	SetPlayerVideoScreen(playerid, 1024, 768);
	Player[playerid].ScaleX = nil;
	Player[playerid].ScaleY = nil;
	Player[playerid].ScreenX = nil;
	Player[playerid].ScreenY = nil;
end
AddEvent(InitScalePlayerVars, "OnPlayerConnect");

local function GetLanguageFont(lang, font)
	if lang and font then
		if lang == "PL" or lang == "RU" then
			return table.concat({string.sub(font, 1, -5), "_"..lang..".TGA"});
		else
			return font;
		end
	end
end

local function GetTextBytes(text)
	local t = {};
	if text then
		for i=1, #text do
			table.insert(t, string.byte(string.sub(text, i, i)));
		end
	end
	return t;
end

function GetPlayerTextX(playerid, font, text)
	if playerid and font and text then
		local x = 0;
		local scale = Player[playerid].ScaleX; if scale == nil then scale = 5.63; end
		local lang = GetPlayerLanguage(playerid);
		local f = 1.0;
		font = string.upper(GetLanguageFont(lang, font));
		local bytes={};
		if lang == "RU" then
			bytes = GetTextBytesRU(text);
			f = 1.05;
		else
			bytes = GetTextBytes(text);
		end
		if bytes and F[font] then
			for k,v in pairs(bytes) do
				if F[font][v] then
					x = x + F[font][v];
				end
			end
		end
		return ((x+string.len(text))*scale)*f;
	end
end

function GetPlayerTextY(playerid, font)
	if playerid and font then
		local scale = Player[playerid].ScaleY;
		if scale == nil then
			scale = 10;
		end
		local lang = GetPlayerLanguage(playerid);
		font = GetLanguageFont(lang, font);
		if H[string.upper(font)] then
			return H[string.upper(font)]*scale;
		end
	end
end

function SetPlayerVideoScreen(playerid, x, y)
	if playerid and x and y then
		if type(x)=="number" and type(y)=="number" then
			Player[playerid].ScreenX = x;
			Player[playerid].ScreenY = y;
			Player[playerid].ScaleX = 5.63*1440/x;
			Player[playerid].ScaleY = 10*900/y;
		end
	end
end

function GetPlayerVideoScreen(playerid)
	return Player[playerid].ScreenX, Player[playerid].ScreenY;
end

function SetAlignText(playerid, font, text,x, x2,y,y2, x_align, y_align)
	if text and font and x and x2 and y and y2 and x_align and y_align then
		x_align = string.lower(x_align); y_align = string.lower(y_align);
		local textwidth, textheight = GetPlayerTextX(playerid, font, text), GetPlayerTextY(playerid, font);
		local width, height = x2-x, y2-y;
		local r_x, r_y;
		if x_align == "right" then
			r_x = x2-textwidth-50;
		elseif x_align == "middle" then
			r_x = (x+(width/2))-(textwidth/2);
		else
			r_x = x + 25;
		end
		if y_align == "up" then
			r_y = y+25;
		elseif y_align == "down" then
			r_y = y2-textheight;
		else
			r_y = (y+(height/2))-(textheight/2);
		end
		return r_x, r_y;
	end
end

print("Modul: Scale\t\tv0.1\tloaded")