OldOnServerStart = OnServerStart;
function _OnServerStart()
	CallHandler("OnServerStart");
end
OnServerStart = _OnServerStart;

OldOnPlayerResponseDialog = OnPlayerResponseDialog;
function _OnPlayerResponseDialog(playerid, menu, button, listitem, inputtext)
	CallHandler("OnPlayerResponseDialog", playerid, menu, button, listitem, inputtext);
end
OnPlayerResponseDialog = _OnPlayerResponseDialog;

OldOnPlayerChangeLanguage = OnPlayerChangeLanguage;
function _OnPlayerChangeLanguage(playerid, currlan, oldlan)
	CallHandler("OnPlayerChangeLanguage", playerid, currlan, oldlan);
end
OnPlayerChangeLanguage = _OnPlayerChangeLanguage;

OldOnPlayerChangeListItem = OnPlayerChangeListItem ;
function _OnPlayerChangeListItem (playerid, currlistitem, oldlistitem)
	CallHandler("OnPlayerChangeListItem ", playerid, currlistitem, oldlistitem);
end
OnPlayerChangeListItem  = _OnPlayerChangeListItem;

OldOnPlayerChangeInputtext = OnPlayerChangeInputtext ;
function _OnPlayerChangeInputtext (playerid, currinputttext, oldinputttext)
	CallHandler("OnPlayerChangeInputtext ", playerid, currinputttext, oldinputttext);
end
OnPlayerChangeInputtext  = _OnPlayerChangeInputtext;

OldOnPlayerEnterPoint = OnPlayerEnterPoint ;
function _OnPlayerEnterPoint(playerid, ID)
	CallHandler("OnPlayerEnterPoint ", playerid, ID);
end
OnPlayerEnterPoint  = _OnPlayerEnterPoint;

OldOnPlayerLeavePoint = OnPlayerLeavePoint;
function _OnPlayerLeavePoint(playerid, ID)
	CallHandler("OnPlayerLeavePoint ", playerid, ID);
end
OnPlayerLeavePoint  = _OnPlayerLeavePoint;

OldOnPlayerEnterKhorinis = OnPlayerEnterKhorinis ;
function _OnPlayerEnterKhorinis(playerid)
	CallHandler("OnPlayerEnterKhorinis", playerid);
end
OnPlayerEnterKhorinis  = _OnPlayerEnterKhorinis;

OldOnPlayerLeaveKhorinis = OnPlayerLeaveKhorinis ;
function _OnPlayerLeaveKhorinis(playerid)
	CallHandler("OnPlayerLeaveKhorinis", playerid);
end
OnPlayerLeaveKhorinis  = _OnPlayerLeaveKhorinis;

