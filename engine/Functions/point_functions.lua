local Point = {};
local Player = {};

Q_KASERNE 	= 1;
Q_GALGEN 		= 2;
Q_MARKT 		= 3;
Q_TEMPEL 		= 4;
Q_UNTERSTADT 	= 5;
Q_HAFEN 		= 6;
Q_OBERSTADT 	= 7;

function StartCheckPlayerForKhorinis()
	SetTimer("CheckForKhorinis", 5000, 1);
end
--AddEvent(StartCheckPlayerForKhorinis, "OnServerStart");

function AddPoint(ID, x, y, z, range, int, _3D, world)
	if ID and x and y and z and range and int then
		_3D = _3D or false;
		world = world or "NEWWORLD//NEWWORLD.ZEN";
		Point[ID] = {X=x, Y=y, Z=z, Range = range,Intervalls = int,_3D = _3D, World = world,TimerID = SetTimerEx("CheckForPoint", int, 1, ID)};
	end
end

function InitPlayerVarsForPoint(playerid)
	Player[playerid] = {};
end
AddEvent(InitPlayerVarsForPoint, "OnPlayerConnect");

function RemovePlayerVarsFromPint(playerid)
	Player[playerid] = nil;
end
AddEvent(RemovePlayerVarsFromPint, "OnPlayerDisconnect");

function CheckForPoint(ID)
	if Point[ID] then
		local range = Point[ID].Range;
		local px, py, pz = Point[ID].X, Point[ID].Y, Point[ID].Z;
		local world = Point[ID].World;
		for playerid in pairs(GetConnectedPlayers()) do
			if GetPlayerWorld(playerid) == world then
				local x, y, z = GetPlayerPos(playerid);
				if Point[ID]._3D then
					local dis =  GetDistance3D(px, py, pz, x, y, z);
					if dis < range and not Player[playerid][ID] then
						OnPlayerEnterPoint(playerid, ID);
						Player[playerid][ID] = true;
					elseif dis > range and Player[playerid][ID] then
						OnPlayerLeavePont(playerid, ID);
						Player[playerid][ID] = false;
					end	
				else
					local dis =  GetDistance2D(px, pz, x, z);
					if dis < range and not Player[playerid][ID] then
						OnPlayerEnterPoint(playerid, ID);
						Player[playerid][ID] = true;
					elseif dis > range and Player[playerid][ID] then
						OnPlayerLeavePont(playerid, ID);
						Player[playerid][ID] = false;
					end	
				end
			end
		end
	end
end

local function GetDistToCenter (playerid, quarter)
	local dist;
	local x, y, z = GetPlayerPos(playerid);
	if (quarter == Q_KASERNE) then
		if (GetDistance3D(x, y, z, 4956.67871, 800.00293, 7277.50732) < GetDistance3D(x, y, z,  6118.79004, 800.003662, 6241.24512)) then
			dist = GetDistance3D(x, y, z,  4956.67871, 800.00293, 7277.50732);
		else
			dist = GetDistance3D(x, y, z,  6118.79004, 800.003662, 6241.24512);
		end
	elseif (quarter == Q_GALGEN) then
		dist = GetDistance3D(x, y, z, 6189.37598, 342.705353, 3021.74097);
		if (dist > 1900) then
			dist = 100000;
		end
	elseif (quarter == Q_MARKT) then
		dist = GetDistance3D(x, y, z, 8764.83789, 320.055481, 5024.51758);
		if (dist > 2300) then
			dist = 100000;
		end
	elseif (quarter == Q_TEMPEL) then
		dist = GetDistance3D(x, y, z,  9336.02441, 320.150452, -719.31781);
	elseif (quarter == Q_UNTERSTADT)then
		dist = GetDistance3D(x, y, z,  6388.60693, 320.084015, -3057.73071);
	elseif (quarter == Q_HAFEN)then
		if (GetDistance3D(x, y, z,  3138.79614,-67.5571976,-4147.32373) < GetDistance3D(x, y, z,  2509.8335,-138.134308,833.718201)) then
			dist = GetDistance3D(x, y, z,  3138.79614,-67.5571976,-4147.32373);
		else
			dist = GetDistance3D(x, y, z,  2509.8335,-138.134308,833.718201);
		end
	elseif (quarter == Q_OBERSTADT) then
		if ( GetDistance3D(x, y, z, 12116.2734, 950.004883, -3708.69507) <  GetDistance3D(x, y, z, 12668.7402, 962.934998, 1030.16675)) 
		and ( GetDistance3D(x, y, z, 12116.2734, 950.004883, -3708.69507) <  GetDistance3D(x, y, z, 14953.9766, 950.004883, -2578.55615)) then	
			dist =  GetDistance3D(x, y, z, 12116.2734, 950.004883, -3708.69507);
		end
	
		if ( GetDistance3D(x, y, z, 12668.7402, 962.934998, 1030.16675) <  GetDistance3D(x, y, z, 12116.2734, 950.004883, -3708.69507))
		and ( GetDistance3D(x, y, z, 12668.7402, 962.934998, 1030.16675) <  GetDistance3D(x, y, z, 14953.9766, 950.004883, -2578.55615)) then
			dist =  GetDistance3D(x, y, z, 12668.7402, 962.934998, 1030.16675);
		end
		
		if ( GetDistance3D(x, y, z, 14953.9766, 950.004883, -2578.55615) <  GetDistance3D(x, y, z, 12116.2734, 950.004883, -3708.69507))
		and ( GetDistance3D(x, y, z, 14953.9766, 950.004883, -2578.55615) <  GetDistance3D(x, y, z, 12668.7402, 962.934998, 1030.16675)) then
			dist = GetDistance3D(x, y, z ,14953.9766, 950.004883, -2578.55615);
		end
	else
		print("WRONG Q-PARAMETER!");
	end
	
	return dist;
end

local function IsQuarterNearest (playerid, quarter)
	local quartDist = GetDistToCenter(playerid, quarter);
	if (quartDist <= GetDistToCenter(playerid, Q_KASERNE))
	and (quartDist <= GetDistToCenter(playerid, Q_GALGEN))
	and (quartDist <= GetDistToCenter(playerid, Q_MARKT))
	and (quartDist <= GetDistToCenter(playerid, Q_TEMPEL))
	and (quartDist <= GetDistToCenter(playerid, Q_UNTERSTADT))
	and (quartDist <= GetDistToCenter(playerid, Q_HAFEN))
	and (quartDist <= GetDistToCenter(playerid, Q_OBERSTADT)) then
		return true;
	else
		return false;
	end
end

local function NpcIsInQuarter (playerid)

	if (IsQuarterNearest(playerid, Q_KASERNE)) then

		return Q_KASERNE;
	end
	
	if (IsQuarterNearest(playerid, Q_GALGEN)) then
		return Q_GALGEN;
	end
	
	if (IsQuarterNearest(playerid, Q_MARKT)) then
		return Q_MARKT;
	end
	
	if (IsQuarterNearest(playerid, Q_TEMPEL)) then
		return Q_TEMPEL;
	end
	
	if (IsQuarterNearest(playerid, Q_UNTERSTADT)) then
		return Q_UNTERSTADT;
	end
	
	if (IsQuarterNearest(playerid, Q_HAFEN)) then
		return Q_HAFEN;
	end

	if (IsQuarterNearest(playerid, Q_OBERSTADT)) then
		return Q_OBERSTADT;
	end
end

function CheckForKhorinis()
	for playerid in pairs(GetConnectedPlayers()) do
		print(NpcIsInQuarter (playerid));
		if NpcIsInQuarter (playerid) and Player[playerid]["Khorinis"] == nil then
			OnPlayerEnterKhorinis(playerid);
			Player[playerid]["Khorinis"] = true;
		elseif  NpcIsInQuarter (playerid)==nil and Player[playerid]["Khorinis"] then
			OnPlayerLeaveKhorinis(playerid);
			Player[playerid]["Khorinis"] = nil;
		end
	end
end