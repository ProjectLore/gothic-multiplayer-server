local Player = {};

function BlockOnPlayerKeyAfterUsingChat(playerid)
	SetPlayerEnable_OnPlayerKey(playerid, 0)
	if Player[playerid].Timer == nil then
		Player[playerid].Timer = SetTimerEx("SetPlayerEnable_OnPlayerKey", 250, 0, playerid, 1);
	else
		if IsTimerActive(Player[playerid].Timer) then
			KillTimer(Player[playerid].Timer);
			Player[playerid].Timer = SetTimerEx("SetPlayerEnable_OnPlayerKey", 250, 0, playerid, 1);
		else
			Player[playerid].Timer = SetTimerEx("SetPlayerEnable_OnPlayerKey", 250, 0, playerid, 1);
		end
	end
end
AddEvent(BlockOnPlayerKeyAfterUsingChat, "OnPlayerText", 1);
AddEvent(BlockOnPlayerKeyAfterUsingChat, "OnPlayerCommandText", 1);
AddEvent(BlockOnPlayerKeyAfterUsingChat, "OnPlayerResponseDialog", 1);

function insertPlayerToConnectedList(playerid)
	Player[playerid] = {};
	Player[playerid].Timer = nil;
end
AddEvent(insertPlayerToConnectedList, "OnPlayerConnect");

function deltePlayerFromConnectedList(playerid)
	Player[playerid] = nil;
end
AddEvent(deltePlayerFromConnectedList, "OnPlayerDisconnect");

function GetConnectedPlayers()
	return Player;
end

function GetEachWord(text)
	local currword = {};
	local words = {};
	for i=1, string.len(text), 1 do
		local currletter = string.sub(text, i,i);
		if currletter == " " then
			table.insert(words, table.concat(currword));
			currword = {};
		elseif string.byte(currletter) == 10 then
			table.insert(words, table.concat(currword));
			table.insert(words, "\n");
			currword = {};
		elseif string.byte(currletter) == 9 then
			table.insert(words, table.concat(currword));
			table.insert(words, string.char(9));
			currword = {};
		else
			table.insert(currword, currletter);
		end
	end
	table.insert(words, table.concat(currword));
	return words;
end

function FormatText(playerid, text, maxlen, font)
	local words = GetEachWord(text);
	local lines = {};
	local currline = {};
	for k,v in pairs(words) do
		if GetPlayerTextX(playerid, font,v) > maxlen then
			return text
		end
		if GetPlayerTextX(playerid, font, table.concat(currline)) + GetPlayerTextX(playerid, font,v) > maxlen then
			table.insert(lines, table.concat(currline));
			currline = {};
			table.insert(currline, v);
			table.insert(currline, " ");
		elseif v == "\n" then
			table.insert(lines, table.concat(currline));
			currline = {};
		else
			table.insert(currline, v);
			table.insert(currline, " ");
		end
	end
	table.insert(lines, table.concat(currline));
	return lines;
end

function string:split(delimiter)
  local result = { }
  local from = 1
  local delim_from, delim_to = string.find( self, delimiter, from )
  while delim_from do
    table.insert( result, string.sub( self, from , delim_from-1 ) )
    from = delim_to + 1
    delim_from, delim_to = string.find( self, delimiter, from )
  end
  table.insert( result, string.sub( self, from ) )
  return result
end



function string.ends(String,End)
   return End=='' or string.sub(String,-string.len(End))==End
end

function string.starts(String,Start)
   return string.sub(String,1,string.len(Start))==Start
end

function string.contains(String, containstr)
	return string.find(string.lower(String), string.lower(containstr)) ~= nil;
end

function table.copy(t)
  local u = { }
  for k, v in pairs(t) do u[k] = v end
  return setmetatable(u, getmetatable(t))
end