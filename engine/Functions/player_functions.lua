local Player = {};

function UpdatePlayerData(i)
	local x, y, z = GetPlayerPos(i);
	local sx, sy, sz = GetPlayerScale(i);
	local r, g, b = GetPlayerColor(i);
	local a, b, c, d = GetPlayerAdditionalVisual(i);
	Player[i] = 
	{
	World = GetPlayerWorld(i),
	Instance = GetPlayerInstance(i),
	Acrobatic = GetPlayerAcrobatic(i),
	BodyModel = a, 
	BodyTexture = b, 
	HeadModel = c, 
	HeadTexture = d,
	Angle = GetPlayerAngle(i),
	Red = r, 
	Green = g, 
	Blue = b,
	Dexterity = GetPlayerDexterity(i),
	ExperienceNextLevel = GetPlayerExperienceNextLevel(i),
	Experience = GetPlayerExperience(i),
	Level = GetPlayerLevel(i),
	MagicLevel = GetPlayerMagicLevel(i),
	LearnPoints = GetPlayerLearnPoints(i),
	Fatness = GetPlayerFatness(i),
	Gold = GetPlayerGold(i),
	MaxHealth = GetPlayerMaxHealth(i),
	Health = GetPlayerHealth(i),
	MaxMana = GetPlayerMaxMana(i),
	Mana = GetPlayerMana(i),
	Name = GetPlayerName(i),
	PosX = x, 
	PosY = y, 
	PosZ = z,
	ScaleX = sx, 
	ScaleY = sy, 
	ScaleZ = sz,
	Science = 
		{
		[0] = GetPlayerScience(i, 0),
		[1] = GetPlayerScience(i, 1),
		[2] = GetPlayerScience(i, 2),
		[3] = GetPlayerScience(i, 3),
		[4] = GetPlayerScience(i, 4),
		[5] = GetPlayerScience(i, 5),
		[6] = GetPlayerScience(i, 6),
		},
	SkillWeapon =
		{
		[0] = GetPlayerSkillWeapon(i, 0),
		[1] = GetPlayerSkillWeapon(i, 1),
		[2] = GetPlayerSkillWeapon(i, 2),
		[3] = GetPlayerSkillWeapon(i, 3),
		},
	Strength = GetPlayerStrength(i),
	Walk = GetPlayerWalk(i),
	Armor = GetEquippedArmor(i),
	MeleeWeapon = GetEquippedMeleeWeapon(i),
	RangedWeapon = GetEquippedRangedWeapon(i),
	};
end

function InitPlayerVarsForGame(playerid)
	Player[playerid] = {};
	UpdatePlayerData(playerid);
end
AddEvent(InitPlayerVarsForGame, "OnPlayerConnect");

local function calculateLevelAndExp(experiencePoints)
	local level = 1;
	local nextLevelExperiencePoints = 500;
	while nextLevelExperiencePoints < experiencePoints do
	level = level + 1;
	nextLevelExperiencePoints = nextLevelExperiencePoints * 2;
	end
	return level, nextLevelExperiencePoints;
end

local function checkForLevelUp(playerid)
	local experience = GetPlayerExperience(playerid);
	local currentLevel = GetPlayerLevel(playerid);
	local nextLevel, nextLevelExp = calculateLevelAndExp(experience);
	local currentLearnPoints = GetPlayerLearnPoints(playerid);
	local addLearnPoints = (nextLevel - currentLevel) * 10;
	SetPlayerLevel(playerid, nextLevel);
	SetPlayerExperienceNextLevel(playerid, nextLevelExp);
	SetPlayerLearnPoints(playerid, currentLearnPoints + addLearnPoints);
end

function GetPlayerData(playerid)
	if playerid then
		return Player[playerid];
	end
end

function SetPlayerData(i, t)
	if i and t and type(t)=="table" then
		if t.World then
			SetPlayerWorld(i, t.World);
		end
		if t.Instance then
			SetPlayerInstance(i, t.Instance);
		end
		if t.Acrobatic then
			SetPlayerAcrobatic(i, tonumber(t.Acrobatic));
		end
		if t.BodyModel or t.BodyTexture or t.HeadModel or t.HeadTexture then
			local a, b, c, d = GetPlayerAdditionalVisual(i);
			a = t.BodyModel or a;
			b = t.BodyTexture or b;
			c = t.HeadModel or c;
			d = t.HeadTexture or d;
			SetPlayerAdditionalVisual(i, a, tonumber(b), c, tonumber(d));
		end
		if t.Angle then
			SetPlayerAngle(i, tonumber(t.Angle));
		end
		if t.Red or t.Green or t.Blue then
			SetPlayerColor(i, tonumber(t.Red) or 255, tonumber(t.Green) or 255,  tonumber(t.Blue) or 255);
		end
		if t.Dexterity then
			SetPlayerDexterity(i, tonumber(t.Dexterity));
		end
		if t.ExperienceNextLevel then
			SetPlayerExperienceNextLevel(i, tonumber(t.ExperienceNextLevel));
		end
		if t.Level then
			SetPlayerLevel(i, tonumber(t.Level));
		end
		if t.Experience then
			SetPlayerExperience(i, tonumber(t.Experience));
		end
		if t.Fatness then
			SetPlayerFatness(i, tonumber(t.Fatness));
		end
		if t.Gold then
			SetPlayerGold(i, tonumber(t.Gold));
		end
		if t.MaxHealth then
			SetPlayerMaxHealth(i, tonumber(t.MaxHealth));
		end
		if t.Health then
			SetPlayerHealth(i, tonumber(t.Health));
		end
		if t.LearnPoints then
			SetPlayerLearnPoints(i, tonumber(t.LearnPoints));
		end
		if t.MagicLevel then
			SetPlayerMagicLevel(i, tonumber(t.MagicLevel));
		end
		if t.MaxMana then
			SetPlayerMaxMana(i, tonumber(t.MaxMana));
		end
		if t.Mana then
			SetPlayerMana(i, tonumber(t.Mana));
		end
		if t.Name then
			SetPlayerName(i, t.Name);
		end
		if t.PosX and t.PosY and t.PosZ then
			SetPlayerPos(i, tonumber(t.PosX), tonumber(t.PosY), tonumber(t.PosZ));
		end
		if t.ScaleX and t.ScaleY and t.ScaleZ then
			SetPlayerScale(i, tonumber(t.ScaleX) , tonumber(t.ScaleY) , tonumber(t.ScaleZ));
		end
		if t.Science then
			SetPlayerScience(i, 0, tonumber(t.Science[0]) or 0);
			SetPlayerScience(i, 1, tonumber(t.Science[1]) or 0);
			SetPlayerScience(i, 2, tonumber(t.Science[2]) or 0);
			SetPlayerScience(i, 3, tonumber(t.Science[3]) or 0);
			SetPlayerScience(i, 4, tonumber(t.Science[4]) or 0);
			SetPlayerScience(i, 5, tonumber(t.Science[5]) or 0);
			SetPlayerScience(i, 6, tonumber(t.Science[6]) or 0);
		end
		if t.SkillWeapon then
			SetPlayerSkillWeapon(i, 0, tonumber(t.SkillWeapon[0]) or 10);
			SetPlayerSkillWeapon(i, 1, tonumber(t.SkillWeapon[1]) or 10);
			SetPlayerSkillWeapon(i, 2, tonumber(t.SkillWeapon[2]) or 10);
			SetPlayerSkillWeapon(i, 3, tonumber(t.SkillWeapon[3]) or 10);
		end
		if t.Strength then
			SetPlayerStrength(i, tonumber(t.Strength));
		end
		if t.Walk then
			SetPlayerWalk(i, t.Walk);
		end
	end
end

function PlayerChangeAcrobatic(playerid, currAcro, oldAcro)
	Player[playerid].Accrobatic = currAcro;
end
AddEvent(PlayerChangeAcrobatic, "OnPlayerChangeAcrobatic");

function PlayerChangeAdditionalVisual(playerid, currBodyModel, currBodyTexture, currHeadModel, currHeadTexture, oldBodyModel, oldBodyTexture, oldHeadModel, oldHeadTexture)
	Player[playerid].BodyModel = currBodyModel;
	Player[playerid].BodyTexture = currBodyTexture;
	Player[playerid].HeadModel = currHeadModel;
	Player[playerid].HeadTexture = currHeadTexture;
end
AddEvent(PlayerChangeAdditionalVisual, "OnPlayerChangeAdditionalVisual");

function PlayerChangeArmor(playerid, currArmor, oldArmor)
	Player[playerid].Armor = currArmor;
end
AddEvent(PlayerChangeArmor, "OnPlayerChangeArmor");

function PlayerChangeClass(playerid, class)
	Player[playerid].Class = class;
end
AddEvent(PlayerChangeClass, "OnPlayerChangeClass");

function PlayerChangeDexterity(playerid, currDex, oldDex)
	Player[playerid].Dexterity = currDex;
end
AddEvent(PlayerChangeDexterity, "OnPlayerChangeDexterity");

function PlayerChangeFatness(playerid,  currFat, oldFat)
	Player[playerid].Fatness = currFat;
end
AddEvent(PlayerChangeFatness, "OnPlayerChangeFatness");

function PlayerChangeGold(playerid, currGold, oldGold)
	Player[playerid].Gold = currGold;
end
AddEvent(PlayerChangeGold, "OnPlayerChangeGold");

function PlayerChangeHealth(playerid, currHealth, oldHealth)
	Player[playerid].Health = currHealth;
end
AddEvent(PlayerChangeHealth, "OnPlayerChangeHealth");

function PlayerChangeInstance(playerid, currInstance, oldInstance)
	Player[playerid].Instance = currInstance;
end
AddEvent(PlayerChangeInstance, "OnPlayerChangeInstance");

function PlayerChangeMana(playerid, currMana, oldMana)
	Player[playerid].Mana = currMana;
end
AddEvent(PlayerChangeMana, "OnPlayerChangeMana");

function PlayerChangeMaxHealth(playerid, currMaxHealth, oldMaxHealth)
	Player[playerid].MaxHealth = currMaxHealth;
end
AddEvent(PlayerChangeMaxHealth, "OnPlayerChangeMaxHealth");

function PlayerChangeMaxMana(playerid, currMaxMana, oldMaxMana)
	Player[playerid].MaxMana = currMaxMana;
end
AddEvent(PlayerChangeMaxMana, "OnPlayerChangeMaxMana");

function PlayerChangeMeleeWeapon(playerid, currMelee, oldMelee)
	Player[playerid].MeleeWeapon = currMelee;
end
AddEvent(PlayerChangeMeleeWeapon, "OnPlayerChangeMeleeWeapon");

function PlayerChangeRangedWeapon(playerid, currRanged, oldRanged)
	Player[playerid].RangedWeapon = currRanged;
end
AddEvent(PlayerChangeRangedWeapon, "OnPlayerChangeRangedWeapon");

function PlayerChangeScience(playerid, scienceID, currScienceValue, oldScienceValue)
	if not Player[playerid].Science then Player[playerid].Science = {} end
	Player[playerid].Science[scienceID] = currScienceValue;
end
AddEvent(PlayerChangeScience, "OnPlayerChangeScience");

function PlayerChangeSkillWeapon(playerid, skillID, currSkillAmount, oldSkillAmount)
	if not Player[playerid].SkillWeapon then Player[playerid].SkillWeapon = {}; end
	Player[playerid].SkillWeapon[skillID] = currSkillAmount;
end
AddEvent(PlayerChangeSkillWeapon, "OnPlayerChangeSkillWeapon");

function PlayerChangStrength(playerid, currStrength, oldStrength)
	Player[playerid].Strength = currStrength;
end
AddEvent(PlayerChangStrength, "OnPlayerChangStrength");

function PlayerChangeWalk(playerid, currWalk, oldWalk)
	Player[playerid].Walk = currWalk;
end
AddEvent(PlayerChangeWalk, "OnPlayerChangeWalk");

function PlayerChangeWorld(playerid, world)
	Player[playerid].World = world;
end
AddEvent(PlayerChangeWorld, "OnPlayerChangeWorld");

function PlayerChangeExperience(playerid, currExp, oldExp)
	Player[playerid].Experience = currExp;
	checkForLevelUp(playerid);
end
AddEvent(PlayerChangeExperience, "OnPlayerChangeExperience");

function PlayerChangeExperienceNextLevel(playerid, currExp, oldExp)
	Player[playerid].ExperienceNextLevel = currExp;
end
AddEvent(PlayerChangeExperienceNextLevel, "OnPlayerChangeExperienceNextLevel");

function PlayerChangeLevel(playerid, currLevel, oldLevel)
	Player[playerid].Level = Level;
end
AddEvent(PlayerChangeLevel, "OnPlayerChangeLevel");

function PlayerChangeLearnPoints(playerid, currLP, oldLP)
	Player[playerid].LearnPoints = currLP;
end
AddEvent(PlayerChangeLearnPoints, "OnPlayerChangeLearnPoints");

function PlayerChangeScale(playerid, currX, currY, currZ, oldX, oldY, oldZ)
	Player[playerid].ScaleX = currX;
	Player[playerid].ScaleY = currY;
	Player[playerid].ScaleZ = currZ;
end
AddEvent(PlayerChangeScale, "OnPlayerChangeScale");

function PlayerChangeMagicLevel(playerid, currLevel, oldLevel)
	Player[playerid].MagicLevel = currLevel;
end
AddEvent(PlayerChangeMagicLevel, "OnPlayerChangeMagicLevel");

function PlayerChangeLearnPoints(playerid, currLP, oldLP)
	Player[playerid].LearnPoints = currLP;
end
AddEvent(PlayerChangeLearnPoints, "OnPlayerChangeLearnPoints");

























